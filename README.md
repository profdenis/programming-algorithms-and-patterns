# Programming Algorithms and Patterns

## Course materials

### Lecture notes

Lecture notes in the `doc` folder

### Code examples

Code examples in the `src` folder
