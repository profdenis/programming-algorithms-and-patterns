<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>01_searching</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="../html/lecture_notes.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js" type="text/javascript"></script>
</head>
<body>
<header id="title-block-header">
<h1 class="title">01_searching</h1>
</header>
<h1 id="algorithms">Algorithms</h1>
<ul>
<li><strong>Algorithm</strong>:
<ul>
<li>A set of rules or instructions that, is applied correctly, will solve a problem in a finite number of steps</li>
<li>A set of mathematical instructions or rules that, especially if given to a computer, will help to calculate an answer to a problem <a href="https://dictionary.cambridge.org/dictionary/english/algorithm">[1]</a></li>
</ul></li>
<li><strong>Algorithm complexity</strong>:
<ul>
<li>Analysis and comparison of algorithms efficiency</li>
<li>2 types of complexity:
<ul>
<li>theoretical (or mathematical)</li>
<li>empirical (or experimental)</li>
</ul></li>
</ul></li>
</ul>
<h2 id="searching-for-a-value-in-an-array">Searching for a value in an array</h2>
<ul>
<li>Given an array of elements, not necessarily sorted, we need to search for a specific element (or value), often called the <em>target</em> element, in the array</li>
<li>The element may be present or not in the array</li>
<li>If the element is present, then we can either
<ul>
<li>return <code>true</code>, or</li>
<li>return the index (or position) where the element was found in the array</li>
</ul></li>
<li>If the element is not present in the array, then we can either
<ul>
<li>return <code>false</code>, or</li>
<li>return <code>-1</code>
<ul>
<li><code>-1</code> is always an invalid array index, that’s why it is commonly used to mean <em>not found</em> when an integer is required as a return value</li>
</ul></li>
</ul></li>
</ul>
<h3 id="sequential-search">Sequential search</h3>
<ul>
<li>When the array elements are not sorted, we don’t really have a choice, we need to use a <em>sequential search</em></li>
<li>The basic idea is simple:
<ol type="1">
<li>start with the first element in the array (index 0)</li>
<li>compare this element with the target element</li>
<li>if they are equal, then we are done, and we can return <code>true</code> or the index</li>
<li>if not, repeat the process with the next element in the array</li>
<li>if we went through all the elements in the array without finding the target element, then the search has failed, and we need to return <code>false</code> or <code>-1</code></li>
</ol></li>
</ul>
<pre><code>function sequential_search(arr, target)
    // arr: array of comparable (or least equatable) elements
    // target: the element we are looking for
    // n: length of arr
    // returns: the index where target was found, or -1
    for i = 0 up to n-1:
        if target == arr[i]:
            return i

    return -1</code></pre>
<figure>
<img src="../images/sequential_search.jpg" alt="" /><figcaption>Sequential search</figcaption>
</figure>
<h2 id="algorithm-complexity">Algorithm complexity</h2>
<ul>
<li>To obtain the complexity of an algorithm, we normally need to analyse it theoretically</li>
<li>We can also do an empirical analysis</li>
<li>To figure out the theoretical complexity of an algorithm, we don’t need to code it and execute it:
<ul>
<li>we can use its pseudocode to analyse it</li>
<li>we count the number of instructions to execute, or a number of specific operations
<ul>
<li>for example, we can count the number of additions or comparisons</li>
</ul></li>
<li>we normally express the complexity in terms of the problem size
<ul>
<li>for example, the number of elements in a list or an array’s length</li>
<li><span class="math inline">\(n\)</span> is often used to denote the problem size</li>
</ul></li>
<li>the notation <span class="math inline">\(O(f(n))\)</span> is often used to give an algorithm’s complexity
<ul>
<li><span class="math inline">\(O\)</span> is pronounced <em>big O</em> (not <em>zero</em>, but the uppercase letter <em>O</em>)</li>
<li><span class="math inline">\(f(n)\)</span> is a function of <span class="math inline">\(n\)</span></li>
</ul></li>
</ul></li>
<li>The most important thing in theoretical analysis, is the order of magnitude of the complexity, of the function <span class="math inline">\(f\)</span></li>
<li>in terms of <span class="math inline">\(n\)</span></li>
<li>The detailed theoretical analysis will often use limits when <span class="math inline">\(n\)</span> goes to infinity: <span class="math inline">\(lim_{n\rightarrow \infty}\)</span></li>
<li>The most important question theoretical analysis:
<ul>
<li>when <span class="math inline">\(n\)</span> grows more and more, what will happen with an algorithm’s performance?</li>
<li>in other words: how will an algorithm’s performance vary when the problem size is going to increase?</li>
</ul></li>
</ul>
<h4 id="common-complexities">Common complexities</h4>
<ul>
<li><em>Constant</em>: <span class="math inline">\(O(1)\)</span></li>
<li><em>Logarithmic</em>: <span class="math inline">\(O(lg\ n)\)</span></li>
<li><em>Linear</em>: <span class="math inline">\(O(n)\)</span></li>
<li><em>Linear-logarithmic</em>: <span class="math inline">\(O(n\ lg\ n)\)</span></li>
<li><em>Quadratic</em>: <span class="math inline">\(O(n^2)\)</span></li>
<li><em>Exponential</em>: <span class="math inline">\(O(2^n)\)</span></li>
<li><em>Factorial</em>: <span class="math inline">\(O(n!)\)</span></li>
</ul>
<h4 id="analysis-3-cases">Analysis: 3 cases</h4>
<ul>
<li>An analysis can be done based on 3 different cases:
<ul>
<li>the best case</li>
<li>the average case</li>
<li>the worst case</li>
</ul></li>
<li>We often concentrate on the worst case, but the average case is also interesting</li>
<li>The best case is less interesting in general because it is much less likely to happen than the other 2 cases</li>
</ul>
<h3 id="sequential-search-complexity">Sequential search: complexity</h3>
<h4 id="best-case">Best case</h4>
<ul>
<li>If we are lucky, no matter how big the array is, we find the target element immediately, at the beginning of the array (<code>index == 0</code>)</li>
<li>In this case, we say that the algorithm is <em>constant</em>, or runs in <em>constant time</em>, or has a <em>constant complexity</em> because no matter what size the array is, the number of operations is the same</li>
<li>we denote its complexity with the expression <span class="math inline">\(O(1)\)</span></li>
</ul>
<h4 id="worst-case">Worst case</h4>
<ul>
<li>If we are not lucky, we have to go through the whole array until the last position (<code>index == n-1</code>), to find the target element</li>
<li>We also need to go through the whole array if the target is not in the array</li>
<li>The number of operations to execute will depend on the value of <span class="math inline">\(n\)</span>
<ul>
<li>for every element in the array, we compare the target to it</li>
<li>if the array contains 10 elements, then we need 10 comparisons</li>
<li>if the array contains 100 elements, then we need 100 comparisons</li>
<li>if the array contains 1000 elements, then we need 1000 comparisons</li>
<li>etc.</li>
</ul></li>
<li>The complexity is denoted <span class="math inline">\(O(n)\)</span> because it depends directly on the array size</li>
<li>If the array size doubles, then the number of comparisons will double</li>
<li>We say that the complexity <span class="math inline">\(O(n)\)</span> is <em>linear</em>, or the algorithm is <em>linear</em>, because the function representing the complexity is a linear function</li>
</ul>
<h4 id="average-case">Average case</h4>
<ul>
<li>The average case is a bit harder to express</li>
<li>We have to use more mathematics to analyse it</li>
<li>In this case, the analysis is not too difficult: on average, we would expect to go through about half the array to find the target element
<ul>
<li>sometimes, we find it close to the beginning, sometimes close to the middle, sometimes close to the end</li>
<li>on average, we find in the middle, because all the cases balance each other
<ul>
<li>that assuming we don’t search too often for elements not in the array, in which case we would need to go through all the elements more often</li>
</ul></li>
</ul></li>
<li>So its complexity is <span class="math inline">\(O(n/2)\)</span></li>
</ul>
<h4 id="question-are-on-and-on2-equal">Question: are <span class="math inline">\(O(n)\)</span> and <span class="math inline">\(O(n/2)\)</span> equal?</h4>
<ul>
<li><em>Answer</em>: <strong>yes</strong></li>
<li>The most important thing is not the exact number of operations, but the <strong>growth</strong> of the number operations relative to the problem size</li>
<li>No matter if we have <span class="math inline">\(O(n)\)</span> or <span class="math inline">\(O(n/2)\)</span>, if the array size doubles, then the number of operations will double</li>
<li>If the array size is multiplied by 100, then the number of operations will also be multiplied 100 in both cases</li>
<li>The growth is <em>linear</em> in both cases
<ul>
<li>the growth is represented by a linear function</li>
</ul></li>
<li>In the <span class="math inline">\(O(f(n))\)</span> notation, the constants are not important</li>
<li>So for the sequential search, <em>the worst case and the average case have the same complexity</em></li>
<li>The average and average cases are not always the same, but in many cases they are</li>
<li>Sometimes the best and the average cases are the same, but rarely</li>
</ul>
<h4 id="question-can-we-do-better">Question: <em>Can we do better?</em></h4>
<ul>
<li><strong>No</strong>, unless we add other conditions to the problem</li>
<li>If the array elements are sorted, then we can do better</li>
<li>We know that the smallest value is at the beginning, and the largest at the end
<ul>
<li>if we suppose an ascending (increasing) order of course</li>
</ul></li>
<li>We can take advantage of this condition to get a faster algorithm</li>
</ul>
<h3 id="binary-search">Binary search</h3>
<ul>
<li><p>If the array elements are sorted, then we can speed up the search</p></li>
<li><p>The <em>binary search</em> algorithm is a <em>divide and conquer</em> algorithm</p></li>
<li><p>The idea is to start by comparing the target element with the element in the middle of the array</p></li>
<li><p>If we are lucky, we will find the target in the middle immediately</p></li>
<li><p>In general, the target element will not be in the middle</p></li>
<li><p>In this case, because the array is sorted, we can eliminate half of the array from our search:</p>
<ul>
<li>if the target element is <strong>smaller</strong> than the element in the middle, we can eliminate the right half of the array from the search because all the elements in the right half are greater than or equal to the middle element, therefore they are greater than the target element</li>
<li>if the target element is <strong>larger</strong> than the element in the middle of the array, we can eliminate the left half of the array from the search because all the elements in the left half are less than or equal to the middle element, therefore they are less than the target element</li>
</ul></li>
<li><p>After eliminating half of the array, we repeat the procedure on the remaining half of the array</p></li>
<li><p><em>Notes</em>:</p>
<ul>
<li>the middle element is not always exactly in the middle of the array
<ul>
<li>if the array size is odd, then the middle element is exactly in the middle</li>
<li>if not, as presented in the following algorithm, the middle element will be just to the left of the exact middle
<ul>
<li>so, in this case, the left “<em>half</em>” will be a bit smaller than the right “<em>half</em>”</li>
</ul></li>
</ul></li>
<li>therefore, we don’t always remove exactly half of the elements</li>
<li>from the point of view of algorithm complexity, if we eliminate <span class="math inline">\(n/2\)</span> or <span class="math inline">\(n/2-1\)</span> or <span class="math inline">\(n/2+1\)</span> elements, it’s not going to make any difference
<ul>
<li>the effect of the <span class="math inline">\(-1\)</span> or the <span class="math inline">\(+1\)</span> is not going to be important when <span class="math inline">\(n\)</span> will be large</li>
</ul></li>
</ul></li>
</ul>
<pre><code>function binary_search(arr, target)
    // arr: array of comparable elements
    // target: the element we are looking for
    // n: length of arr
    // returns: the index where target was found, or -1
    
    // we keep note of the active part of the array with the following 3 variables
    start = 0
    middle = n / 2
    end = n - 1
    while start &lt;= end:
        if target == arr[middle]: // found!
            return middle
        if target &lt; arr[middle]: // the target is less than the middle element
                                 // we have to search in the left half
            end = middle - 1     // we move the end to the left of middle
        else:                    // the target is greater than the middle element
                                 // we have to search in the right half
            start = middle + 1   // we move the start to the right of middle
        middle = (start + end)/2
    return -1</code></pre>
<figure>
<img src="../images/binary_search.jpg" alt="" /><figcaption>Binary search</figcaption>
</figure>
<h3 id="binary-search-complexity">Binary search: Complexity</h3>
<h4 id="best-case-1">Best case</h4>
<ul>
<li>If we are lucky, like in the sequential search, we will find the target element on the first comparison with the middle element</li>
<li>So no matter how big the array is, we will find the target element in constant time</li>
<li>Complexity: <span class="math inline">\(O(1)\)</span></li>
</ul>
<h4 id="worst-case-1">Worst case</h4>
<ul>
<li>How many elements need to be compared to the target element if we are not lucky?
<ul>
<li>we need to express this number in terms of the array size <span class="math inline">\(n\)</span></li>
</ul></li>
<li>For the sequential search, the active array size (or the number of elements potentially equal to the target element) goes down by 1 at each step if the target element has not been found</li>
<li>For the binary search, how does the active array size decreases?
<ul>
<li>at each step we don’t find the target element, we eliminate approximately half the array</li>
<li>so the active array size is divided by 2 (approximately) at each step</li>
<li>the active array size after each step is approximately <span class="math inline">\(n/2\)</span> (if we didn’t find the target element)</li>
</ul></li>
<li>How many times can we cut the array in half in the algorithm before we have nothing left?
<ul>
<li>the loop is a <code>while</code> loop with the condition <code>start &lt;= end</code></li>
<li>the loop will terminate when <code>start &gt; end</code> (if we didn’t find the target element)</li>
<li>how can we get <code>start &gt; end</code>?
<ul>
<li>either when we do <code>start = middle + 1</code> or <code>end = middle - 1</code></li>
<li>if <code>start == end</code> and we do 1 of these 2 computations, then we will get <code>start &gt; end</code>
<ul>
<li>because at the previous step, we did <code>middle = (start + end)/2</code></li>
<li>and with <code>start == end</code>, <code>middle == start == end</code></li>
</ul></li>
</ul></li>
<li>when <code>start == middle == end</code>, there’s only 1 element left to check
<ul>
<li>either we find the target element and we return</li>
<li>or we get <code>start &gt; end</code></li>
</ul></li>
<li>so after getting only 1 element left to check, we are sure to stop the search</li>
<li>therefore, we divide <span class="math inline">\(n\)</span> by 2 many times until we get <span class="math inline">\(n = 1\)</span></li>
<li>the number of times we can divide the array size by 2 is <span class="math inline">\(log_{2}n=lg\ n\)</span></li>
</ul></li>
<li><strong>Reminder</strong>: if <span class="math inline">\(x = log_{2}n\)</span>, then <span class="math inline">\(2^x = n\)</span></li>
<li>The complexity of the binary search algorithm in the worst case is <span class="math inline">\(O(lg\ n)\)</span></li>
<li>We also say that the binary search algorithm is <strong>logarithmic</strong></li>
<li><em>Examples</em>:
<ul>
<li>if <span class="math inline">\(n = 64\)</span>, we will need to cut the array in half a maximum of 6 times because <span class="math inline">\(lg\ 64 = 6\)</span>
<ul>
<li>or if you prefer <span class="math inline">\(2^6 = 64\)</span></li>
</ul></li>
<li>if <span class="math inline">\(n = 128\)</span>, then we have <span class="math inline">\(lg\ 128 = 7\)</span></li>
<li>if <span class="math inline">\(n = 1024\)</span>, then we have <span class="math inline">\(lg\ 1024 = 10\)</span></li>
</ul></li>
<li>So every time the array size doubles, we add 1 step to the worst case
<ul>
<li>it’s much better than the sequential search, which doubles the number of steps whenever the array size doubles</li>
<li>but don’t forget that the binary search works only on sorted arrays, and that the sequential search works on all arrays, sorted or not</li>
</ul></li>
</ul>
<h4 id="average-case-1">Average case</h4>
<ul>
<li><p>On average, we expect to find the target element in the middle of the process</p></li>
<li><p>The number of times we divide the array in half is divided by 2</p></li>
<li><p>So on average, we have <span class="math inline">\(O(\frac{1}{2} lg\ n) = O(lg\ n)\)</span></p></li>
<li><p><em>Notes</em>:</p>
<ul>
<li>if the array size is not a power of 2, then we will not get exactly <span class="math inline">\(lg\ n\)</span> for the number of steps because we cannot divide exactly by 2 at each step. But in terms of algorithm complexity, it doesn’t matter, a <span class="math inline">\(+1\)</span> or <span class="math inline">\(-1\)</span> a few times in the process will not make a difference when <span class="math inline">\(n\)</span> is large</li>
<li>the most important thing is the growth of the number of steps when <span class="math inline">\(n\)</span> gets larger</li>
</ul></li>
</ul>
</body>
</html>
