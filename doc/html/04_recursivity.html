<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>04_recursivity</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="../html/lecture_notes.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js" type="text/javascript"></script>
</head>
<body>
<header id="title-block-header">
<h1 class="title">04_recursivity</h1>
</header>
<h1 id="recursivity">Recursivity</h1>
<h2 id="recursive-definitions">Recursive definitions</h2>
<ul>
<li>A recursive definition is a definition that depends on itself</li>
<li>In the computer science world, recursive definitions are common</li>
<li>Not only computer scientists use recursivity in their code, they use also recursive definitions to name their projects</li>
<li>Examples <a href="https://fr.wikipedia.org/wiki/Sigles_auto-référentiels">[1]</a>:
<ul>
<li><strong>GNU</strong>: <em>GNU’s not unix</em></li>
<li><strong>PHP</strong>: <em>PHP: Hypertext Preprocessor</em></li>
<li><strong>PIP</strong>: <em>PIP Installs Packages</em></li>
</ul></li>
</ul>
<h2 id="recursive-mathematical-functions">Recursive mathematical functions</h2>
<ul>
<li>In number theory, the whole numbers are defined by a recursive mathematical definition, more specifically using the successor function <span class="math inline">\(s(x)\)</span>
<ul>
<li>we suppose the existence of the number 0, and we apply <span class="math inline">\(s\)</span> the needed number of times
<ul>
<li><span class="math inline">\(s(0)\)</span></li>
<li><span class="math inline">\(s(s(0))\)</span></li>
<li><span class="math inline">\(s(s(0))\)</span></li>
<li>…</li>
</ul></li>
<li>by convention, we write
<ul>
<li><span class="math inline">\(s(0) = 1\)</span></li>
<li><span class="math inline">\(s(s(0)) = s(1) = 2\)</span></li>
<li><span class="math inline">\(s(s(s(0))) = s(2) = 3\)</span></li>
<li>…</li>
</ul></li>
</ul></li>
<li><span class="math inline">\(s\)</span> is a primitive recursive function, and is used as the base of the definition of addition</li>
</ul>
<h3 id="sum-of-positive-whole-numbers">Sum of positive whole numbers</h3>
<p><span class="math display">\[S=1+2+3+\cdots +(n-2)+(n-1)+n=\sum _{i=1}^{n}i={\frac {n(n+1)}{2}}\]</span></p>
<ul>
<li>Suppose we don’t know the formula to calculate the sum of positive whole numbers</li>
<li>We could calculate it in 2 ways:
<ul>
<li>iterative (with a loop)</li>
</ul>
<pre><code>function iterative_sum(n):
    sum = 0
    for i = 1 up to n:
        sum += i
    return sum</code></pre>
<ul>
<li>recursive (with a recursive function) <span class="math display">\[\sum _{i=1}^{n}i=n+\sum _{i=1}^{n-1}i\]</span></li>
</ul>
<pre><code>function recursive_sum(n):
    if n &lt; 1:
        return 0
    return n + recursive_sum(n-1)</code></pre></li>
</ul>
<h4 id="complexity-of-iterative_sum">Complexity of <code>iterative_sum</code></h4>
<ul>
<li>The complexity is simple to calculate
<ul>
<li>the operations <code>=</code> and <code>+=</code> are constant</li>
<li>we have a loop from 1 to <span class="math inline">\(n\)</span>, so there are <span class="math inline">\(n\)</span> iterations of the loop</li>
<li>this gives us a complexity of <span class="math inline">\(O(n)\)</span></li>
</ul></li>
<li>If we used the the formula, it would be even better because we would not have the loop
<ul>
<li>we would have only a multiplication, an addition, and a division</li>
<li>so we would have a complexity of <span class="math inline">\(O(1)\)</span></li>
</ul></li>
</ul>
<h4 id="complexity-of-recursive_sum">Complexity of <code>recursive_sum</code></h4>
<ul>
<li>There’s no loop in this function, but that doesn’t mean there isn’t code repetition</li>
<li>To analyse a recursive function, we need to count how many times the recursive function is called</li>
<li>To simplify the notation, suppose the function <code>recursive_sum</code> is named <span class="math inline">\(f\)</span></li>
<li>The first call to the function is <span class="math inline">\(f(n)\)</span>, and if <span class="math inline">\(n \geq 1\)</span>, then we call <span class="math inline">\(f(n-1)\)</span></li>
<li>If <span class="math inline">\(n-1 \geq 1\)</span>, then we call <span class="math inline">\(f(n-2)\)</span>, and we keep repeating this process until we call <span class="math inline">\(f(0)\)</span>
<ul>
<li>there won’t be any recursive call with <span class="math inline">\(f(0)\)</span></li>
<li>the sequence of recursive calls will stop with <span class="math inline">\(f(0)\)</span></li>
<li>we say that <span class="math inline">\(n&lt;1\)</span> is a base case, or a stop condition for the recursive calls</li>
<li><em>without any base case</em> (there could be many), we could have a situation with <strong>infinite recursivity</strong>
<ul>
<li>actually, in practice, there’s a limit on the number of recursive calls a program can make, so we cannot have an infinite number of recursive calls, and the program will crash after a while</li>
</ul></li>
</ul></li>
<li>Using the mathematical notation, we would write the function <code>recursive_sum</code> (using the shortcut <span class="math inline">\(f\)</span> another time): <span class="math display">\[f(n) = \begin{cases} 0 &amp; n &lt; 1 \\ n+f(n-1) &amp; n \geq 1 \end{cases}\]</span></li>
<li>We will call the function <span class="math inline">\(n+1\)</span> times: <span class="math inline">\(f(n), f(n-1), f(n-2), ..., f(1), f(0)\)</span></li>
</ul>
<figure>
<img src="../images/recursive_sum.jpg" alt="" /><figcaption>Recursive sum</figcaption>
</figure>
<ul>
<li>We will have <span class="math inline">\(n+1\)</span> function calls, each time with a constant number of operations constant (which do not depend on <span class="math inline">\(n\)</span>):
<ul>
<li>after the <code>if</code> which compares <code>n</code> to 1 (constant) either
<ul>
<li>return the value 0 (constant)</li>
<li>make an addition with the result of the recursive call, then return a value (also constant)</li>
</ul></li>
</ul></li>
<li>The complexity will be <span class="math inline">\(O(n)\)</span>, as for the iterative sum</li>
<li>From the theoretical point of vue, their complexities are identical</li>
<li>But from the practical point of vue, the iterative sum will be faster because calling a function is more expensive than looping</li>
<li>Whenever possible, we will use the iterative version of algorithm</li>
<li>But sometimes, expressing an algorithm in an iterative way is difficult, and expressing it in a recursive way is much easier, so the recursive version will be preferred in these cases</li>
</ul>
<h3 id="function-call-stack">Function call stack</h3>
<ul>
<li>Every time a function (or a method) is called, in a recursive way or not, space is reserved this call on the <strong>call stack</strong></li>
<li>Space is allocated for the function parameters and also for the local variables</li>
<li>Information is also kept to know where to return to (where the function was called)</li>
<li>Every function call has its own space on the call stack, so if we call the same function multiple times in a recursive manner, each call will have its own copy of each parameter and each local variable</li>
<li>Therefore, each function call doesn’t interfere with the other one, unless the parameters are pointers or references to objects located somewhere else in memory, and the different function calls are given the same pointers or references</li>
</ul>
<figure>
<img src="../images/call_stack.jpg" alt="" /><figcaption>Function call stack: recursive sum</figcaption>
</figure>
<h3 id="fibonacci-numbers">Fibonacci numbers</h3>
<ul>
<li><p>The <em>Fibonacci numbers</em> sequence is defined in a recursive way on positive integers: <span class="math display">\[F_n = \begin{cases} 0 &amp; n = 0 \\ 1 &amp; n = 1 \\ F_{n-1}+F_{n-2} &amp; n &gt; 1 \end{cases}\]</span></p></li>
<li><p>To calculate <span class="math inline">\(F_5\)</span>, we need to calculate <span class="math inline">\(F_4\)</span> and <span class="math inline">\(F_3\)</span></p>
<ul>
<li>but to calculate <span class="math inline">\(F_4\)</span>, we also need to calculate <span class="math inline">\(F_3\)</span> and <span class="math inline">\(F_2\)</span></li>
<li>and for <span class="math inline">\(F_3\)</span>, we need <span class="math inline">\(F_2\)</span> and <span class="math inline">\(F_1\)</span></li>
<li>and we need to continue and apply the same principle, which can become complicated, especially when we start with a value larger than 5</li>
</ul></li>
<li><p>The Fibonacci numbers sequence is often denoted <span class="math inline">\(f(n)\)</span> instead of <span class="math inline">\(F_n\)</span></p></li>
</ul>
<figure>
<img src="../images/Animation_Fibonacci.gif" alt="" /><figcaption>Animation Fibonacci</figcaption>
</figure>
<p>EloiBerlinger / CC BY-SA (https://creativecommons.org/licenses/by-sa/4.0)</p>
<ul>
<li>The pseudocode is very simple, it is translated directly from the recursive definition</li>
</ul>
<pre><code>function fibonacci_recursive(n):
    if n == 0 or n == 1:
        return n
    return fibonacci_recursive(n-1) + fibonacci_recursive(n-2)</code></pre>
<ul>
<li><em>Notes</em>:
<ul>
<li>before we can execute the addition on the last line, both recursive calls have to complete and return their value</li>
<li>so the first recursive call <code>fibonacci_recursive(n-1)</code> will be completely executed before the second recursive call <code>fibonacci_recursive(n-2)</code> can proceed and be computed</li>
<li>and only then the addition can be calculated, and the result can be returned after that</li>
</ul></li>
</ul>
<figure>
<img src="../images/fibo5_calls.jpg" alt="" /><figcaption>Fibonacci numbers (n=5): recursive calls</figcaption>
</figure>
<figure>
<img src="../images/fibo5_return.jpg" alt="" /><figcaption>Fibonacci numbers (n=5): return</figcaption>
</figure>
<p><em>Note</em>: the order in which the recursive calls are made is given in the black circles</p>
<h4 id="repetitions">Repetitions</h4>
<ul>
<li>The recursive definition of the Fibonacci numbers is simple</li>
<li>But it is not efficient</li>
<li>There are too many repetitions: the function with the same value of <span class="math inline">\(n\)</span> appears multiple times in the recursive calls tree</li>
<li>For example, we can represent the recursive call tree for <span class="math inline">\(F_6\)</span> in terms of the <span class="math inline">\(F_5\)</span> recursive call tree
<ul>
<li>we create a new root for <span class="math inline">\(F_6\)</span></li>
<li>we copy the <span class="math inline">\(F_5\)</span> tree to the left of <span class="math inline">\(F_6\)</span></li>
<li>and we copy the <span class="math inline">\(F_4\)</span> tree to the right of <span class="math inline">\(F_6\)</span></li>
</ul></li>
<li>So to calculate <span class="math inline">\(F_6\)</span>, we repeat all the calculations of <span class="math inline">\(F_5\)</span>, plus all the calculations of <span class="math inline">\(F_4\)</span>, which were themselves includes in <span class="math inline">\(F_5\)</span>!</li>
</ul>
<figure>
<img src="../images/fibo6_calls.jpg" alt="" /><figcaption>Fibonacci numbers (n=6): recursive calls</figcaption>
</figure>
<figure>
<img src="../images/fibo6_repetitions.jpg" alt="" /><figcaption>Fibonacci numbers (n=6): repetitions</figcaption>
</figure>
<h4 id="complexity-of-fibonacci_recursive">Complexity of <code>fibonacci_recursive</code></h4>
<ul>
<li><p>The analysis of a recursive algorithm depends on the number of recursive calls of the recursive function</p></li>
<li><p>Let <span class="math inline">\(A_i\)</span> be the number of times <span class="math inline">\(f(i)\)</span> is called during the execution of <span class="math inline">\(f(n)\)</span>, then we have <span class="math display">\[A_i = \begin{cases} 1
&amp; i = n\ \text{ou}\ i = n-1 \\ A_{i+1}+A_{i+2} &amp; 0 &lt; n &lt; n-1 \\ A_{2} &amp; n = 0 \end{cases}\]</span></p></li>
<li><p><span class="math inline">\(f(i)\)</span> is called by only <span class="math inline">\(f(i+1)\)</span> and <span class="math inline">\(f(i+2)\)</span>, therefore the number of times <span class="math inline">\(f(i)\)</span> is called is the sum of the number of times <span class="math inline">\(f(i+1)\)</span> and <span class="math inline">\(f(i+2)\)</span> are called</p>
<ul>
<li>with the exceptions of <span class="math inline">\(f(n)\)</span> <span class="math inline">\(f(n-1)\)</span>, which are called exactly once each</li>
<li>and <span class="math inline">\(f(0)\)</span> is called only by <span class="math inline">\(f(2)\)</span></li>
</ul></li>
<li><p>For example, when executing <span class="math inline">\(f(6)\)</span>:</p>
<ul>
<li><span class="math inline">\(A_{6} = 1\)</span></li>
<li><span class="math inline">\(A_{5} = 1\)</span></li>
<li><span class="math inline">\(A_{4} = 1 + 1 = 2\)</span></li>
<li><span class="math inline">\(A_{3} = 1 + 2 = 3\)</span></li>
<li><span class="math inline">\(A_{2} = 2 + 3 = 5\)</span></li>
<li><span class="math inline">\(A_{1} = 5 + 3 = 8\)</span></li>
<li><span class="math inline">\(A_{0} = 5\)</span></li>
</ul></li>
<li><p>Except for <span class="math inline">\(A_{0}\)</span>, the numbers <span class="math inline">\(A_i\)</span> are themselves the numbers in the Fibonacci sequence!</p></li>
<li><p>The number of recursive calls is then <span class="math display">\[(F_1 + F_2 + F_3 + \cdots + F_{n-1} + F_{n}) + F_{n-1} = F_{n-1} + \sum _
{i=1}^{n}F_i = O(2^n)\]</span></p></li>
<li><p>The complexity is <strong>exponential</strong>!</p></li>
<li><p><em>Notes</em>:</p>
<ul>
<li>the complete mathematical proof of this exponential complexity is too complicated to be presented here in an introduction to recursive functions complexity</li>
<li>the main idea is that every time we add 1 to the parameter <span class="math inline">\(n\)</span>, we basically double the number of calculations necessary to calculate the answer</li>
<li>if we go from <span class="math inline">\(F_n\)</span> to <span class="math inline">\(F_{n+1}\)</span>, we recalculate everything we did in <span class="math inline">\(F_{n}\)</span> and in <span class="math inline">\(F_{n-1}\)</span>, so we almost double the work</li>
</ul></li>
</ul>
<h4 id="fibonacci-iterative-version">Fibonacci, iterative version</h4>
<ul>
<li>The <em>memoization</em> technique
<ul>
<li>to avoid repeating all the same calculations many times, we can “<em>write a memo</em>” to remember the result from a previous calculation</li>
<li>we can reuse the result of a previous calculation whenever we need it, without recalculating it every time</li>
</ul></li>
<li>For example, if we want to calculate <span class="math inline">\(F_6 = F_5 + F_4\)</span>, we can calculate <span class="math inline">\(F_4\)</span> first, and then reuse this result when we calculate <span class="math inline">\(F_5\)</span>
<ul>
<li>but both <span class="math inline">\(F_5\)</span> and <span class="math inline">\(F_4\)</span> need <span class="math inline">\(F_3\)</span>, so we also need to avoid calculating <span class="math inline">\(F_3\)</span> many times</li>
</ul></li>
<li>The simplest thing to do is to start calculating the <span class="math inline">\(F_i\)</span> from “the bottom”, in other words start with the small values of <span class="math inline">\(F_i\)</span>
<ul>
<li><span class="math inline">\(F_0 = 0\)</span> and <span class="math inline">\(F_1 = 1\)</span> are already known</li>
<li>then calculate <span class="math inline">\(F_2\)</span> from <span class="math inline">\(F_0\)</span> and <span class="math inline">\(F_1\)</span></li>
<li>then calculate <span class="math inline">\(F_3\)</span> from <span class="math inline">\(F_1\)</span> and <span class="math inline">\(F_2\)</span></li>
<li>we don’t need to keep all the values of <span class="math inline">\(F_i\)</span>, only the last 2</li>
</ul></li>
</ul>
<pre><code>function fibonacci_iterative(n):
    f_i2 = 0  // for F i-2: at the beginning F0, then F1, F2, F3, ...
    f_i1 = 1  // for F i-1: at the beginning, F1, then F2, F3, F4, ...
    if n == 0 or n == 1:
        return n
    for i = 2 up to n:
        f_i = f_i1 + f_i2
        f_i2 = f_i1       // for the next iteration
        f_i1 = f_i        // f_i1 becomes f_i2, and f_i becomes f_i1
    return f_i</code></pre>
<figure>
<img src="../images/fibo6_iterative.jpg" alt="" /><figcaption>Fibonacci numbers (n=6): iterative version</figcaption>
</figure>
<h4 id="complexity-of-fibonacci_iterative">Complexity of <code>fibonacci_iterative</code></h4>
<ul>
<li>The analysis of this iterative version of the algorithm is much simpler than the recursive version</li>
<li>We have a loop with <span class="math inline">\(n-1\)</span> iterations, and all other operations are constant (assignments, comparisons and addition), so we have a linear complexity <span class="math inline">\(O(n)\)</span></li>
<li>A linear complexity is by far better than an exponential complexity
<ul>
<li>the execution time of <span class="math inline">\(F_{100}\)</span> will be about 10 times larger than the execution time of <span class="math inline">\(F_{10}\)</span> with the iterative version</li>
<li>but with the recursive version, we will have <span class="math inline">\(O(2^{100})=O(2^{90}\times 2^{10})\)</span>, so <span class="math inline">\(F_{100}\)</span> will be about <span class="math inline">\(2^{90}=1237940039285380274899124224\approx 1,2\times 10^{27}\)</span> slower than <span class="math inline">\(F_{10}\)</span>!</li>
</ul></li>
<li>Another problem will be the memory used by the recursive version: there’s a good chance the call stack, which has a limited amount of memory allocated to it, will run out of space, and we will get some kind of <code>StackOverflow</code> exception
<ul>
<li>with the iterative version, we only need a fixed (constant) amount of memory</li>
</ul></li>
</ul>
<h2 id="recursive-sequential-search">Recursive sequential search</h2>
<pre><code>function sequential_search_recursive(arr, target, start)
    // arr: array of comparable (or least equatable) elements
    // target: the element we are looking for
    // start: start searching at that index
    // n: length of arr
    // returns: the index where target was found, or -1
    if start &gt;= n:
        return -1
    if target == arr[start]:
        retourn start
    return sequential_search_recursive(arr, target, start + 1)</code></pre>
<figure>
<img src="../images/rec_seq_search.jpg" alt="" /><figcaption>Recursive sequential search</figcaption>
</figure>
<h2 id="recursive-binary-search">Recursive binary search</h2>
<pre><code>function binary_search_recursive(arr, target, start, end)
    // arr: array of comparable (or least equatable) elements
    // target: the element we are looking for
    // start: start searching at that index
    // end: search up to this index (inclusive)
    // n: length of arr
    // returns: the index where target was found, or -1
    if start &gt; end:
        return -1
    middle = (start + end)/2
    if target == arr[middle]: // found!
        return middle
    if target &lt; arr[middle]:  
        return binary_search_recursive(arr, target, start, middle - 1)
    return binary_search_recursive(arr, target, middle + 1, end)</code></pre>
<figure>
<img src="../images/rec_binary_search.jpg" alt="" /><figcaption>Recursive binary search</figcaption>
</figure>
</body>
</html>
