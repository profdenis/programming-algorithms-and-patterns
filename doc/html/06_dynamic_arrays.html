<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>06_dynamic_arrays</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="../html/lecture_notes.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js" type="text/javascript"></script>
</head>
<body>
<header id="title-block-header">
<h1 class="title">06_dynamic_arrays</h1>
</header>
<h1 id="dynamic-arrays">Dynamic Arrays</h1>
<h2 id="implementing-the-listt-interface-with-a-variable-length-array">Implementing the <code>List&lt;T&gt;</code> interface with a variable length array</h2>
<ul>
<li><a href="https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html">Interface <code>List&lt;T&gt;</code></a></li>
<li><strong>List</strong>: ordered collection of objects that can be accessed by their index</li>
<li>Key methods:
<ul>
<li><code>add(element)</code>: add an element at the end of the list</li>
<li><code>add(index, element)</code>: add (or insert) an element at a specific index in the list</li>
<li><code>indexOf</code>: find the index of an element in the list</li>
<li><code>remove(element)</code> and <code>remove(index)</code>: find and remove an element from the list, and remove the element at the specified index</li>
</ul></li>
</ul>
<h2 id="basic-principles-of-the-arraylist">Basic principles of the <em>ArrayList</em></h2>
<ul>
<li>Generic class, with a generic type for the elements in the list</li>
<li>Contains an array with elements of the generic type: <code>T[] array</code></li>
<li>2 properties:
<ul>
<li><code>count</code>: the number of elements in the list</li>
<li><code>capacity</code>: the array length</li>
</ul></li>
<li><code>count</code> <span class="math inline">\(\leq\)</span> <code>capacity</code>
<ul>
<li>there will normaly be unused space in the array</li>
<li>for example, if the capacity is 10 and the number of elements in the list is 6, there will be 4 empty cells in the arrays</li>
<li>the empty cells are always at the end, there cannot be empty cells (or holes) in the middle or at the beginning of the array
<ul>
<li>unless the number of elements is 0, and in this case all cells are empty</li>
</ul></li>
</ul></li>
</ul>
<figure>
<img src="../images/ArrayList_free_space.jpg" alt="" /><figcaption>Dynamic arrays - free space</figcaption>
</figure>
<h3 id="adding-and-inserting-elements-in-an-arraylist">Adding and inserting elements in an ArrayList</h3>
<ul>
<li>The <code>add(T)</code> method adds an element at the end of the list, so at the end of the active part of the array
<ul>
<li>we assume, for now, there’s enough space in the array to add the element</li>
</ul></li>
</ul>
<pre><code>method add(element):
    array[count] = element
    count++</code></pre>
<figure>
<img src="../images/ArrayList_add.jpg" alt="" /><figcaption>Dynamic arrays - add an element at the end</figcaption>
</figure>
<ul>
<li>The <code>add(index, element)</code> method insert an element at the given index
<ul>
<li>this method is more complicated than <code>add(element)</code>, because it needs to make space for the new element</li>
<li>we do not replace an element by another one, we instead move elements to the right to make space for the new element</li>
</ul></li>
</ul>
<pre><code>method add(index, element):
    // move elements to the right to make space for the new element
    for j = index up to count-1:
        array[j+1] = array[j]
    array[index] = element
    count++</code></pre>
<figure>
<img src="../images/ArrayList_insert.jpg" alt="" /><figcaption>Dynamic arrays - insert an element</figcaption>
</figure>
<h2 id="what-happens-when-theres-no-more-space-in-the-array">What happens when there’s no more space in the array?</h2>
<ul>
<li>In other words, what will happen when we call <code>add</code> (any version) and that <code>count == capacity</code>?
<ul>
<li><em>Answer</em>: we need to increase the array capacity</li>
<li>that’s why an ArrayList is often referred to as a <strong>dynamic array</strong>
<ul>
<li>the array capacity is increased as necessary</li>
</ul></li>
</ul></li>
</ul>
<pre><code>method increaseCapacity(newCapacity):
    temp = new array of size newCapacity
    copy the current array elements into the temp array
    array = temp</code></pre>
<figure>
<img src="../images/ArrayList_increase_capacity.jpg" alt="" /><figcaption>Dynamic arrays - increase capacity</figcaption>
</figure>
<h4 id="how-to-choose-the-new-capacity">How to choose the new capacity</h4>
<ul>
<li>Obviously, the new capacity has to be larger than the old one, if not there won’t be enough space for all the elements</li>
<li>We should avoid increasing the capacity by only 1 at each step because to allocate a new array and copy all the elements is expensive
<ul>
<li>we don’t want to call <code>increaseCapacity</code> every time we call <code>add</code></li>
</ul></li>
<li>but we don’t want to allocate an array that is too big because there will be too much wasted space</li>
<li>2 common techniques:
<ul>
<li>increase the capacity by a constant, for example by 10 elements, each time we call <code>increaseCapacity</code></li>
<li>or we can multiply the capacity by 2 each time we call <code>increaseCapacity</code></li>
</ul></li>
<li>If we want to minimize the wasted space without calling <code>increaseCapacity</code> each time, we could choose the first option</li>
<li>If we want to minimize the complexity (the number of operations), then the second is better</li>
<li>In <code>add</code>, we have to start by checking if there’s enough space for the new element, and if not, we call <code>increaseCapacity</code> before adding the new element</li>
</ul>
<h3 id="indexof"><code>indexOf</code></h3>
<ul>
<li>This method is using the sequential search algorithm</li>
<li>We have to be careful to search only active part of the array, by searching only the first <code>count</code> elements, and not all the elements in the array</li>
</ul>
<h3 id="removing-elements">Removing elements</h3>
<ul>
<li>The <code>remove</code> methods are the opposite of the <code>add</code> methods
<ul>
<li><code>remove(index)</code>: remove an element at a specific index
<ul>
<li>very similar to <code>add(index, element)</code>, except that we remove an element instead of adding it</li>
<li>we cannot leave <em>holes</em> in the array, so we need to shift to the left the elements occurring after the elements we want to remove
<ul>
<li>unless the element we want to remove is the last one</li>
</ul></li>
</ul></li>
<li><code>remove(element)</code>: removes an element
<ul>
<li>here, the index is not specified</li>
<li>so we need to find the index of the element to remove with <code>indexOf</code></li>
<li>and if we find it, we remove it with <code>remove(index)</code></li>
<li>we return <em>true</em> if the element has been removed, or <em>false</em> otherwise</li>
</ul></li>
</ul></li>
</ul>
<figure>
<img src="../images/ArrayList_remove.jpg" alt="" /><figcaption>Dynamic arrays - remove an element</figcaption>
</figure>
<pre><code>method remove(index):
    for j = index+1 up to count-1:
        tab[j-1] = tab[j]
    count--
    
method remove(item):
    index = indexOf(item)
    if index &lt; 0:
        return false
    remove(index)
    return true</code></pre>
<h2 id="iterators">Iterators</h2>
<ul>
<li><em>Iterators</em> are used to go through all the elements of a collection</li>
<li>If the collection keeps the elements in order, as in an array list and in a list in general, we can iterate (or loop) through the list elements by position (or index) with a loop accessing the elements by index with the <code>get</code> method</li>
</ul>
<pre><code>for i = 0 up to n-1:
    f(list.get(i))</code></pre>
<ul>
<li>In general, a collection is not necessarily ordered (the elements are not stored as a sequence), so we cannot access the elements by position</li>
<li>Also, some implementations of the <code>List</code> interface, more specifically the linked list implementations, do support access by position, but each access will a complexity of <span class="math inline">\(O(n)\)</span> on average, so it’s not efficient</li>
<li>We can use a kind of <code>for each</code> loop to iterate through the elements
<ul>
<li>internally, the compiler will transform the <code>for each</code> loop into a while loop using an iterator</li>
</ul></li>
</ul>
<pre><code>for each element x in the list:
    f(x)</code></pre>
<ul>
<li>An iterator for an array list should contain, at least, the following:
<ul>
<li>a reference to the array list</li>
<li>a current index</li>
<li>a method to check if there’s a next element: <code>hasNext</code> in Java</li>
<li>a method to get the next element and move the iterator the following element: <code>next</code> in Java</li>
</ul></li>
<li>Normally, an iterator starts before the first element, and the first call to <code>hasNext</code> will check if there’s a first element (i.e. if the list is not empty)</li>
<li>Successive calls to <code>next</code> return the next element and move the current index to the second, third, fourth, … elements</li>
<li>When there are no more elements, <code>hasNext</code> returns <em>false</em>, otherwise it returns <em>true</em></li>
<li>If the list is empty, the first call to <code>next</code> will throw an exception. An exception will also be thrown by <code>next</code> if there are no more elements left</li>
</ul>
</body>
</html>
