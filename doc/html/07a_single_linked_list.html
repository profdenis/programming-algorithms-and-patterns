<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>07a_single_linked_list</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="../html/lecture_notes.css" />
</head>
<body>
<header id="title-block-header">
<h1 class="title">07a_single_linked_list</h1>
</header>
<h1 id="linked-lists">Linked lists</h1>
<p>https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html</p>
<ul>
<li>The goal is to implement the same <code>List</code> interface as implemented by the <code>ArrayList</code> class, but without using an array</li>
<li>We use nodes containing list elements, plus at least 1 reference to the next node</li>
<li>The nodes in a <em>single linked list</em> contain only 1 reference to the next node</li>
<li>The nodes in a <em>double linked list</em> contain 2 references to other nodes: a reference to the previous node and a reference to the next node</li>
</ul>
<h2 id="single-linked-list">Single linked list</h2>
<ul>
<li>The <code>SingleLinkedList&lt;T&gt;</code> class contains 2 references to nodes:
<ul>
<li><code>first</code>: the first node in the list</li>
<li><code>last</code>: the last node in the list</li>
</ul></li>
<li>It’s not mandatory to keep a reference to the last node, but it can facilitate some operations, including the addition of an element at the end of the list</li>
<li>We also keep the number of nodes in the list in the <code>count</code> field to avoid going through the list to count the number of nodes</li>
<li>A <em>node</em> in a single linked list (internal class <code>Node</code>) contains
<ul>
<li>an element of type <code>T</code></li>
<li>a reference to the next node</li>
</ul></li>
</ul>
<figure>
<img src="../images/sll_4nodes.jpg" alt="" /><figcaption>Single linked list with 4 nodes</figcaption>
</figure>
<h3 id="single-linked-list-methods">Single linked list methods</h3>
<h4 id="add-an-element-at-the-end-of-the-list">Add an element at the end of the list</h4>
<pre><code>method addElement(element):
    // if the list is empty, both first and last are null
    if last is null:
        first = last = new Node(element)
    else:
        // the new node must go after the current last node
        last.next = new Node(element)
        // we move the reference to the last node to the new last node
        last = last.next
    count++</code></pre>
<figure>
<img src="../images/sll_add.jpg" alt="" /><figcaption>Single linked list - add at the end</figcaption>
</figure>
<ul>
<li>If we didn’t have a reference to the last node, then we would need to go through all the nodes in the list from the first node to find the last node where we need to add the new node</li>
<li>In a linked list, as opposed to an array, we don’t have a direct access (by index) to an element</li>
<li>We always need to start from the first node, then follow the references to the next nodes until we find the element we want</li>
<li>For example, to find the last node, if we didn’t have a reference, we would need to do:</li>
</ul>
<pre><code>current = first
while current.next is not null:
    current = current.next</code></pre>
<h4 id="add-an-element-at-the-beginning-of-the-list">Add an element at the beginning of the list</h4>
<pre><code>method addBeginning(element):
    // if the list is empty, then first and last are null
    if last is null:
        first = last = new Node(element)
    else:
        // the new first node must go before the current first node
        first = new Node(element, first)
    count++</code></pre>
<figure>
<img src="../images/sll_add_beginning.jpg" alt="" /><figcaption>Single linked list - add at the beginning</figcaption>
</figure>
<h4 id="add-an-element-at-a-given-index">Add an element at a given index</h4>
<ul>
<li>All the methods based on an index necessitate going through the nodes starting from the first nodes, similarly to what has been described above, but by counting the nodes to stop on the correct node
<ul>
<li>exceptions: if the index is equal to 0 or to <code>count</code>, we can call the methods acting directly on the first or last nodes</li>
</ul></li>
<li>To avoid repeating the same loop in many places, we can define a method to find a node at a given index</li>
</ul>
<pre><code>method findNode(index):
    current = first
    for i = 0; i &lt; index and current != null; i++:
        current = current.next
    return current</code></pre>
<ul>
<li>If the index is too big, then <code>null</code> will be returned</li>
</ul>
<figure>
<img src="../images/sll_find_node.jpg" alt="" /><figcaption>Single linked list - find a node by index</figcaption>
</figure>
<pre><code>method add(index, element):
    if index == count:
        add(element)
    else if index == 0:
        addBeginning(element)
    else:
        previous = findNode(index - 1)
        previous.next = new Node(element, previous.next)
        count++</code></pre>
<ul>
<li>It is important to note that we need to find the node preceding the node at the specified index because it is not possible to go back to a previous node
<ul>
<li>if we find the node at index <code>i</code>, we cannot adjust the <code>next</code> reference of the previous node (index <code>i-1</code>) to the new node</li>
<li><code>previous.next</code> refers to the node with index <code>i</code>
<ul>
<li>the new node must be placed at this location</li>
<li>so in <code>new Node(element, previous.next)</code>, <code>previous.next</code> refers to the old node at index <code>i</code> (which will become the node at index <code>i+1</code>)</li>
<li>and the new node is placed at after the node at index <code>i-1</code></li>
</ul></li>
</ul></li>
</ul>
<figure>
<img src="../images/sll_insert.jpg" alt="" /><figcaption>Single linked list - insert</figcaption>
</figure>
<h4 id="indexof-method"><code>indexOf</code> method</h4>
<ul>
<li>This method is actually a sequential search, but on nodes instead of an array</li>
<li><code>indexOf</code> is similar to <code>findNode</code>
<ul>
<li><code>findNode</code> counts the nodes from the first node and returns <code>i</code>th node</li>
<li><code>indexOf</code> counts the number of nodes from the first node until it finds the given element, and then returns the index of the node</li>
</ul></li>
</ul>
<pre><code>method indexOf(element):
    current = first
    index = 0
    while current != null:
        if current.element.equals(element):
            return index
        current = current.next
        index++
    return -1</code></pre>
<ul>
<li>We start at the beginning
<ul>
<li>with <code>current</code> referring to the first node</li>
<li>and with <code>index</code> equal to 0 (the first node is at index 0)</li>
</ul></li>
<li>And while we haven’t found the given element and that we haven’t reached the end of the list
<ul>
<li>we move <code>current</code> to the next node</li>
<li>and we add 1 to <code>index</code></li>
</ul></li>
<li>If we reach the end of the list without finding the element, we return -1</li>
</ul>
<h4 id="removeindex"><code>remove(index)</code></h4>
<ul>
<li>The <code>remove(index)</code> method must find the node at the right index so that we can remove it</li>
<li>As for <code>add(index, element)</code>, we need to find the node before the node to process to be able to adjust the references correctly
<ul>
<li>so we use the <code>findNode</code> method once again</li>
</ul></li>
<li>If we need to remove the first node, there’s no node before it, so we need to make it a special case</li>
<li>If we need to remove the last node, then we need to use the <code>findNode</code> method also because starting from <code>last</code>, there’s no way to go back and reposition <code>last</code> to the node just before the last
<ul>
<li>we absolutely need to use <code>findNode</code> to find the node before the last node, which will become the new last node</li>
</ul></li>
</ul>
<pre><code>methode remove(index):
    if index == 0:
        temp = first
        first = first.next
        temp.next = null
        count--
    else:
        previous = findNode(index - 1)
        if last == previous.next:
            last = previous
        temp = previous.next
        previous.next = temp.next
        temp.next = null
        count--</code></pre>
<figure>
<img src="../images/sll_remove.jpg" alt="" /><figcaption>Single linked list - remove</figcaption>
</figure>
<h4 id="removeelement"><code>remove(element)</code></h4>
<ul>
<li>Instead of using <code>findNode</code> as in <code>remove(index)</code>, <code>remove(element)</code> executes a sequential search to find the correct node</li>
<li>Once we find the correct node, we remove it</li>
<li>But to remove a node, we need to know which node precedes it, so in the sequential search, we need to adjust 2 references: <code>previous</code> and <code>current</code>
<ul>
<li>if <code>previous</code> is null, we remove the first node</li>
<li>we also need to be careful to update <code>last</code> if we are removing the last node</li>
</ul></li>
</ul>
<pre><code>method remove(element):
    current = first
    previous = null
    while current is not null:
        if current.element.equals(element):
            if previous == null: // if we are at the beginning
                first = current.next
            else:
                previous.next = current.next
            if last == current:
                last = previous
            current.next = null
            count--
            return true
            
        previous = current;
        current = current.next;
        
    return false</code></pre>
<h2 id="iterators">Iterators</h2>
<ul>
<li><p>An <em>iterator</em> is an object used to iterate through all the objects of a collection</p></li>
<li><p>An iterator is used in <em>for each</em> loops, or in <code>while</code> loops using the <code>hasNext</code> and the <code>next</code> methods directly, to loop over the elements of a collection, without knowing the implementation details of the data structure</p></li>
<li><p>For a linked list (single or double), an iterator holds a reference to the next node to be processed in the list</p></li>
<li><p>We could also keep a reference to the whole list and to the previous node that was processed if we wanted to allow for list modifications through the iterator</p></li>
<li><p>Usually, an iterator logically starts <strong>before</strong> the first element</p>
<ul>
<li>when we call <code>hasNext</code>, we check if there is at least 1 more node to process
<ul>
<li>the first time, it will actually check if the first node is not null</li>
<li>in other words, it will return true if there’s at least 1 element in the list</li>
</ul></li>
<li>then we call <code>next</code>, which not only returns the next element, but also moves the current node reference to the next one
<ul>
<li>the first call to <code>next</code> will return the first element</li>
</ul></li>
</ul></li>
<li><p>An iterator returns the list elements, not the list nodes, to preserve the encapsulation and loose coupling principles</p></li>
</ul>
</body>
</html>
