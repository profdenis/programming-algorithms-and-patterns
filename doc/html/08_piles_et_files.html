<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>08_piles_et_files</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="../html/lecture_notes.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js" type="text/javascript"></script>
</head>
<body>
<header id="title-block-header">
<h1 class="title">08_piles_et_files</h1>
</header>
<h1 id="stack-and-queues">Stack and queues</h1>
<ul>
<li>A <em>stack</em> is a data structure of type <strong>first in, last out (FILO)</strong> (or <em>first come, last serve</em>), or equivalently, of type <strong>last in, first out (LIFO)</strong></li>
<li>A <em>queue</em> is a data structure of type <strong>first in, first out (FIFO)</strong> (or <em>first come, first serve</em>), or equivalently, of type <strong>last in, last out (LILO)</strong></li>
<li>These types define in which order the elements will be added and removed from the data structure</li>
<li>We don’t add and we don’t remove elements at/from specific positions in these structures, we instead follow the FILO or LIFO vs. FIFO or LILO principles of each structure</li>
</ul>
<h2 id="stacks">Stacks</h2>
<ul>
<li><p>We always interact with the top of the stack:</p>
<ul>
<li><strong>push</strong>: add an element on the top of the stack</li>
<li><strong>pop</strong>: remove an element from the top of the stack</li>
</ul></li>
<li><p>That’s why a stack is a <strong>last in, first out (LIFO)</strong> data structure because the last element we added to the stack will be the first one to be removed</p></li>
<li><p>At the opposite, the first element we added to the stack will be at the bottom, below all the other elements, therefore it will be the last one to be removed, all the other elements will have to be removed before we can remove the first element we added</p></li>
<li><p>We cannot add elements to a stack in the middle or under the stack, we can only added on the top of the stack</p></li>
<li><p>Same thing about removing elements, we can only remove elements from the top of the stack</p></li>
<li><p>The most common way to visualise a stack is by using an array, but we don’t have to implement a stack with an array: we can also use a linked list</p></li>
<li><p>To help visualise a stack, the array is represented vertically, with index 0 at the bottom, and the top of the stack will be higher, at index <code>t</code> (or <code>top</code>), depending on the number of elements in the stack</p></li>
</ul>
<figure>
<img src="../images/stack_push.jpg" alt="" /><figcaption>Stack - push</figcaption>
</figure>
<figure>
<img src="../images/stack_pop.jpg" alt="" /><figcaption>Stack - pop</figcaption>
</figure>
<h2 id="implementing-a-stack">Implementing a stack</h2>
<ul>
<li><p>We can either use an array or a linked list to implement a stack</p></li>
<li><p>In both cases, it is easier to delegate the stack methods towards an array list or a linked list object, rather than reimplementing the whole code</p></li>
<li><p>So we can keep a field of type <code>ArrayList</code> or of type <code>SingleLinkedList</code> (there’s no need for a double linked list here, see below for details)</p>
<ul>
<li>and redirect (delegate) the work to the array or linked list methods</li>
</ul></li>
<li><p>Doing this avoids writing the same code more than once (the <em>DRY: Don’t repeat yourself</em> principle), and it simplifies the development of a project (less code to write, less code to test, bugs to correct in only one place, …)</p></li>
<li><p>In some cases, if we only need a stack or a queue and that we need to absolutely optimize the code as much as possible, then rewriting the code might be a good idea, but in general, the DRY principle preferable</p></li>
<li><p>For the same reasons, we don’t usually need 2 or more different implementations of linked lists, so we will choose only one type of linked list, unless we need to optimize the code different purposes</p></li>
<li><p>Therefore, it is better to delegate to an array list or a linked list, and later on, if necessary, for performance reasons, we could refactor our stack to implement it directly without using delegation</p>
<ul>
<li>this change should be transparent (no need to modify the code using the stack or queue)</li>
</ul></li>
<li><p>If we use an array list to implement a stack, we can simply add and remove elements from the end of the list</p>
<ul>
<li>these 2 operations on the end of the array list are constant (<span class="math inline">\(O(1)\)</span>), unless we need to increase the capacity of the underlying array</li>
<li>if we choose a good capacity to start with, we won’t increase the capacity often, so it won’t significantly impact the performance</li>
</ul></li>
<li><p>If we use a double linked list, then we can add or remove either from the start or the end</p>
<ul>
<li>but we need to choose only, either always add and remove from the end, or add and remove from the start</li>
<li>these operations are constant <span class="math inline">\(O(1)\)</span>, so we should have a good performance</li>
</ul></li>
<li><p>But if we use a single linked list, then we should choose the beginning of the list as the top of the stack (add and remove at the start of the list) because if we chose the end of the list as the top of the stack, we would have a linear complexity <span class="math inline">\(O(n)\)</span> for popping an element</p></li>
<li><p>Similarly, it would not make sense to use index 0 in a stack implemented with an <code>ArrayList</code> for the top of the stack, because both pushing and popping would become linear</p></li>
</ul>
<h3 id="what-are-stacks-useful-for">What are stacks useful for?</h3>
<ul>
<li>Method call stacks are the most common application of stacks</li>
<li>Each time a method (or function is called) a <em>stack frame</em> is pushed on the call stack
<ul>
<li>the details vary from a programming language to another, and from an operating to another
<ul>
<li>if the program is compiled natively, to be executed directly on the host OS, then the stack frame contents will depend on the OS</li>
<li>if the language is interpreted or compiled to byte code to be executed on a virtual machine, like Java, then the stack frame contents will depend on the virtual machine</li>
</ul></li>
<li>but in any case, there will be similar data in the stack frame, such as the method parameters, local variables and other information, such as where to return to once the method has completed</li>
</ul></li>
<li>Stacks can also be used in many other places, every time we need a LIFO or FILO structure
<ul>
<li>when parsing binary trees, when we don’t want to (or cannot) use resursive calls</li>
<li>when evaluating mathematical expressions or when compiling programs</li>
<li>when traversing a graph</li>
</ul></li>
<li>In general, stacks are used in recursive algorithms
<ul>
<li>with explicit recursive calls using the call stack</li>
<li>or with implicit recursivity, in other words when the algorithm doesn’t use the call stack with recursive functions, but instead manages a stack directly to simulate recursive calls</li>
<li>the algorithms of type <em>backtrack</em>, which need to go back (or backtrack) to a previous point in the algorithm</li>
</ul></li>
</ul>
<h2 id="queues">Queues</h2>
<ul>
<li>We always interact with a queue in this way:
<ul>
<li><strong>enqueue</strong>: we add an element at the end of the queue</li>
<li><strong>dequeue</strong>: we remove an element from the beginning of the queue</li>
</ul></li>
<li>A queue is used as a <em>waiting queue</em>, a structure of type *FIFO or LILO:
<ul>
<li>the first element added is at the front (or beginning) of the queue</li>
<li>the last element added is at the back (or end)</li>
<li>the first element to come out is at the front</li>
<li>the last element to come out is at the back</li>
</ul></li>
<li>There exists also <em>priority queues</em>, in which the order within the queue is based on a priority system différent from a simple queue
<ul>
<li>we will not discuss this special type of queue here</li>
<li>we will concentrate on “ordinary” queues (the FIFO queues)</li>
</ul></li>
</ul>
<h3 id="implementing-a-queue">Implementing a queue</h3>
<ul>
<li>The best (or some say the easiest) way to represent a queue is with a linked list, to which we add on one side and remove from the other</li>
<li>But which side exactly do we add? And to which side do we remove?</li>
<li>We have to consider the complexity of adding and removing from each side, and which type of linked list we want to use</li>
<li>If we use a double linked list, wether it is cicular or not, we can add and remove from both ends in constant time <span class="math inline">\(O( 1)\)</span>, so we only need to decide which side we are going for the front and the back
<ul>
<li>since a double linked list is symmetric, it doesn’t matter which side we choose, as long as we respect the FIFO nature of the queue</li>
</ul></li>
<li>If we use a single linked list, we have to careful because removing an element from the end is not constant:</li>
<li>So we can add the elements at the end, and remove them from the beginning
<ul>
<li>this way, we are only using operations with constant complexity</li>
<li>as long as we have a direct reference to the last node in the list</li>
</ul></li>
<li>We can implement the <code>Queue</code> class by delegation, like for the stack described earlier
<ul>
<li>our class holds a reference to a list, created by the constructor</li>
<li>and we redirect(delegate) the calls to add and remove elements to the list methods</li>
</ul></li>
</ul>
<h4 id="can-we-implement-a-queue-with-an-array-list">Can we implement a queue with an array list?</h4>
<ul>
<li>Yes, we can, but performance will not be great
<ul>
<li>adding and removing from the beginning of an array, at index 0, is not efficient</li>
<li>we won’t have a choice to have a linear complexity, either to enqueue or dequeue an element, depending which side we choose for the front and the back of the queue</li>
</ul></li>
<li>We could use a circular list implemented by an array list
<ul>
<li>instead of using the number of elements to denote where to add the next element at the end, and 0 to denote the index of the first element
<ul>
<li>we could have a variable index for the start, and the same for the end, different from the number of elements in the list</li>
</ul></li>
<li>so the list would not always start at index 0</li>
<li>but we would keep the condition that we cannot have holes in the middle of the list, meaning that all the elements must be contiguous, without space (or holes) between the elements, but possibly with empty space before the start index and after the last index</li>
</ul></li>
</ul>
<figure>
<img src="../images/queue_circular_array.jpg" alt="" /><figcaption>Queue - circular array</figcaption>
</figure>
<ul>
<li>This type of array list is not only useful to implement queues
<ul>
<li>it’s also useful for a general purpose array list</li>
</ul></li>
<li>We can visualise the list as having the end of the array (index 5 in the previous example) touching the beginning of the array at index 0
<ul>
<li>think of an array that would be drawn on a piece of paper, and that the piece of paper would be bent to make a ring, with the end of the array touching the beginning</li>
</ul></li>
<li>After having added the element 3 in the previous example, the end index is 5
<ul>
<li>and, when we want to add 6, we realise that we are going pass the end of the array, so we go back to the beginning (we wrap around) and we put the new element at index 0</li>
<li>we can use the <em>modulo</em> operation (<code>%</code> in many programming languages) with the array capacity to wrap around to index 0</li>
</ul></li>
<li>We can use the same techniques to increase the capacity of the array when it is full
<ul>
<li>but we have to be careful when we copy the elements into the new array: we cannot leave a hole in the wrong place
<ul>
<li>we need to empty space to be before the first element or after the last element</li>
</ul></li>
</ul></li>
</ul>
<figure>
<img src="../images/queue_increase_capacity.jpg" alt="" /><figcaption>Queue - circular array - increase capacity</figcaption>
</figure>
</body>
</html>
