# Algorithms

- **Algorithm**:
    - A set of rules or instructions that, is applied correctly, will solve a problem in a finite number of steps
    - A set of mathematical instructions or rules that, especially if given to a computer, will help to calculate an
      answer to a problem [[1]](https://dictionary.cambridge.org/dictionary/english/algorithm)
- **Algorithm complexity**:
    - Analysis and comparison of algorithms efficiency
    - 2 types of complexity:
        - theoretical (or mathematical)
        - empirical (or experimental)

## Searching for a value in an array

- Given an array of elements, not necessarily sorted, we need to search for a specific element (or value), often called
  the *target* element, in the array
- The element may be present or not in the array
- If the element is present, then we can either
    - return `true`, or
    - return the index (or position) where the element was found in the array
- If the element is not present in the array, then we can either
    - return `false`, or
    - return `-1`
        - `-1` is always an invalid array index, that's why it is commonly used to mean *not found* when an integer is
          required as a return value

### Sequential search

- When the array elements are not sorted, we don't really have a choice, we need to use a *sequential search*
- The basic idea is simple:
    1. start with the first element in the array (index 0)
    2. compare this element with the target element
    3. if they are equal, then we are done, and we can return `true` or the index
    4. if not, repeat the process with the next element in the array
    5. if we went through all the elements in the array without finding the target element, then the search has failed,
       and we need to return `false` or `-1`

```
function sequential_search(arr, target)
    // arr: array of comparable (or least equatable) elements
    // target: the element we are looking for
    // n: length of arr
    // returns: the index where target was found, or -1
    for i = 0 up to n-1:
        if target == arr[i]:
            return i

    return -1
```

![Sequential search](../images/sequential_search.jpg)

## Algorithm complexity

- To obtain the complexity of an algorithm, we normally need to analyse it theoretically
- We can also do an empirical analysis
- To figure out the theoretical complexity of an algorithm, we don't need to code it and execute it:
    - we can use its pseudocode to analyse it
    - we count the number of instructions to execute, or a number of specific operations
        - for example, we can count the number of additions or comparisons
    - we normally express the complexity in terms of the problem size
        - for example, the number of elements in a list or an array's length
        - $n$ is often used to denote the problem size
    - the notation $O(f(n))$ is often used to give an algorithm's complexity
        - $O$ is pronounced *big O* (not *zero*, but the uppercase letter *O*)
        - $f(n)$ is a function of $n$


- The most important thing in theoretical analysis, is the order of magnitude of the complexity, of the function $f$
- in terms of $n$
- The detailed theoretical analysis will often use limits when $n$ goes to infinity: $lim_{n\rightarrow \infty}$
- The most important question theoretical analysis:
    - when $n$ grows more and more, what will happen with an algorithm's performance?
    - in other words: how will an algorithm's performance vary when the problem size is going to increase?

#### Common complexities

- *Constant*: $O(1)$
- *Logarithmic*: $O(lg\ n)$
- *Linear*: $O(n)$
- *Linear-logarithmic*: $O(n\ lg\ n)$
- *Quadratic*: $O(n^2)$
- *Exponential*: $O(2^n)$
- *Factorial*: $O(n!)$

#### Analysis: 3 cases

- An analysis can be done based on 3 different cases:
    - the best case
    - the average case
    - the worst case
- We often concentrate on the worst case, but the average case is also interesting
- The best case is less interesting in general because it is much less likely to happen than the other 2 cases

### Sequential search: complexity

#### Best case

- If we are lucky, no matter how big the array is, we find the target element immediately, at the beginning of the
  array (`index == 0`)
- In this case, we say that the algorithm is *constant*, or runs in *constant time*, or has a *constant complexity*
  because no matter what size the array is, the number of operations is the same
- we denote its complexity with the expression $O(1)$

#### Worst case

- If we are not lucky, we have to go through the whole array until the last position (`index == n-1`), to find the
  target element
- We also need to go through the whole array if the target is not in the array
- The number of operations to execute will depend on the value of $n$
    - for every element in the array, we compare the target to it
    - if the array contains 10 elements, then we need 10 comparisons
    - if the array contains 100 elements, then we need 100 comparisons
    - if the array contains 1000 elements, then we need 1000 comparisons
    - etc.
- The complexity is denoted $O(n)$ because it depends directly on the array size
- If the array size doubles, then the number of comparisons will double
- We say that the complexity $O(n)$ is *linear*, or the algorithm is *linear*, because the function representing the
  complexity is a linear function

#### Average case

- The average case is a bit harder to express
- We have to use more mathematics to analyse it
- In this case, the analysis is not too difficult: on average, we would expect to go through about half the array to
  find the target element
    - sometimes, we find it close to the beginning, sometimes close to the middle, sometimes close to the end
    - on average, we find in the middle, because all the cases balance each other
        - that assuming we don't search too often for elements not in the array, in which case we would need to go
          through all the elements more often
- So its complexity is $O(n/2)$

#### Question: are $O(n)$ and $O(n/2)$ equal?

- *Answer*: **yes**
- The most important thing is not the exact number of operations, but the **growth** of the number operations relative
  to the problem size
- No matter if we have $O(n)$ or $O(n/2)$, if the array size doubles, then the number of operations will double
- If the array size is multiplied by 100, then the number of operations will also be multiplied 100 in both cases
- The growth is *linear*  in both cases
    - the growth is represented by a linear function
- In the $O(f(n))$ notation, the constants are not important
- So for the sequential search, *the worst case and the average case have the same complexity*
- The average and average cases are not always the same, but in many cases they are
- Sometimes the best and the average cases are the same, but rarely

#### Question: *Can we do better?*

- **No**, unless we add other conditions to the problem
- If the array elements are sorted, then we can do better
- We know that the smallest value is at the beginning, and the largest at the end
    - if we suppose an ascending (increasing) order of course
- We can take advantage of this condition to get a faster algorithm

### Binary search

- If the array elements are sorted, then we can speed up the search
- The *binary search* algorithm is a *divide and conquer* algorithm
- The idea is to start by comparing the target element with the element in the middle of the array
- If we are lucky, we will find the target in the middle immediately
- In general, the target element will not be in the middle
- In this case, because the array is sorted, we can eliminate half of the array from our search:
    - if the target element is **smaller** than the element in the middle, we can eliminate the right half of the array
      from the search because all the elements in the right half are greater than or equal to the middle element,
      therefore they are greater than the target element
    - if the target element is **larger** than the element in the middle of the array, we can eliminate the left half of
      the array from the search because all the elements in the left half are less than or equal to the middle element,
      therefore they are less than the target element
- After eliminating half of the array, we repeat the procedure on the remaining half of the array


- *Notes*:
    - the middle element is not always exactly in the middle of the array
        - if the array size is odd, then the middle element is exactly in the middle
        - if not, as presented in the following algorithm, the middle element will be just to the left of the exact
          middle
            - so, in this case, the left "*half*" will be a bit smaller than the right "*half*"
    - therefore, we don't always remove exactly half of the elements
    - from the point of view of algorithm complexity, if we eliminate $n/2$ or $n/2-1$ or $n/2+1$ elements, it's not
      going to make any difference
        - the effect of the $-1$ or the $+1$ is not going to be important when $n$ will be large

```
function binary_search(arr, target)
    // arr: array of comparable elements
    // target: the element we are looking for
    // n: length of arr
    // returns: the index where target was found, or -1
    
    // we keep note of the active part of the array with the following 3 variables
    start = 0
    middle = n / 2
    end = n - 1
    while start <= end:
        if target == arr[middle]: // found!
            return middle
        if target < arr[middle]: // the target is less than the middle element
                                 // we have to search in the left half
            end = middle - 1     // we move the end to the left of middle
        else:                    // the target is greater than the middle element
                                 // we have to search in the right half
            start = middle + 1   // we move the start to the right of middle
        middle = (start + end)/2
    return -1
```

![Binary search](../images/binary_search.jpg)

### Binary search: Complexity

#### Best case

- If we are lucky, like in the sequential search, we will find the target element on the first comparison with the
  middle element
- So no matter how big the array is, we will find the target element in constant time
- Complexity: $O(1)$

#### Worst case

- How many elements need to be compared to the target element if we are not lucky?
    - we need to express this number in terms of the array size $n$
- For the sequential search, the active array size (or the number of elements potentially equal to the target element)
  goes down by 1 at each step if the target element has not been found
- For the binary search, how does the active array size decreases?
    - at each step we don't find the target element, we eliminate approximately half the array
    - so the active array size is divided by 2 (approximately) at each step
    - the active array size after each step is approximately $n/2$ (if we didn't find the target element)

- How many times can we cut the array in half in the algorithm before we have nothing left?
    - the loop is a `while` loop with the condition `start <= end`
    - the loop will terminate when `start > end` (if we didn't find the target element)
    - how can we get `start > end`?
        - either when we do `start = middle + 1` or `end = middle - 1`
        - if `start == end` and we do 1 of these 2 computations, then we will get `start > end`
            - because at the previous step, we did `middle = (start + end)/2`
            - and with `start == end`, `middle == start == end`
    - when `start == middle == end`, there's only 1 element left to check
        - either we find the target element and we return
        - or we get `start > end`
    - so after getting only 1 element left to check, we are sure to stop the search
    - therefore, we divide $n$ by 2 many times until we get $n = 1$
    - the number of times we can divide the array size by 2 is $log_{2}n=lg\ n$

- **Reminder**: if $x = log_{2}n$, then $2^x = n$
- The complexity of the binary search algorithm in the worst case is $O(lg\ n)$
- We also say that the binary search algorithm is **logarithmic**
- *Examples*:
    - if $n = 64$, we will need to cut the array in half a maximum of 6 times because $lg\ 64 = 6$
        - or if you prefer $2^6 = 64$
    - if $n = 128$, then we have $lg\ 128 = 7$
    - if $n = 1024$, then we have $lg\ 1024 = 10$
- So every time the array size doubles, we add 1 step to the worst case
    - it's much better than the sequential search, which doubles the number of steps whenever the array size doubles
    - but don't forget that the binary search works only on sorted arrays, and that the sequential search works on all
      arrays, sorted or not

#### Average case

- On average, we expect to find the target element in the middle of the process
- The number of times we divide the array in half is divided by 2
- So on average, we have $O(\frac{1}{2} lg\ n) = O(lg\ n)$

- *Notes*:
    - if the array size is not a power of 2, then we will not get exactly $lg\ n$ for the number of steps because we
      cannot divide exactly by 2 at each step. But in terms of algorithm complexity, it doesn't matter, a $+1$ or $-1$ a
      few times in the process will not make a difference when $n$ is large
    - the most important thing is the growth of the number of steps when $n$ gets larger
