# Sorting algorithms

- Sorting array elements
    - Selection sort
    - Insertion sort
- Sorting algorithms complexity

## Selection sort

- The first step of the *selection sort* is similar to the sequential search
    - we go through the array elements
    - but, instead of looking for a specific target element, we are looking for the smallest element in the array
- The second step is to put this element (the smallest element, i.e. the minimum) at the beginning of the array
- Then we repeat these steps, starting with the element after the minimum we just processed
- *Note*: we could also look for the largest (maximum) element and put it at the end of the array

![Animation: selection sort](../images/Selection_sort_animation.gif)

[https://commons.wikimedia.org/wiki/File:Selection_sort_animation.gif](https://commons.wikimedia.org/wiki/File:Selection_sort_animation.gif)

```
function selection_sort(arr):
    // arr: array of comparable elements
    // n: array size
    for i = 0 up to n-2:
        min = arr[i]
        min_pos = i
        for j = i+1 up to n-1:
            if arr[j] < arr[min_pos]:
                min = arr[j]
                min_pos = j
        arr[min_pos] = arr[i]
        arr[i] = min
``` 

![selection sort part 1](../images/selection_sort1.jpg)

![selection sort part 2](../images/selection_sort2.jpg)


### Selection sort: complexity

- The complexity of the selection sort depends on the complexity of finding the minimum in the array
    - in the algorithm given above, finding the minimum is done in the inner loop
    - how many times the operations in this loop are repeated?
        - it depends on the value of $i$
            - with $i=0$, there will be $n-1$ repetitions of the loop
            - with $i=1$, there will be $n-2$ repetitions of the loop
            - ...
            - with $i=n-2$, there will be $1$ repetition of the loop
        - so, on average, there will be $n/2$ repetitions of the loop
    - each operation inside the loop doesn't depend on the array size $n$
        - no matter the value of $n$, the same number of operations will be executed inside the loop
        - so the complexity of 1 repetition of the loop is constant $O(1)$
    - in total, with $n/2$ repetitions, each repetition being constant, we get a complexity of $O(n/2)=O(n)$ for the
      inner loop

- But we also need to consider the outer loop, which has $n-1$ repetitions
    - in addition to the inner loop, the outer loop does a few constant operations, which do not depend on $n$
    - so 1 repetition of the outer loop has a complexity of $O(n)+O(1)=O(n)$
        - since $O(1)$ is smaller than $O(n)$, especially when $n$ becomes large, we can simplify the complexity of 1
          outer loop repetition to $O(n)$
- In total, $n-1$ repetitions of code with complexity $O(n)$ gives $$(n-1)\times O(n) = n\times O(n) - O(n) = O(n^2) -
  O(n) = O(n^2)$$
    - if we repeat $O(n)$ operations $n$ times, in total, we will get about $n^2$ operations, so $O(n^2)$ operations
    - since $O(n)$ is smaller than $O(n^2)$, especially when $n$ becomes large, we can simplify the complexity to $O(
      n^2)$
- So the selection sort is **quadratic**
    - if the array size is multiplied by 2, then the number of operations is multiplied by 4
    - if the array size is multiplied by 10, then the number of operations is multiplied by 100

## Insertion sort

- The main idea behind the *insertion sort* is simple:
    - the array is split into 2 parts:
        - the first part is sorted, which contains 1 element at the beginning at index 0
            - an array (or part of an array) containing only 1 element is always sorted
        - the second part is not sorted and contains all the other elements
    - at each step, we add an element from the non-sorted part into the sorted part
        - but we cannot add this new element anywhere in the sorted part
            - we have to **insert** it in the right position in the sorted part

![Animation: insertion sort](../images/Insertion_sort_animation.gif)

Nuno Nogueira (Nmnogueira) / CC BY-SA (https://creativecommons.org/licenses/by-sa/2.5)
[https://commons.wikimedia.org/wiki/File:Insertion_sort_animation.gif](https://commons.wikimedia.org/wiki/File:Insertion_sort_animation.gif)

```
function insertion_sort(arr):
    // arr: array of comparable elements
    // n: array size
    
    // sorted part of the array: from 0 up to i
    // at the beginning of the loop, i is not in the sorted part
    // at the end of the loop, i is in the sorted part
    for i = 1 up to n-1:
        x = arr[i]
        j = i
        // move the elements greater than x in the sorted part to the right
        while j > 0 and arr[j-1] > x:
                arr[j] = arr[j-1]
                j--
                
        arr[j] = x
```

![Insertion sort part 1](../images/insertion_sort1.jpg)

![Insertion sort part 2](../images/insertion_sort2.jpg)

### Insertion sort: complexity

- The outer loop is executed $n-1$ times
- For each iteration of this loop
    - the 3 assignments at the beginning of the loop (outside the inner loop) are constant $O(1)$
    - the number of iterations of the inner loop is:
        - best case: $0$ iterations
            - if we are lucky, the element at position $i$ is larger than all the elements in the already sorted part of
              the array
        - worst case: $i$ iterations
            - if we are not lucky, the element at position $i$ is smaller than all the elements in the already sorted
              part of the array
        - average case: approximately $i/2$ iterations
            - on average, we have to move about half the elements already sorted to make space for the new element
    - inside the inner loop, there's 1 assignment and 1 decrement
        - so each iteration of the inner loop is constant $O(1)$


- So, what the complexity of the whole insertion sort algorithm?
    - in the worst case, the number of iterations of the inner loop will be, for the different values of $i$, $$1 + 2 +
      3 + \cdots + (n-2) + (n-1) = \frac{(n-1)n}{2} = \frac{n^2 - n - 1}{2}$$
        - this result comes from the arithmetic sum of the first whole numbers, with $n-1$ instead of $n$ as the last
          value $$S=1+2+3+\cdots +(n-1)+n=\sum _{i=1}^{n}i={\frac {n(n+1)}{2}}$$
    - given that the assignments outside the inner loop are constant, we can ignore them
    - therefore, we get a complexity of $O(n^2)$ because we can ignore the constants and the terms less than $n^2$
    - the average case will give the same complexity
        - we will get instead $$\sum _{i=1}^{n-1}\frac{i}{2}=\frac{1}{2}\sum _{i=1}^{n-1}i={\frac {(n-1)n}{4}}$$
        - which is giving also a complexity of $O(n^2)$
