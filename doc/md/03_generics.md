# Searching and sorting generic arrays

- Redefining searching and sorting functions for all data types is not practical, since the only things that would
  differ are the data types themselves and maybe the use of `.equals` or `.compareTo` versus the use of `==`and `<`
- Using generic arrays and functions, and also *lambda* functions make sense here
- But there must be some restrictions on the data types for the algorithms to work properly
    - for the *sequential search*, we must at least be able to compare elements to make sure they are equal or not
    - for the *binary search* and for *sorting*, we must be able to compare elements not only to know if they are equal
      or not, but also less than (or greater than) other elements
        - in Java or C# terms, the elements must be **Comparable**
- Lambda functions can also be useful to specify alternative (non-standard) ways to compare elements, or to compare
  elements that are not directly comparable (i.e. that don't implement the `Comparable`interface)
    - for example, if we have an array of `PersonV1` objects, maybe the default `equals` and `compareTo` functions would
      compare the `PersonV1` objects on their `id`s, but it might be useful in some applications to sort them by last
      name, or by their birthdate
    - instead of implementing different versions of the sorting functions to do this, the sorting functions can accept
      a `Comparator` object used to compare the `PersonV1` objects instead of using the default `compareTo` function