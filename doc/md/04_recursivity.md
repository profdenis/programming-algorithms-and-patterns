# Recursivity

## Recursive definitions

- A recursive definition is a definition that depends on itself
- In the computer science world, recursive definitions are common
- Not only computer scientists use recursivity in their code, they use also recursive definitions to name their projects
- Examples [[1]](https://fr.wikipedia.org/wiki/Sigles_auto-référentiels):
    - **GNU**: *GNU's not unix*
    - **PHP**: *PHP: Hypertext Preprocessor*
    - **PIP**: *PIP Installs Packages*

## Recursive mathematical functions

- In number theory, the whole numbers are defined by a recursive mathematical definition, more specifically using the
  successor function $s(x)$
    - we suppose the existence of the number 0, and we apply $s$ the needed number of times
        - $s(0)$
        - $s(s(0))$
        - $s(s(0))$
        - ...
    - by convention, we write
        - $s(0) = 1$
        - $s(s(0)) = s(1) = 2$
        - $s(s(s(0))) = s(2) = 3$
        - ...
- $s$ is a primitive recursive function, and is used as the base of the definition of addition

### Sum of positive whole numbers

$$S=1+2+3+\cdots +(n-2)+(n-1)+n=\sum _{i=1}^{n}i={\frac {n(n+1)}{2}}$$

- Suppose we don't know the formula to calculate the sum of positive whole numbers
- We could calculate it in 2 ways:
    - iterative (with a loop)
    ```
    function iterative_sum(n):
        sum = 0
        for i = 1 up to n:
            sum += i
        return sum
    ```
    - recursive (with a recursive function) $$\sum _{i=1}^{n}i=n+\sum _{i=1}^{n-1}i$$
    ```
    function recursive_sum(n):
        if n < 1:
            return 0
        return n + recursive_sum(n-1)
    ```

#### Complexity of `iterative_sum`

- The complexity is simple to calculate
    - the operations `=` and `+=` are constant
    - we have a loop from 1 to $n$, so there are $n$ iterations of the loop
    - this gives us a complexity of $O(n)$
- If we used the the formula, it would be even better because we would not have the loop
    - we would have only a multiplication, an addition, and a division
    - so we would have a complexity of $O(1)$

#### Complexity of `recursive_sum`

- There's no loop in this function, but that doesn't mean there isn't code repetition
- To analyse a recursive function, we need to count how many times the recursive function is called
- To simplify the notation, suppose the function `recursive_sum` is named $f$
- The first call to the function is $f(n)$, and if $n \geq 1$, then we call $f(n-1)$
- If $n-1 \geq 1$, then we call $f(n-2)$, and we keep repeating this process until we call $f(0)$
    - there won't be any recursive call with $f(0)$
    - the sequence of recursive calls will stop with $f(0)$
    - we say that $n<1$ is a base case, or a stop condition for the recursive calls
    - *without any base case* (there could be many), we could have a situation with **infinite recursivity**
        - actually, in practice, there's a limit on the number of recursive calls a program can make, so we cannot have
          an infinite number of recursive calls, and the program will crash after a while
- Using the mathematical notation, we would write the function `recursive_sum` (using the shortcut $f$ another time):
  $$f(n) = \begin{cases} 0 & n < 1 \\ n+f(n-1) & n \geq 1 \end{cases}$$
- We will call the function $n+1$ times: $f(n), f(n-1), f(n-2), ..., f(1), f(0)$

![Recursive sum](../images/recursive_sum.jpg)

- We will have $n+1$ function calls, each time with a constant number of operations constant (which do not depend on
  $n$):
    - after the `if` which compares `n` to 1 (constant) either
        - return the value 0 (constant)
        - make an addition with the result of the recursive call, then return a value (also constant)

- The complexity will be $O(n)$, as for the iterative sum
- From the theoretical point of vue, their complexities are identical
- But from the practical point of vue, the iterative sum will be faster because calling a function is more expensive
  than looping
- Whenever possible, we will use the iterative version of algorithm
- But sometimes, expressing an algorithm in an iterative way is difficult, and expressing it in a recursive way is much
  easier, so the recursive version will be preferred in these cases

### Function call stack

- Every time a function (or a method) is called, in a recursive way or not, space is reserved this call on the **call
  stack**
- Space is allocated for the function parameters and also for the local variables
- Information is also kept to know where to return to (where the function was called)
- Every function call has its own space on the call stack, so if we call the same function multiple times in a recursive
  manner, each call will have its own copy of each parameter and each local variable
- Therefore, each function call doesn't interfere with the other one, unless the parameters are pointers or references
  to objects located somewhere else in memory, and the different function calls are given the same pointers or
  references

![Function call stack: recursive sum](../images/call_stack.jpg)

### Fibonacci numbers

- The *Fibonacci numbers* sequence is defined in a recursive way on positive integers:
  $$F_n = \begin{cases} 0 & n = 0 \\ 1 & n = 1 \\ F_{n-1}+F_{n-2} & n > 1 \end{cases}$$

- To calculate $F_5$, we need to calculate $F_4$ and $F_3$
    - but to calculate $F_4$, we also need to calculate $F_3$ and $F_2$
    - and for $F_3$, we need $F_2$ and $F_1$
    - and we need to continue and apply the same principle, which can become complicated, especially when we start with
      a value larger than 5
- The Fibonacci numbers sequence is often denoted $f(n)$ instead of $F_n$

![Animation Fibonacci](../images/Animation_Fibonacci.gif)

EloiBerlinger / CC BY-SA (https://creativecommons.org/licenses/by-sa/4.0)

- The pseudocode is very simple, it is translated directly from the recursive definition

```
function fibonacci_recursive(n):
    if n == 0 or n == 1:
        return n
    return fibonacci_recursive(n-1) + fibonacci_recursive(n-2)
```

- *Notes*:
    - before we can execute the addition on the last line, both recursive calls have to complete and return their value
    - so the first recursive call `fibonacci_recursive(n-1)` will be completely executed before the second recursive
      call `fibonacci_recursive(n-2)` can proceed and be computed
    - and only then the addition can be calculated, and the result can be returned after that

![Fibonacci numbers (n=5): recursive calls](../images/fibo5_calls.jpg)

![Fibonacci numbers (n=5): return](../images/fibo5_return.jpg)


*Note*: the order in which the recursive calls are made is given in the black circles

#### Repetitions

- The recursive definition of the Fibonacci numbers is simple
- But it is not efficient
- There are too many repetitions: the function with the same value of $n$ appears multiple times in the recursive calls
  tree
- For example, we can represent the recursive call tree for $F_6$ in terms of the $F_5$ recursive call tree
    - we create a new root for $F_6$
    - we copy the $F_5$ tree to the left of $F_6$
    - and we copy the $F_4$ tree to the right of $F_6$
- So to calculate $F_6$, we repeat all the calculations of $F_5$, plus all the calculations of $F_4$, which were
  themselves includes in $F_5$!

![Fibonacci numbers (n=6): recursive calls](../images/fibo6_calls.jpg)

![Fibonacci numbers (n=6): repetitions](../images/fibo6_repetitions.jpg)


#### Complexity of `fibonacci_recursive`

- The analysis of a recursive algorithm depends on the number of recursive calls of the recursive function
- Let $A_i$ be the number of times $f(i)$ is called during the execution of $f(n)$, then we have $$A_i = \begin{cases} 1
  & i = n\ \text{ou}\ i = n-1 \\ A_{i+1}+A_{i+2} & 0 < n < n-1 \\ A_{2} & n = 0 \end{cases}$$
- $f(i)$ is called by only $f(i+1)$ and $f(i+2)$, therefore the number of times $f(i)$ is called is the sum of the
  number of times $f(i+1)$ and $f(i+2)$ are called
    - with the exceptions of $f(n)$ $f(n-1)$, which are called exactly once each
    - and $f(0)$ is called only by $f(2)$

- For example, when executing $f(6)$:
    - $A_{6} = 1$
    - $A_{5} = 1$
    - $A_{4} = 1 + 1 = 2$
    - $A_{3} = 1 + 2 = 3$
    - $A_{2} = 2 + 3 = 5$
    - $A_{1} = 5 + 3 = 8$
    - $A_{0} = 5$
- Except for $A_{0}$, the numbers $A_i$ are themselves the numbers in the Fibonacci sequence!

- The number of recursive calls is then $$(F_1 + F_2 + F_3 + \cdots + F_{n-1} + F_{n}) + F_{n-1} = F_{n-1} + \sum _
  {i=1}^{n}F_i = O(2^n)$$
- The complexity is **exponential**!
- *Notes*:
    - the complete mathematical proof of this exponential complexity is too complicated to be presented here in an
      introduction to recursive functions complexity
    - the main idea is that every time we add 1 to the parameter $n$, we basically double the number of calculations
      necessary to calculate the answer
    - if we go from $F_n$ to $F_{n+1}$, we recalculate everything we did in $F_{n}$ and in $F_{n-1}$, so we almost
      double the work

#### Fibonacci, iterative version

- The *memoization* technique
    - to avoid repeating all the same calculations many times, we can "*write a memo*" to remember the result from a
      previous calculation
    - we can reuse the result of a previous calculation whenever we need it, without recalculating it every time
- For example, if we want to calculate $F_6 = F_5 + F_4$, we can calculate $F_4$ first, and then reuse this result when
  we calculate $F_5$
    - but both $F_5$ and $F_4$ need $F_3$, so we also need to avoid calculating $F_3$ many times
- The simplest thing to do is to start calculating the $F_i$ from "the bottom", in other words start with the small
  values of $F_i$
    - $F_0 = 0$ and $F_1 = 1$ are already known
    - then calculate $F_2$ from $F_0$ and $F_1$
    - then calculate $F_3$ from $F_1$ and $F_2$
    - we don't need to keep all the values of $F_i$, only the last 2

```
function fibonacci_iterative(n):
    f_i2 = 0  // for F i-2: at the beginning F0, then F1, F2, F3, ...
    f_i1 = 1  // for F i-1: at the beginning, F1, then F2, F3, F4, ...
    if n == 0 or n == 1:
        return n
    for i = 2 up to n:
        f_i = f_i1 + f_i2
        f_i2 = f_i1       // for the next iteration
        f_i1 = f_i        // f_i1 becomes f_i2, and f_i becomes f_i1
    return f_i
```

![Fibonacci numbers (n=6): iterative version](../images/fibo6_iterative.jpg)

#### Complexity of `fibonacci_iterative`

- The analysis of this iterative version of the algorithm is much simpler than the recursive version
- We have a loop with $n-1$ iterations, and all other operations are constant (assignments, comparisons and addition),
  so we have a linear complexity $O(n)$
- A linear complexity is by far better than an exponential complexity
    - the execution time of $F_{100}$ will be about 10 times larger than the execution time of $F_{10}$ with the
      iterative version
    - but with the recursive version, we will have $O(2^{100})=O(2^{90}\times 2^{10})$, so $F_{100}$ will be about
      $2^{90}=1237940039285380274899124224\approx 1,2\times 10^{27}$ slower than $F_{10}$!
- Another problem will be the memory used by the recursive version: there's a good chance the call stack, which has a
  limited amount of memory allocated to it, will run out of space, and we will get some kind of `StackOverflow`
  exception
  - with the iterative version, we only need a fixed (constant) amount of memory


## Recursive sequential search

```
function sequential_search_recursive(arr, target, start)
    // arr: array of comparable (or least equatable) elements
    // target: the element we are looking for
    // start: start searching at that index
    // n: length of arr
    // returns: the index where target was found, or -1
    if start >= n:
        return -1
    if target == arr[start]:
        retourn start
    return sequential_search_recursive(arr, target, start + 1)
```

![Recursive sequential search](../images/rec_seq_search.jpg)


## Recursive binary search

```
function binary_search_recursive(arr, target, start, end)
    // arr: array of comparable (or least equatable) elements
    // target: the element we are looking for
    // start: start searching at that index
    // end: search up to this index (inclusive)
    // n: length of arr
    // returns: the index where target was found, or -1
    if start > end:
        return -1
    middle = (start + end)/2
    if target == arr[middle]: // found!
        return middle
    if target < arr[middle]:  
        return binary_search_recursive(arr, target, start, middle - 1)
    return binary_search_recursive(arr, target, middle + 1, end)
```

![Recursive binary search](../images/rec_binary_search.jpg)
