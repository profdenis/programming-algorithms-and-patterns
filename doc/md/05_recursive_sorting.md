# Recursive sorting algorithms

## Divide-and-conquer algorithms

- **Divide-and-conquer** algorithms is a very important algorithms development technique
- Usually, *divide-and-conquer* algorithms are recursive, but it's not mandatory
- There are 2 steps: **divide** and **conquer**
    - *divide*: the problem is split (or *divided*) in sub-problems
        - usually, the problem is split into 2 sub-problems, but it could be split into more sub-problems
        - ideally, the sub-problems should be of approximately the same size, but it's not always possible
        - and the algorithm has to solve the sub-problems, usually recursively (but not always)
    - *conquer*: the sub-problems are combined to provide a solution for the complete problem

### *Divide-and-conquer* sorting algorithms

- If we need to sort an array of size $n$, and we want to apply the *divide-and-conquer* technique, we can
    - split the array into 2 halves (or almost halves if $n$ is odd) of size $n/2$ each (or sizes $\lfloor n/2\rfloor$
      and $\lceil n/2\rceil$)
    - sort each array half using the same technique
        - we split each half in 2, to get 2 sub-arrays of size $n/4$
        - we sort each quarter of the array in the same way
            - we get sub-arrays of size $n/8$
            - etc.
    - then we combine the sorted sub-arrays to get a fully sorted array
- In some algorithms (like in the *merge sort* algorithm for example), most of the work is done in the *conquer* part
  (which we call the *merge* part for the merge sort), and the *divide* part is very simple
- In other algorithms (like in the *quicksort* algorithm for example), most of the work is done in the *divide*  part (
  which we call the *partition* part for the quicksort), and the *conquer* part is very simple

## Merge sort

- The first step in the *merge sort* algorithm is simple:
    - split the array of size $n$ into 2 equal parts of size $n/2$, or almost equal parts of sizes $\lfloor n/2\rfloor$
      and $\lceil n/2\rceil$ if $n$ is odd
- Since the algorithm is recursive, we need to define the base case well to stop the sequence of recursive calls and
  avoid an infinite recursion
    - because the goal is to sort the array, we need to find a condition for which we can be sure that the sub-array is
      already sorted and doesn't need further processing, so that we can stop the recursion
    - if the array (or sub-array) is of size 1 or less ($n\leq 1$), there's nothing to sort, therefore we don't need to
      split the array further, and we don't need any more recursive calls
    - other options are possible (discussed below)

- The second step is to merge, or conquer, the sub-arrays already sorted by the recursive calls

```
fonction merge_sort(arr, start, end):
    // first call: mergeSort(arr, 0, n-1)
    n = end - start + 1
    if n <= 1:
        return 
    middle = (start + end + 1) / 2
    merge_sort(arr, start, middle-1)
    merge_sort(arr, middle, end)
    merge(arr, start, middle, end)
```

![Merge sort 1](../images/merge_sort1.jpg)

![Merge sort 2](../images/merge_sort2.jpg)

![Merge sort 3](../images/merge_sort3.jpg)

### Merge

- In `merge`, we need a temporary array to allow for an efficient merge
    - to avoid creating a temporary array of the needed size on every call, it would be possible to create a temporary
      array of size $n$ at the beginning, and reuse this temporary array on each call to `merge`
    - from the point of view of the theoretical complexity of the algorithm, it doesn't make any difference, the same
      number of operations will be executed from the point of vue of the *big-O* notation
    - but from the practical point of vue, it could make a small noticeable difference, depending on the programming
      language, the compiler and the operating system used

- The goal is to put the elements of the 2 sorted sub-arrays in the correct order in the temporary array
- We maintain 3 indexes (or pointers), 1 for each sub-array and 1 for the temp array, to keep the index of the current
  element in each array or sub-array
    - they are initialized to refer to the first element in each array or sub-array
- We need to compare the current elements in the 2 sub-arrays (indexes `i` and `j`)
    - if `tab[i] <= tab[j]`, then we put `tab[i]` in the temp array, and we increment `i` et `k` (the current index in
      the temp array) to refer to the next elements
    - we repeat the comparison `tab[i] <= tab[j]` until we get an element such that `tab[i] > tab[j]`, or until there
      are no elements left in the first sub-array to put in the temp array
        - the condition `i < middle` makes sure that the index `i` stays within the first sub-array

- We repeat the same process, but reversed
    - we want to put the elements from the second sub-array which are smaller that the current element in the first
      sub-array into the temp array
    - we also need to make sure we don't move `j` too far
- Because the length of the temp array is equal to the sum of the lengths of the sub-arrays, the temp array will be full
  when the sub-arrays have been emptied
- In this version of the algorithm, we try to move as many elements as possible from the first sub-array (the first
  inner loop)
    - and then we try to move as many elements as possible from the second sub-array (the second inner loop)
    - and we repeat as long as there remains elements to be moved
- The last step is to copy the elements from the temp array into the original array

```
function merge(arr, start, middle, end):
    // temp: temporary array of size end - start + 1
    n = end - start + 1
    i = start   // index in the first sub-array
    j = middle  // index in the second sub-array
    k = 0       // index the temp array
    while k < n:
        while i < middle and (j > end or arr[i] <= arr[j]):
            temp[k] = arr[i]
            i++
            k++
        while j <= end and (i >= middle or arr[j] <= arr[i]):
            temp[k] = tab[j]
            j++
            k++
            
    for k = 0 up to end-start:
        arr[start+k] = temp[k]
```

![Merge](../images/merge.jpg)

### Complexity: merge sort

- The analysis of a recursive algorithm usually has 2 parts:
    1. the analysis of a single execution of the algorithm (a single call to the function), *without counting* the
       recursive calls
        - in the merge sort, the most important is the complexity of the `merge` function, because the other operations,
          except the recursive calls, are constant( 1 condition and the calculation of `middle`)
    2. the number of recursive calls of the function, or the amount of work done by the recursive calls

#### Complexity of `merge`

- The key operations are moving the elements from the 2 sub-array into the temp array, and the copying the elements from
  the temp array into the original array
    - we have to copy all the elements from the 2 sub-arrays exactly 1 time to the temp array
        - the first sub-array has `middle - start` elements
        - the second sub-array has `end - middle +1` elements
    - the while loops are used to determine the order in which the elements are copied
    - so we have `(middle - start) + (end - middle + 1) = end - start + 1` elements to copy
        - exactly the number of elements in the temp array
- So we need to copy `end - start + 1` elements from `arr` to `temp`, and then copy `end - start + 1`
  elements from `temp` to `arr`
    - with `n = end - start + 1`, we have $O(2n) = O(n)$
    - so `merge` is linear

![Merge sort: recursive calls](../images/merge_sort_calls.jpg)

- At each level of recursive calls, we have a total length of all sub-arrays of $n$
    - at the root, we have a call of an array of size $n$
    - then we have 2 calls on sub-arrays of size $n/2$, for a total of $n$
    - then we have 4 calls on sub-arrays of size $n/4$, for a total of $n$
    - then we have 8 calls on sub-arrays of size $n/8$, for a total of $n$
    - ...
    - then we have $n/2$ calls on sub-arrays of size $2$, for a total of $n$
    - at then end, we have $n$ calls on sub-arrays of size $1$, for a total of $n$


- Since `merge` is linear $O(n)$, we have $O(n)$ at each level
    - we have $n$ elements to put in the the right place in their corresponding sub-arrays at each level
- And the number of levels (the *height*) is $h = log_2n$, so we have a total complexity of $O(n\ log_2n)$
    - if we divide $n$ by 2 repetitively until we get 1, we will have divided $n$ by two $log_2n$ times

## Quicksort

- There exists multiple versions of the quicksort algorithm
- More specifically, there exists multiple versions of the `partition` function, which is at the core of the quicksort
  algorithm
- As opposed to the merge sort algorithm, the recursive calls are made at the end, after having done all the work in the
  `partition` function
    - in the merge sort algorithm, the recursive calls are done first, and all the work is done in the `merge` function

- The basic principle of all partitioning methods are the same:
    - we choose a **pivot** element
    - we move all the elements smaller than the pivot at the beginning of the array, *before* the pivot
    - and we move all the elements larger than the pivot element at the end of the array, *after* the pivot
- Then we apply the same principle, with a recursive call, to the elements before the pivot (so to all the elements
  smaller than the pivot), and to the elements after the pivot (so to all elements larger than the pivot) with another
  recursive call
- The pivot element is positioned at its final location because all the elements smaller than the pivot are placed
  before it, and all the larger elements are placed after it

- The first partitioning algorithm is often called the *Lomuto partition*

```
function quick_sort(arr, start, end):
    // n: array length
    // first call: quick_sort(arr, 0, n-1)
    n = end - start + 1
    if n <= 1:
        return
    p = partition_lomuto(arr, start, end)
    quick_sort(arr, start, p-1)
    quick_sort(arr, p+1, end)
```

```
function partition_lomuto(arr, start, end):
    pivot = arr[end]
    i = start  // position where the pivot should go
    for j = start up to end-1:
        if arr[j] < pivot:  // every time we find an element smaller than the pivot
            swap arr[i] and arr[j]
            i = i + 1  // the final position of the pivot is moved to the right
    swap arr[i] and arr[end]  // put the pivot in the right place
    return i
```

- The Lomuto partition works this way:
    - we choose the last element as the pivot
    - `i` contains the final position of the pivot, and starts at `start`
        - at the beginning, we consider all the elements to be larger than the pivot by default, so at the beginning,
          there's nothing before the pivot, that's why `i = start`
    - we want to put all the elements smaller than the pivot to the left of the pivot, therefore everytime we find an
      element smaller than the pivot in the loop going through all the elements we move it at the beginning and
      increment i`
    - at the end, we put the pivot in its correct position, at index `i`

![Quicksort: Lomuto partition](../images/quicksort_lomuto.jpg)

- After the first partition, there are 2 sub-problems, one of size 2, and the other of size 6
- Depending on the pivot chosen, the sub-problems sizes will vary
- There will be 2 recursive calls:
    - `quick_sort(arr, 0, 1)`
        - `1` will be chosen as the pivot, and since there's only 1 element besides the pivot, only 1 comparison will be
          done
        - because $2 > 1$, there won't be any swap in the loop
        - only 1 swap done at the end, to put the pivot at the right place
    - `quick_sort(arr, 3, 8)`
        - `5` will be chosen as the pivot
        - all the elements smaller will need to go to the left of the pivot, and the elements larger than the pivot to
          the right
- Note that the pivot itself is not included in the recursive calls because it is already located in the correct
  position in the sorted array place, dans le tableau trié

### Complexity: quick sort

#### Worst case

- The complexity of the quick sort algorithm is more complicated to analyze because the algorithm doesn't guarantee that
  the array will be split into 2 equal parts (or almost equal parts)
    - if the pivot is always the largest (or the smallest) element in the sub-array at each step, then the right (or
      left) "half" will always be empty
        - these cases happen when the array is already sorted, either in increasing or decreasing order
    - and the other "half", the left (or right) "half" will always be of size $n-1$
    - we will have a situation similar to the recursive sequential search, with recursive calls on arrays of size $n-1$
- Therefore, in the worst case, there will be $n$ calls to the `partition_lomuto` function
- The `partition_lomuto` is linear, because of the `0` to `end-1` loop ($n-1$ repetitions), and all the other operations
  are constant
- $n$ repetitions of a linear function gives a quadratic algorithm, $O(n^2)$

#### Best case

- The best case would be to have a situation similar to the merge sort, in which we always split the problem into 2
  equal (or almost equal) parts at each step
- If this is the case, we will have $log_2n$ levels of recursive calls because the problem size will be divided in half
  at each step
    - and at each level, the total work done by the `partition_lomuto` function will be $O(n)$
- Therefore the quick sort algorithm has a complexity of $O(n\ log_2n)$ in the best case

#### Average case

- On average, in the general case, it will be rare to always get the worst case at each step
    - sometimes we will get the best case, sometimes the worst case, and most of the time cases in between
    - we could show that the intermediate partitions, partitions that split the array into fractions of $1/3$-$2/3$
      instead of $1/2$-$1/2$, also give a number of recursive call levels that is logarithmic (but with a different
      base)
    - so we will also get a complexity of $O(n\ log_b n)$, with a base possibly different from 2
    - in the theoretical analysis, the logarithm base is not important, so we will get the same complexity in the
      average as in the best case, $O(n\ log_2n)$

### Other partitioning algorithms

- There exists many ways to partition an array for the quick sort algorithm
- The Lomuto partition is not as efficient as other algorithm because it more sensitive to the pivot choice, which is
  always the element in the sub-array
- It is possible to make the pivot choice more random
    - the worst case $O(n^2)$ is always possible, but less likely because the pivot is chosen at random
    - the worst case will happen when we are really unlucky when we always end up choosing the largest or the smallest
      value in the array as the pivot at each step

```
function quick_sort_random_pivot(arr, start, end):
    // n: array length
    // first call: quick_sort_random_pivot(arr, 0, n-1)
    n = end - start + 1
    if n <= 1:
        return
    a = random_index(start, end)  // choose an index at random in the sub-array
    swap arr[a] and arr[end]
    p = partition_lomuto(arr, start, end)
    quick_sort_random_pivot(arr, start, p-1)
    quick_sort_random_pivot(arr, p+1, end)
```

- This version uses the same Lomuto partition
- The choice of a random pivot is done before the call to `partition_lomuto`
    - we choose a random index in the sub-array
    - we swap the element at that random position with the element at the end
    - and the sub-array is partitioned according to this element chosen at random

- There exists other partitioning methods, like for example the "*median of 3 elements*" method
    - we compare the elements at indexes `start`, `middle`, and `end`, and we take the median of these 3 values as the
      pivot
        - the median is the element in the middle of these 3 elements when they are sorted
    - we can replace the random pivot choice in `quick_sort_random_pivot` by the choosing the median of 3 elements, and
      continue in the same way we did with the `partition_lomuto` function


- The Lomuto partition is not very efficient and there are many duplicate elements in the array
    - another worst case is when all the elements are the same
    - there exists partitioning algorithms that split the elements into 3 parts:
        - the elements smaller than the pivot
        - the éléments equal to the pivot
        - the elements larger than the pivot
    - and the recursive calls are done on the first and third parts of the array
    - this way, if all the elements are the same, the recursive calls will be on made on empty sub-arrays, therefore
      they will terminate immediately without other recursive calls récursifs
