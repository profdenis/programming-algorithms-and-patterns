# Dynamic Arrays

## Implementing the `List<T>` interface with a variable length array

- [Interface `List<T>`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html)
- **List**: ordered collection of objects that can be accessed by their index
- Key methods:
    - `add(element)`: add an element at the end of the list
    - `add(index, element)`: add (or insert) an element at a specific index in the list
    - `indexOf`: find the index of an element in the list
    - `remove(element)` and `remove(index)`: find and remove an element from the list, and remove the element at the
      specified index

## Basic principles of the *ArrayList*

- Generic class, with a generic type for the elements in the list
- Contains an array with elements of the generic type: `T[] array`
- 2 properties:
    - `count`: the number of elements in the list
    - `capacity`: the array length
- `count` $\leq$ `capacity`
    - there will normaly be unused space in the array
    - for example, if the capacity is 10 and the number of elements in the list is 6, there will be 4 empty cells in the
      arrays
    - the empty cells are always at the end, there cannot be empty cells (or holes) in the middle or at the beginning of
      the array
        - unless the number of elements is 0, and in this case all cells are empty

![Dynamic arrays - free space](../images/ArrayList_free_space.jpg)

### Adding and inserting elements in an ArrayList

- The `add(T)` method adds an element at the end of the list, so at the end of the active part of the array
    - we assume, for now, there's enough space in the array to add the element

```
method add(element):
    array[count] = element
    count++
```

![Dynamic arrays - add an element at the end](../images/ArrayList_add.jpg)

- The `add(index, element)` method insert an element at the given index
    - this method is more complicated than `add(element)`, because it needs to make space for the new element
    - we do not replace an element by another one, we instead move elements to the right to make space for the new
      element

```
method add(index, element):
    // move elements to the right to make space for the new element
    for j = index up to count-1:
        array[j+1] = array[j]
    array[index] = element
    count++
```

![Dynamic arrays - insert an element](../images/ArrayList_insert.jpg)

## What happens when there's no more space in the array?

- In other words, what will happen when we call `add` (any version) and that `count == capacity`?
    - *Answer*: we need to increase the array capacity
    - that's why an ArrayList is often referred to as a **dynamic array**
        - the array capacity is increased as necessary

```
method increaseCapacity(newCapacity):
    temp = new array of size newCapacity
    copy the current array elements into the temp array
    array = temp
```

![Dynamic arrays - increase capacity](../images/ArrayList_increase_capacity.jpg)

#### How to choose the new capacity

- Obviously, the new capacity has to be larger than the old one, if not there won't be enough space for all the elements
- We should avoid increasing the capacity by only 1 at each step because to allocate a new array and copy all the
  elements is expensive
    - we don't want to call `increaseCapacity` every time we call `add`
- but we don't want to allocate an array that is too big because there will be too much wasted space
- 2 common techniques:
    - increase the capacity by a constant, for example by 10 elements, each time we call `increaseCapacity`
    - or we can multiply the capacity by 2 each time we call `increaseCapacity`
- If we want to minimize the wasted space without calling `increaseCapacity` each time, we could choose the first option
- If we want to minimize the complexity (the number of operations), then the second is better
- In `add`, we have to start by checking if there's enough space for the new element, and if not, we
  call `increaseCapacity` before adding the new element

### `indexOf`

- This method is using the sequential search algorithm
- We have to be careful to search only active part of the array, by searching only the first `count` elements, and not
  all the elements in the array

### Removing elements

- The `remove` methods are the opposite of the `add` methods
    - `remove(index)`: remove an element at a specific index
        - very similar to `add(index, element)`, except that we remove an element instead of adding it
        - we cannot leave *holes* in the array, so we need to shift to the left the elements occurring after the
          elements we want to remove
            - unless the element we want to remove is the last one
    - `remove(element)`: removes an element
        - here, the index is not specified
        - so we need to find the index of the element to remove with `indexOf`
        - and if we find it, we remove it with `remove(index)`
        - we return *true* if the element has been removed, or *false* otherwise

![Dynamic arrays - remove an element](../images/ArrayList_remove.jpg)

```
method remove(index):
    for j = index+1 up to count-1:
        tab[j-1] = tab[j]
    count--
    
method remove(item):
    index = indexOf(item)
    if index < 0:
        return false
    remove(index)
    return true
```

## Iterators

- *Iterators* are used to go through all the elements of a collection
- If the collection keeps the elements in order, as in an array list and in a list in general, we can iterate (or loop)
  through the list elements by position (or index) with a loop accessing the elements by index with the `get` method

```
for i = 0 up to n-1:
    f(list.get(i))
```

- In general, a collection is not necessarily ordered (the elements are not stored as a sequence), so we cannot access
  the elements by position
- Also, some implementations of the `List` interface, more specifically the linked list implementations, do support
  access by position, but each access will a complexity of $O(n)$ on average, so it's not efficient
- We can use a kind of `for each` loop to iterate through the elements
    - internally, the compiler will transform the `for each` loop into a while loop using an iterator

```
for each element x in the list:
    f(x)
```

- An iterator for an array list should contain, at least, the following:
    - a reference to the array list
    - a current index
    - a method to check if there's a next element: `hasNext` in Java
    - a method to get the next element and move the iterator the following element: `next` in Java
- Normally, an iterator starts before the first element, and the first call to `hasNext` will check if there's a first
  element (i.e. if the list is not empty)
- Successive calls to `next` return the next element and move the current index to the second, third, fourth, ...
  elements
- When there are no more elements, `hasNext` returns *false*, otherwise it returns *true*
- If the list is empty, the first call to `next` will throw an exception. An exception will also be thrown by `next` if
  there are no more elements left
