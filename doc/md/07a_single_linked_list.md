# Linked lists

https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/List.html

- The goal is to implement the same `List` interface as implemented by the `ArrayList` class, but without using an array
- We use nodes containing list elements, plus at least 1 reference to the next node
- The nodes in a *single linked list* contain only 1 reference to the next node
- The nodes in a *double linked list* contain 2 references to other nodes: a reference to the previous node and a
  reference to the next node

## Single linked list

- The `SingleLinkedList<T>` class contains 2 references to nodes:
    - `first`: the first node in the list
    - `last`: the last node in the list
- It's not mandatory to keep a reference to the last node, but it can facilitate some operations, including the addition
  of an element at the end of the list
- We also keep the number of nodes in the list in the `count` field to avoid going through the list to count the number
  of nodes
- A *node* in a single linked list (internal class `Node`) contains
    - an element of type `T`
    - a reference to the next node

![Single linked list with 4 nodes](../images/sll_4nodes.jpg)

### Single linked list methods

#### Add an element at the end of the list

```
method addElement(element):
    // if the list is empty, both first and last are null
    if last is null:
        first = last = new Node(element)
    else:
        // the new node must go after the current last node
        last.next = new Node(element)
        // we move the reference to the last node to the new last node
        last = last.next
    count++
```

![Single linked list - add at the end](../images/sll_add.jpg)

- If we didn't have a reference to the last node, then we would need to go through all the nodes in the list from the
  first node to find the last node where we need to add the new node
- In a linked list, as opposed to an array, we don't have a direct access (by index) to an element
- We always need to start from the first node, then follow the references to the next nodes until we find the element we
  want
- For example, to find the last node, if we didn't have a reference, we would need to do:

```
current = first
while current.next is not null:
    current = current.next
```

#### Add an element at the beginning of the list

```
method addBeginning(element):
    // if the list is empty, then first and last are null
    if last is null:
        first = last = new Node(element)
    else:
        // the new first node must go before the current first node
        first = new Node(element, first)
    count++
```

![Single linked list - add at the beginning](../images/sll_add_beginning.jpg)

#### Add an element at a given index

- All the methods based on an index necessitate going through the nodes starting from the first nodes, similarly to what
  has been described above, but by counting the nodes to stop on the correct node
    - exceptions: if the index is equal to 0 or to `count`, we can call the methods acting directly on the first or last
      nodes
- To avoid repeating the same loop in many places, we can define a method to find a node at a given index

```
method findNode(index):
    current = first
    for i = 0; i < index and current != null; i++:
        current = current.next
    return current
```

- If the index is too big, then `null` will be returned

![Single linked list - find a node by index](../images/sll_find_node.jpg)

```
method add(index, element):
    if index == count:
        add(element)
    else if index == 0:
        addBeginning(element)
    else:
        previous = findNode(index - 1)
        previous.next = new Node(element, previous.next)
        count++
```

- It is important to note that we need to find the node preceding the node at the specified index because it is not
  possible to go back to a previous node
    - if we find the node at index `i`, we cannot adjust the `next` reference of the previous node (index `i-1`) to the
      new node
    - `previous.next` refers to the node with index `i`
        - the new node must be placed at this location
        - so in `new Node(element, previous.next)`, `previous.next` refers to the old node at index `i` (which will
          become the node at index `i+1`)
        - and the new node is placed at after the node at index `i-1`

![Single linked list - insert](../images/sll_insert.jpg)

#### `indexOf` method

- This method is actually a sequential search, but on nodes instead of an array
- `indexOf` is similar to `findNode`
    - `findNode` counts the nodes from the first node and returns `i`th node
    - `indexOf` counts the number of nodes from the first node until it finds the given element, and then returns the
      index of the node

```
method indexOf(element):
    current = first
    index = 0
    while current != null:
        if current.element.equals(element):
            return index
        current = current.next
        index++
    return -1
```

- We start at the beginning
    - with `current` referring to the first node
    - and with `index` equal to 0 (the first node is at index 0)
- And while we haven't found the given element and that we haven't reached the end of the list
    - we move `current` to the next node
    - and we add 1 to `index`
- If we reach the end of the list without finding the element, we return -1

#### `remove(index)`

- The `remove(index)` method must find the node at the right index so that we can remove it
- As for `add(index, element)`, we need to find the node before the node to process to be able to adjust the references
  correctly
    - so we use the `findNode` method once again
- If we need to remove the first node, there's no node before it, so we need to make it a special case
- If we need to remove the last node, then we need to use the `findNode` method also because starting from `last`,
  there's no way to go back and reposition `last` to the node just before the last
    - we absolutely need to use `findNode` to find the node before the last node, which will become the new last node

```
methode remove(index):
    if index == 0:
        temp = first
        first = first.next
        temp.next = null
        count--
    else:
        previous = findNode(index - 1)
        if last == previous.next:
            last = previous
        temp = previous.next
        previous.next = temp.next
        temp.next = null
        count--
```

![Single linked list - remove](../images/sll_remove.jpg)

#### `remove(element)`

- Instead of using `findNode` as in `remove(index)`, `remove(element)` executes a sequential search to find the correct
  node
- Once we find the correct node, we remove it
- But to remove a node, we need to know which node precedes it, so in the sequential search, we need to adjust 2
  references: `previous` and `current`
    - if `previous` is null, we remove the first node
    - we also need to be careful to update `last` if we are removing the last node

```
method remove(element):
    current = first
    previous = null
    while current is not null:
        if current.element.equals(element):
            if previous == null: // if we are at the beginning
                first = current.next
            else:
                previous.next = current.next
            if last == current:
                last = previous
            current.next = null
            count--
            return true
            
        previous = current;
        current = current.next;
        
    return false
```

## Iterators

- An *iterator* is an object used to iterate through all the objects of a collection
- An iterator is used in *for each* loops, or in `while` loops using the `hasNext` and the `next` methods directly, to
  loop over the elements of a collection, without knowing the implementation details of the data structure

- For a linked list (single or double), an iterator holds a reference to the next node to be processed in the list
- We could also keep a reference to the whole list and to the previous node that was processed if we wanted to allow for
  list modifications through the iterator
- Usually, an iterator logically starts **before** the first element
    - when we call `hasNext`, we check if there is at least 1 more node to process
        - the first time, it will actually check if the first node is not null
        - in other words, it will return true if there's at least 1 element in the list
    - then we call `next`, which not only returns the next element, but also moves the current node reference to the
      next one
        - the first call to `next` will return the first element
- An iterator returns the list elements, not the list nodes, to preserve the encapsulation and loose coupling principles
