## Double Linked Lists

- A node in a double linked list is similar to a single linked list node, but it adds a reference to the previous node
  to the existing 2 references in the single linked list node
- The algorithms in the different methods are also similar, but, since it is possible to find the previous node of a
  given node directly because of previous node references are kept in the nodes, the algorithms need to be adapted
    - for example, it is possible to remove the last element in a double linked list from the last node reference,
      without having to iterate through the list, because we can find the node preceding the last node directly from
      within the last node
- we also need to update all the previous an next node references correctly
- **Note**: special cases, such as operations on empty lists, are not all covered in the examples below: it is very
  important to treat these cases carefully and correctly when implementing the methods

![Double linked list with 4 nodes](../images/dll_4nodes.jpg)

### Add at the beginning

- When adding at the beginning, the new node will become the first node
- To add an element at the beginning, we need to
    - create a new node with the old first node as its next node
    - make the new node the previous node of the old first node
    - move the reference to the first node to the new node

![Double linked list - add at the beginning](../images/dll_add_beginning.jpg)

### Add at the end

- When adding at the end, the new node will become the new last node
- To add an element ad the end, we need to
    - create a new node with the old last node as its previous node
    - make the new node the next node of the old last node
    - move the reference to the last node to the new node

![Double linked list - add at the end](../images/dll_add_end.jpg)

### Operations Symmetry

- Double linked lists are symmetrical
    - *add at the beginning* and *add at the end* are symmetrical operations on double linked lists
    - if we reverse the roles of *next* and *previous* references and of the first and last references, we have the same
      operations

### Add (insert) in the middle

- Suppose we already have a reference to the node following where we want to insert the new element, which we call the
  current node
    - this node could be found by index (for example, insert at index `i`), or by other means
- To add an element before this current node, we need to
    - create a new node with
        - the previous node of the current node as its previous node
        - and the current node as its next node
    - make the new node the next node of its previous node
    - make the new node the previous node of its next node

![Double linked list - insert](../images/dll_add_middle.jpg)

### Remove from the beginning

- To remove the first node in the list, we need to
    - move the first node reference to its next node (i.e. the second node in the list)
    - set to null the reference to the next node in the old first node
    - set to null the reference to the previous node in the new first node

![Double linked list - remove from the beginning](../images/dll_remove_beginning.jpg)

### Remove from the end

- Symmetric to *remove from the beginning*
- To remove the last node in the list, we need to
    - move the last node reference to its previous node (i.e. the second to last node in the list)
    - set to null the reference to the previous node in the old last node
    - set to null the reference to the next node in the new last node

![Double linked list - remove from the end](../images/dll_remove_end.jpg)

### Remove from the middle

- Suppose we already have a reference to the node we want to remove, which we call the current node
    - this node could be found by index (for example, insert at index `i`), or by other means
- To remove a node in the middle of the list, we need to
    - move the reference to the previous node in the node following the current node to the node preceding the current
      node
    - move the reference to the next node in the node preceding the current node to the node following the current node
    - set to null the references to the previous and next nodes in the current node
    -

![Double linked list - remove from the middle](../images/dll_remove_middle.jpg)

### Other operations

- The other operations are very similar, if not identical, to those of a single linked list simples, including the
  concept of iterators
    - the only difference is that we could more easily create a revers iterator, which is not very common

### Single linked lists vs. double linked lists

- From the point of view of the user, a double linked list is more flexible than a single linked list
    - the list is symmetrical
    - adding and removing at the beginning and the end are always constant $O(1)$ for a double linked list
        - removing from the end in a single linked list is linear $O(n)$
        - adding at the end of a single linked list is constant if we have a reference to the last node, but it is
          linear if not
    - we can iterate through a double linked list in both directions, which is impossible in a single linked list

- So why do we need a single linked list?
    - a double linked list is more complicated, there are more references to manage
    - the nodes of a double linked list are bigger, they use more memory because of the second reference to another
      node (the previous node)
    - removing from the end of a list is not always necessary, depending on how we use the list (e.g. for a stack)
    - going through the elements in both directions is not always necessary
- If we want to follow the **KIS** (*Keep it simple*) principle, it would be better to use a single linked list if we
  don't need the advantages the double linked list provides

### Implementation with *dummy nodes*

![Double linked list - dummy nodes](../images/dll_dummy.jpg)

- Some linked list implementations (single or double) make use of *dummy nodes* to avoid having to check if the first or
  last node references are null or not
- An empty list in this case will always have 2 nodes (or maybe 1 for a single linked list), a *dummy* first node and
  a *dummy* last node
- If, for example, we need to add a node to the list, we are guaranteed to always have a node before and a node after
  the new node, even if we add it at the beginning or the end
- So we need to check for special cases less often
- Adding a node only has 1 case, it's adding a node in between other nodes

![Double linked list - adding between dummy nodes](../images/dll_dummy_add.jpg)

### Circular linked list

- In a circular list, the next of the last node is the first node
    - and the previous of the first node is the last node
- If we use dummy nodes, then we need only 1 dummy node. between the first and the last nodes
- Since the first and the last nodes are connected, it's not necessary to keep references on both nodes
    - we can keep only 1 reference to the first node, or a reference to the dummy node if we use one, and we can easily
      find the last last from there

![Circular Double linked list](../images/dll_circular.jpg)
