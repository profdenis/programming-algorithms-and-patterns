# Comparison of complexities for different `List` implementations

*Note*: it is assumed that the linked list implementations have a reference to the last node in the list. If not, the
$O(1)$ methods working at the end of the list would become $O(n)$

## Worst case

| Method                      | `ArrayList` | `SingleLinkedList` | `DoubleLinkedList` |
|-----------------------------|-------------|--------------------|--------------------|
| `nodeAt`                    | n/a         | $O(n)$             | $O(n)$             |
| `add` (at the end)          | $O(n)$      | $O(1)$             | $O(1)$             |
| `add` (at the beginning)    | $O(n)$      | $O(1)$             | $O(1)$             |
| `add` (at given index)      | $O(n)$      | $O(n)$             | $O(n)$             |
| `indexOf`                   | $O(n)$      | $O(n)$             | $O(n)$             |
| `remove` (at the end)       | $O(1)$      | $O(n)$             | $O(1)$             |
| `remove` (at the beginning) | $O(n)$      | $O(1)$             | $O(1)$             |
| `remove` (at given index)   | $O(n)$      | $O(n)$             | $O(n)$             |
| `get`, `set`                | $O(1)$      | $O(n)$             | $O(n)$             |
| `isEmpty`, `size`           | $O(1)$      | $O(1)$             | $O(1)$             |

## Best case

*Notes*:

- Be careful with the best case, as it doesn't reflect common use; it's more about if we are lucky, and it's not a very
  indicator of performance
- All cases, including the best case, are for a problem size of $n$, meaning $n$ elements in the list: the best case
  cannot be if the list is empty or contains only 1 element

| Method                      | `ArrayList` | `SingleLinkedList` | `DoubleLinkedList` |
|-----------------------------|-------------|--------------------|--------------------|
| `nodeAt`                    | n/a         | $O(1)$             | $O(1)$             |
| `add` (at the end)          | $O(1)$      | $O(1)$             | $O(1)$             |
| `add` (at the beginning)    | $O(n)$      | $O(1)$             | $O(1)$             |
| `add` (at given index)      | $O(1)$      | $O(1)$             | $O(1)$             |
| `indexOf`                   | $O(1)$      | $O(1)$             | $O(1)$             |
| `remove` (at the end)       | $O(1)$      | $O(n)$             | $O(1)$             |
| `remove` (at the beginning) | $O(n)$      | $O(1)$             | $O(1)$             |
| `remove` (at given index)   | $O(n)$      | $O(n)$             | $O(n)$             |
| `get`, `set`                | $O(1)$      | $O(n)$             | $O(n)$             |
| `isEmpty`, `size`           | $O(1)$      | $O(1)$             | $O(1)$             |

## Average case

| Method                      | `ArrayList` | `SingleLinkedList` | `DoubleLinkedList` |
|-----------------------------|-------------|--------------------|--------------------|
| `nodeAt`                    | n/a         | $O(n)$             | $O(n)$             |
| `add` (at the end)          | $O(1)$      | $O(1)$             | $O(1)$             |
| `add` (at the beginning)    | $O(n)$      | $O(1)$             | $O(1)$             |
| `add` (at given index)      | $O(n)$      | $O(n)$             | $O(n)$             |
| `indexOf`                   | $O(n)$      | $O(n)$             | $O(n)$             |
| `remove` (at the end)       | $O(1)$      | $O(n)$             | $O(1)$             |
| `remove` (at the beginning) | $O(n)$      | $O(1)$             | $O(1)$             |
| `remove` (at given index)   | $O(n)$      | $O(n)$             | $O(n)$             |
| `get`, `set`                | $O(1)$      | $O(n)$             | $O(n)$             |
| `isEmpty`, `size`           | $O(1)$      | $O(1)$             | $O(1)$             |



