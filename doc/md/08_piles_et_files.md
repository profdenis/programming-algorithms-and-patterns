# Stack and queues

- A *stack* is a data structure of type **first in, last out (FILO)** (or *first come, last serve*), or equivalently, of
  type **last in, first out (LIFO)**
- A *queue* is a data structure of type **first in, first out (FIFO)** (or *first come, first serve*), or equivalently,
  of type **last in, last out (LILO)**
- These types define in which order the elements will be added and removed from the data structure
- We don't add and we don't remove elements at/from specific positions in these structures, we instead follow the FILO
  or LIFO vs. FIFO or LILO principles of each structure

## Stacks

- We always interact with the top of the stack:
    - **push**: add an element on the top of the stack
    - **pop**: remove an element from the top of the stack
- That's why a stack is a **last in, first out (LIFO)** data structure because the last element we added to the stack
  will be the first one to be removed
- At the opposite, the first element we added to the stack will be at the bottom, below all the other elements,
  therefore it will be the last one to be removed, all the other elements will have to be removed before we can remove
  the first element we added
- We cannot add elements to a stack in the middle or under the stack, we can only added on the top of the stack
- Same thing about removing elements, we can only remove elements from the top of the stack

- The most common way to visualise a stack is by using an array, but we don't have to implement a stack with an array:
  we can also use a linked list
- To help visualise a stack, the array is represented vertically, with index 0 at the bottom, and the top of the stack
  will be higher, at index
  `t` (or `top`), depending on the number of elements in the stack

![Stack - push](../images/stack_push.jpg)

![Stack - pop](../images/stack_pop.jpg)

## Implementing a stack

- We can either use an array or a linked list to implement a stack
- In both cases, it is easier to delegate the stack methods towards an array list or a linked list object, rather than
  reimplementing the whole code
- So we can keep a field of type `ArrayList` or of type `SingleLinkedList` (there's no need for a double linked list
  here, see below for details)
    - and redirect (delegate) the work to the array or linked list methods
- Doing this avoids writing the same code more than once (the *DRY: Don't repeat yourself* principle), and it simplifies
  the development of a project (less code to write, less code to test, bugs to correct in only one place, ...)


- In some cases, if we only need a stack or a queue and that we need to absolutely optimize the code as much as
  possible, then rewriting the code might be a good idea, but in general, the DRY principle preferable
- For the same reasons, we don't usually need 2 or more different implementations of linked lists, so we will choose
  only one type of linked list, unless we need to optimize the code different purposes
- Therefore, it is better to delegate to an array list or a linked list, and later on, if necessary, for performance
  reasons, we could refactor our stack to implement it directly without using delegation
    - this change should be transparent (no need to modify the code using the stack or queue)

- If we use an array list to implement a stack, we can simply add and remove elements from the end of the list
    - these 2 operations on the end of the array list are constant ($O(1)$), unless we need to increase the capacity of
      the underlying array
    - if we choose a good capacity to start with, we won't increase the capacity often, so it won't significantly impact
      the performance
- If we use a double linked list, then we can add or remove either from the start or the end
    - but we need to choose only, either always add and remove from the end, or add and remove from the start
    - these operations are constant $O(1)$, so we should have a good performance
- But if we use a single linked list, then we should choose the beginning of the list as the top of the stack (add and
  remove at the start of the list) because if we chose the end of the list as the top of the stack, we would have a
  linear complexity $O(n)$ for popping an element
- Similarly, it would not make sense to use index 0 in a stack implemented with an `ArrayList` for the top of the stack,
  because both pushing and popping would become linear

### What are stacks useful for?

- Method call stacks are the most common application of stacks
- Each time a method (or function is called) a *stack frame* is pushed on the call stack
    - the details vary from a programming language to another, and from an operating to another
        - if the program is compiled natively, to be executed directly on the host OS, then the stack frame contents
          will depend on the OS
        - if the language is interpreted or compiled to byte code to be executed on a virtual machine, like Java, then
          the stack frame contents will depend on the virtual machine
    - but in any case, there will be similar data in the stack frame, such as the method parameters, local variables and
      other information, such as where to return to once the method has completed

- Stacks can also be used in many other places, every time we need a LIFO or FILO structure
    - when parsing binary trees, when we don't want to (or cannot) use resursive calls
    - when evaluating mathematical expressions or when compiling programs
    - when traversing a graph
- In general, stacks are used in recursive algorithms
    - with explicit recursive calls using the call stack
    - or with implicit recursivity, in other words when the algorithm doesn't use the call stack with recursive
      functions, but instead manages a stack directly to simulate recursive calls
    - the algorithms of type *backtrack*, which need to go back (or backtrack) to a previous point in the algorithm

## Queues

- We always interact with a queue in this way:
    - **enqueue**: we add an element at the end of the queue
    - **dequeue**: we remove an element from the beginning of the queue
- A queue is used as a *waiting queue*, a structure of type *FIFO or LILO:
    - the first element added is at the front (or beginning) of the queue
    - the last element added is at the back (or end)
    - the first element to come out is at the front
    - the last element to come out is at the back
- There exists also *priority queues*, in which the order within the queue is based on a priority system différent from
  a simple queue
    - we will not discuss this special type of queue here
    - we will concentrate on "ordinary" queues (the FIFO queues)

### Implementing a queue

- The best (or some say the easiest) way to represent a queue is with a linked list, to which we add on one side and
  remove from the other
- But which side exactly do we add? And to which side do we remove?
- We have to consider the complexity of adding and removing from each side, and which type of linked list we want to use
- If we use a double linked list, wether it is cicular or not, we can add and remove from both ends in constant time $O(
  1)$, so we only need to decide which side we are going for the front and the back
    - since a double linked list is symmetric, it doesn't matter which side we choose, as long as we respect the FIFO
      nature of the queue

- If we use a single linked list, we have to careful because removing an element from the end is not constant:
- So we can add the elements at the end, and remove them from the beginning
    - this way, we are only using operations with constant complexity
    - as long as we have a direct reference to the last node in the list

- We can implement the `Queue` class by delegation, like for the stack described earlier
    - our class holds a reference to a list, created by the constructor
    - and we redirect(delegate) the calls to add and remove elements to the list methods

#### Can we implement a queue with an array list?

- Yes, we can, but performance will not be great
    - adding and removing from the beginning of an array, at index 0, is not efficient
    - we won't have a choice to have a linear complexity, either to enqueue or dequeue an element, depending which side
      we choose for the front and the back of the queue
- We could use a circular list implemented by an array list
    - instead of using the number of elements to denote where to add the next element at the end, and 0 to denote the
      index of the first element
        - we could have a variable index for the start, and the same for the end, different from the number of elements
          in the list
    - so the list would not always start at index 0
    - but we would keep the condition that we cannot have holes in the middle of the list, meaning that all the elements
      must be contiguous, without space (or holes) between the elements, but possibly with empty space before the start
      index and after the last index

![Queue - circular array](../images/queue_circular_array.jpg)

- This type of array list is not only useful to implement queues
    - it's also useful for a general purpose array list
- We can visualise the list as having the end of the array (index 5 in the previous example) touching the beginning of
  the array at index 0
    - think of an array that would be drawn on a piece of paper, and that the piece of paper would be bent to make a
      ring, with the end of the array touching the beginning
- After having added the element 3 in the previous example, the end index is 5
    - and, when we want to add 6, we realise that we are going pass the end of the array, so we go back to the
      beginning (we wrap around) and we put the new element at index 0
    - we can use the *modulo* operation (`%` in many programming languages) with the array capacity to wrap around to
      index 0
- We can use the same techniques to increase the capacity of the array when it is full
    - but we have to be careful when we copy the elements into the new array: we cannot leave a hole in the wrong place
        - we need to empty space to be before the first element or after the last element

![Queue - circular array - increase capacity](../images/queue_increase_capacity.jpg)
