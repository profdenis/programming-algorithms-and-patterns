public class Day1 {

    public static int sequentialSearch(int[] arr, int target) {
        for (int i = 0; i < arr.length; i++) {
            if (target == arr[i]) {
                // found
                return i;
            }
        }
        // not found
        return -1;
    }

    public static void main(String[] args) {
        int[] arr1 = {8, 2, 9, 7, 8, 4, 1, 6, 5};
        int[] targets = {4, 10, 8, 0, 7, 6, 100};

        for (int i = 0; i < targets.length; i++) {

            int index = sequentialSearch(arr1, targets[i]);
            if (index != -1) {
                System.out.println("Found " + targets[i] + " at index " + index);
            } else {
                System.out.println(targets[i] + " not found!");
            }
        }
        for (int target : targets) {
            int index = sequentialSearch(arr1, target);
            if (index != -1) {
                System.out.println("Found " + target + " at index " + index);
            } else {
                System.out.println(target + " not found!");
            }
        }
    }
}
