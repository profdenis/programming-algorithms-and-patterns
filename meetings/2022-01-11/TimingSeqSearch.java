import java.util.Random;

public class TimingSeqSearch {

    public static int[] genRandomArray(int length, int max) {
        int[] temp = new int[length];
        Random rand = new Random();

        for (int i = 0; i < temp.length; i++) {
            temp[i] = rand.nextInt(max);
        }
        return temp;
    }

    public static void main(String[] args) {
//        int[] arr1 = {8, 2, 9, 7, 8, 4, 1, 6, 5};
        int n_searches = 10;

        for (int length = 10; length <= 1000000; length *= 10) {
            int[] arr1 = genRandomArray(length, length/2);
            int[] targets = genRandomArray(n_searches, length/2);
            long total = 0;

            int index = Day1.sequentialSearch(arr1, 0);
            for (int i = 0; i < targets.length; i++) {
                long start = System.nanoTime();
                index = Day1.sequentialSearch(arr1, targets[i]);
                long end = System.nanoTime();
                total += end - start;
            }
//            System.out.println(index);

            System.out.printf("%d,%d\n", length, total/n_searches);
        }
    }

}
