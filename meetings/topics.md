# Topics and Exercises

## 2022-01-10

### Topics

- Course outline
- Sequential search
    - algorithm
    - code
- Intro to algo analysis

## 2022-01-11

### Topics

- Cloning a git repo in IntelliJ
- Experimental analysis in Java

## 2022-01-12

### Topics

- Binary search
    - algorithm
    - code
    - ~~theoretical analysis~~
    - experimental analysis

### Exercises

1. Create a `Stopwatch` class with 1 constructor and 4 methods:
    - constructor: doesn't take any argument, it initializes the `elapsed` time field to 0 (similar to the `total`
      variable in the timing examples)
    - `public void start()`: starts the stopwatch (it starts counting the time)
    - `public void end()`: stops counting the time (it updates the elapsed time)
    - `public long getTime()`: get the value in the `elapsed` time field
    - `public void reset()`: reset the `elapsed` time field to 0
2. Modify the timing examples to use your `Stopwatch` class instead of calling `System.nanoTime()` directly (the
   stopwatch will call it instead)
3. Create 2 CSV files: `sequentialSearch.csv` and `binarySearch.csv`
4. Generate execution times for each algorithm and save them in their corresponding files
    - try going beyond 1 million if possible (10 million, 100 million, ...)
5. Using a spreadsheet software (LibreOffice, Excel, ...), create a XY scatter diagram with 2 data series (1 for each
   algorithm)
    - use logarithmic scales for both axes
    - format the diagram properly
    - do the lines follow (approximately) the expected growth predicted by the theoretical analysis?
6. Read lecture notes: `01_searching`

## 2022-01-17

### Topics

- Discussion: exercises from 2022-01-12
- Binary search
    - theoretical analysis
- Selection sort

### Exercises

1. Trace the execution of the selection sort algorithm
    - on paper
    - using the debugger

## 2022-01-18

### Topics

- Insertion sort

### Exercises

1. Trace the execution of the insertion sort algorithm
    - on paper
    - using the debugger

## 2022-01-19

### Exercises

1. Experimental analysis of the 2 sorting algorithms (selection and insertion)
    1. copy the class `TimingSeqSearch` into `TimingSorting`
    2. copy the 2 sorting methods into that class
    3. generate data in a CSV format for each algorithm
2. Using a spreadsheet software, create a diagram for the generated data (2 lines on the same diagram)
3. This will be part of the first assignment, so each student should do it by themselves
    1. but you can compare the results you get with each other to see if you are getting something similar
    2. you can also help each other with the spreadsheet software to create the diagrams

## 2022-01-24

### Topics

- Divide and conquer algorithms
- Merge sort
    - algorithm
    - overview of complexity

### Exercises

1. Trace the execution of the merge sort algorithm
    - on paper
    - using the debugger
2. Work on assignment 1

## 2022-01-25

### Topics

- Quick sort
    - algorithm
    - overview of complexity

### Exercises

1. Trace the execution of the quick sort algorithm
    - on paper
    - using the debugger
2. Work on assignment 1

## 2022-01-26

### Topics

1. Recursive algorithms
    - sum of integers
    - Fibonacci numbers
    - debugging recursive algorithms

### Exercises

1. Work on assignment 1

## 2022-01-31

### Topics

- Generics
- `Comparable` interface
- Generic searching with `Comparable`

### Exercises

1. Work on assignment 1


## 2022-02-01

### Topics

- `Comparator` interface
- Generic searching with `Comparator`
- Generic sorting with `Comparable` and `Comparator`

### Exercises

1. Work on assignment 1

## 2022-02-02

### Topics

- Sorting and searching in the standard library
  - `Arrays`
  - `Map` and `SortedMap`

### Exercises

1. Add methods to `GenericSorting.java`, similar to the `insertionSort` and `selectionSort` methods using `Comparable`
   and `Comparator`, for the other sorting algorithms (`mergeSort`, `quickSort`, `quickSortRandomPivot`, `bubbleSort`
   , `gnomeSort`)
    - do not do this for assignment 1: you only need to sort arrays of `int`s in the assignment
2. Don't forget to submit assignment 1


## 2022-02-07

### Topics

- `List` interface
  - Java Collection Framework JCF
  - 2 Java implementations in the JCF: `ArrayList` vs. `LinkedList`
  - simplified `List` interface for the course in package `com.profdenis.list`
- Array lists (theory)

### Exercises (for the week)

1. Write a Java class with a `main` method doing the following:
   1. create an `ArrayList` of `Person` objects with a capacity of 4
   2. create 4 `Person` objects and add them at the end of the list
   3. create another `Person` object and add it at the end of the list
      - this should trigger the expansion of the underlying array
   4. create another `Person` object and add it at index 2 in the list
   5. tests the other `List` methods on this list by choosing appropriate parameters to test all possibilities
2. Trace the execution of your code on paper and/or with the debugger

## 2022-02-08

### Topics

- Array lists (implementation)


## 2022-02-09

### Topics

- Exercises and exam preparation


## 2022-02-14

- Exam 1 (3 hours)


## 2022-02-15

### Topics

- Single linked list (theory)

### Exercises (for the week)

1. same as previous exercise, but with a single link list


## 2022-02-16

### Topics

- Single linked list (implementation)


## 2022-02-21

### Topics

- Double linked list (theory)
- Double linked list (implementation)

### Exercises (for the week)

1. same as previous exercise, but with a double link list


## 2022-02-22

### Topics

- Double linked list (continued)


## 2022-02-23

### Topics

- Work on assignment 2


## 2022-02-28

### Topics

- Work on assignment 2

## 2022-03-01

### Topics

- Stacks and queues
- Complexity: array lists and linked lists

## 2022-03-02

### Topics

- Work on assignment 2

