package com.profdenis.comparators;

import java.util.Comparator;
import java.util.Objects;

public class Person implements Comparable<Person> {
    private static int nextId = 1;
    private final int id;
    private String name;
    private String email;

    public Person(String name, String email) {
        this.id = nextId;
        nextId++;
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int compareTo(Person o) {
        return Integer.compare(this.id, o.id);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public static class ComparatorId implements Comparator<Person> {

        @Override
        public int compare(Person o1, Person o2) {
            return o1.compareTo(o2);
        }
    }

    public static class ComparatorName implements Comparator<Person> {

        @Override
        public int compare(Person o1, Person o2) {
            return o1.name.compareTo(o2.name);
        }
    }

    public static class ComparatorEmail implements Comparator<Person> {

        @Override
        public int compare(Person o1, Person o2) {
            return o1.email.compareTo(o2.email);
        }
    }

    public static class ComparatorIdReverse implements Comparator<Person> {

        @Override
        public int compare(Person o1, Person o2) {
            return - o1.compareTo(o2);
        }
    }
}
