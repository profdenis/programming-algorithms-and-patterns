package com.profdenis.comparators;

import com.profdenis.searching.SearchingGeneric;
import com.profdenis.sorting.SortingGeneric;

import java.util.Arrays;
import java.util.Comparator;

public class PersonSearchingSorting {

    public static void main(String[] args) {
        Person p1 = new Person("Denis", "denis@example.com");
        Person p2 = new Person("Alice", "alice@example.com");
        Person p3 = new Person("Alice", "alice3@example.com");
        Person p4 = new Person("Alice4", "alice@example.com");
        Person p5 = new Person("Bob", "bob@example.com");
        Person[] people = {p3, p2, p1, p5, p4};

        System.out.println(p1.compareTo(p2));
        Comparator<Person> comp = new Person.ComparatorId();
        System.out.println(comp.compare(p1, p2));

        System.out.println(Arrays.toString(people));

        // sequential search
        System.out.printf("\nSearching for %s with sequential search\n", p1);
        int index = SearchingGeneric.sequentialSearch(people, p1);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        System.out.printf("\nSearching for %s with sequential search\n", p5);
        index = SearchingGeneric.sequentialSearch(people, p5);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        System.out.printf("\nSearching for %s with sequential search\n", p3);
        index = SearchingGeneric.sequentialSearch(people, p3);
        System.out.printf("Found at index %d: %s\n\n\n", index, people[index]);

        // binary search
        SortingGeneric.selectionSort(people);
        System.out.println(Arrays.toString(people));

        System.out.printf("\nSearching for %s with binary search\n", p1);
        index = SearchingGeneric.binarySearch(people, p1);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        System.out.printf("\nSearching for %s with binary search\n", p5);
        index = SearchingGeneric.binarySearch(people, p5);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        System.out.printf("\nSearching for %s with binary search\n", p3);
        index = SearchingGeneric.binarySearch(people, p3);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        // sorting and searching by different keys
        System.out.println("\n\n\nSearching by name");
        Comparator<Person> comparator = new Person.ComparatorName();
        // sorting by name
//        SortingGeneric.selectionSort(people, comparator);
        Arrays.sort(people, comparator);
        System.out.println(Arrays.toString(people));
        System.out.printf("\nSearching for %s with binary search\n", p2);
        index = SearchingGeneric.binarySearch(people, p2, comparator);
        while (index != -1) {
            System.out.printf("Found at index %d: %s\n", index, people[index]);
            index = SearchingGeneric.binarySearch(people, p2, index + 1, comparator);
        }

        // sorting by different keys
        comparator = new Person.ComparatorIdReverse();
        Arrays.sort(people, comparator);
        SortingGeneric.insertionSort(people, comparator);
        System.out.println(Arrays.toString(people));
    }
}
