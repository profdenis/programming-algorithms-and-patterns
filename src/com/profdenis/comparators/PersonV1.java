package com.profdenis.comparators;

import java.util.Objects;

public class PersonV1 implements Comparable<PersonV1> {
    public int id;
    public String name;
    public String email;

    public PersonV1(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonV1 personV1 = (PersonV1) o;
        return id == personV1.id
                && name.equals(personV1.name)
                && email.equals(personV1.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, email);
    }

    @Override
    public int compareTo(PersonV1 o) {
        int comp = Integer.compare(this.id, o.id);
        if (comp == 0) {
            comp = this.name.compareTo(o.name);
        }
        if (comp == 0) {
            comp = this.email.compareTo(o.email);
        }
        return comp;
    }

    @Override
    public String toString() {
        return "PersonV1{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
