package com.profdenis.comparators;

import com.profdenis.searching.SearchingGeneric;
import com.profdenis.sorting.SortingGeneric;

import java.util.Arrays;

public class PersonV1SearchingSorting {

    public static void main(String[] args) {
        PersonV1 p1 = new PersonV1(1, "Denis", "denis@example.com");
        PersonV1 p2 = new PersonV1(2, "Alice", "alice@example.com");
        PersonV1 p3 = new PersonV1(3, "Alice", "alice3@example.com");
        PersonV1 p4 = new PersonV1(4, "Alice4", "alice@example.com");
        PersonV1 p5 = new PersonV1(1, "Bob", "bob@example.com");
        PersonV1[] people = {p1, p2, p3, p4, p5};

        System.out.println(Arrays.toString(people));

        System.out.printf("\nSearching for %s with sequential search\n", p1);
        int index = SearchingGeneric.sequentialSearch(people, p1);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        System.out.printf("\nSearching for %s with sequential search\n", p5);
        index = SearchingGeneric.sequentialSearch(people, p5);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        System.out.printf("\nSearching for %s with sequential search\n", p3);
        index = SearchingGeneric.sequentialSearch(people, p3);
        System.out.printf("Found at index %d: %s\n\n\n", index, people[index]);

        SortingGeneric.selectionSort(people);
        System.out.println(Arrays.toString(people));

        System.out.printf("\nSearching for %s with binary search\n", p1);
        index = SearchingGeneric.binarySearch(people, p1);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        System.out.printf("\nSearching for %s with binary search\n", p5);
        index = SearchingGeneric.binarySearch(people, p5);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        System.out.printf("\nSearching for %s with binary search\n", p3);
        index = SearchingGeneric.binarySearch(people, p3);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

    }
}
