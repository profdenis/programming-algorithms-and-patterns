package com.profdenis.comparators;

import java.util.Objects;

public class PersonV2 implements Comparable<PersonV2> {
    public int id;
    public String name;
    public String email;

    public PersonV2(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonV2 person = (PersonV2) o;
        return id == person.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int compareTo(PersonV2 o) {
        return Integer.compare(this.id, o.id);
    }

    @Override
    public String toString() {
        return "PersonV2{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

}
