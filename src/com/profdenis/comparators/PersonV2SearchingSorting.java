package com.profdenis.comparators;

import com.profdenis.searching.SearchingGeneric;
import com.profdenis.sorting.SortingGeneric;

import java.util.Arrays;

public class PersonV2SearchingSorting {

    public static void main(String[] args) {
        PersonV2 p1 = new PersonV2(1, "Denis", "denis@example.com");
        PersonV2 p2 = new PersonV2(2, "Alice", "alice@example.com");
        PersonV2 p3 = new PersonV2(3, "Alice", "alice3@example.com");
        PersonV2 p4 = new PersonV2(4, "Alice4", "alice@example.com");
        PersonV2 p5 = new PersonV2(1, "Bob", "bob@example.com");
        PersonV2[] people = {p1, p2, p3, p4, p5};

        System.out.println(Arrays.toString(people));

        System.out.printf("\nSearching for %s with sequential search\n", p1);
        int index = SearchingGeneric.sequentialSearch(people, p1);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        System.out.printf("\nSearching for %s with sequential search\n", p5);
        index = SearchingGeneric.sequentialSearch(people, p5);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        System.out.printf("\nSearching for %s with sequential search\n", p3);
        index = SearchingGeneric.sequentialSearch(people, p3);
        System.out.printf("Found at index %d: %s\n\n\n", index, people[index]);

        SortingGeneric.selectionSort(people);
        System.out.println(Arrays.toString(people));

        System.out.printf("\nSearching for %s with binary search\n", p1);
        index = SearchingGeneric.binarySearch(people, p1);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        System.out.printf("\nSearching for %s with binary search\n", p5);
        index = SearchingGeneric.binarySearch(people, p5);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

        System.out.printf("\nSearching for %s with binary search\n", p3);
        index = SearchingGeneric.binarySearch(people, p3);
        System.out.printf("Found at index %d: %s\n", index, people[index]);

    }
}
