package com.profdenis.jcf;

import com.profdenis.comparators.Person;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestJCFList {

    public static void main(String[] args) {
        List<Person> list = new LinkedList<>();
        list.add(new Person("Denis", "denis@example.com"));
        list.add(new Person("Alice", "alice@example.com"));
        list.add(new Person("Alice", "alice3@example.com"));
        list.add(new Person("Alice4", "alice@example.com"));
        list.add(new Person("Bob", "bob@example.com"));

        System.out.println(list);
        for (Person person : list) {
            System.out.println(person);
        }
    }
}
