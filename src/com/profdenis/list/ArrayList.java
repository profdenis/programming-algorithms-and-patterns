package com.profdenis.list;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayList<T> implements List<T> {
    private static final int DEFAULT_CAPACITY = 10;
    private T[] array;
    private int count;

    public ArrayList() {
        this(DEFAULT_CAPACITY);
    }

    public ArrayList(int capacity) {
        this.count = 0;
        array = (T[]) new Object[capacity];
    }

//    @SafeVarargs
//    public ArrayList(T... elements) {
//        this(DEFAULT_CAPACITY, elements);
//    }

    @SafeVarargs
    public ArrayList(int capacity, T... elements) {
        if (elements.length > capacity) {
            capacity = elements.length;
        }
        this.count = elements.length;
        array = (T[]) new Object[capacity];
        System.arraycopy(elements, 0, array, 0, elements.length);
    }

    private void increaseCapacity() {
        increaseCapacity(array.length * 2);
    }
    private void increaseCapacity(int newCapacity) {
        T[] temp = (T[]) new Object[newCapacity];
        System.arraycopy(array, 0, temp, 0, array.length);
        array = temp;
    }

    public int getCapacity() {
        return array.length;
    }

    @Override
    public void add(T element) {
        add(count, element);
    }

    @Override
    public void add(int index, T element) {
        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException();
        }
        if (count == array.length) {
            increaseCapacity();
        }
        //noinspection ManualArrayCopy
        for (int i = count; i > index; i--) {
            array[i] = array[i - 1];
        }
        array[index] = element;
        count++;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        return array[index];
    }

    @Override
    public int indexOf(T element) {
        for (int i = 0; i < count; i++) {
            if (array[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public boolean remove(T element) {
        int index = indexOf(element);
        if (index == -1) {
            return false;
        }
        remove(index);
        return true;
    }

    @Override
    public T remove(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        T temp = array[index];

        for (int i = index; i < count - 1; i++) {
            array[i] = array[i + 1];
        }
        count--;
        array[count] = null;
        return temp;
    }

    @Override
    public T set(int index, T element) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        T temp = array[index];
        array[index] = element;
        return temp;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public Iterator<T> iterator() {
        return new ListIterator();
    }

    @Override
    public String toString() {
        return "(" + count + ", " + array.length + ")" + Arrays.toString(array);
    }

    private class ListIterator implements Iterator<T> {
        private int current = 0;
        @Override
        public boolean hasNext() {
            return current < count;
        }

        @Override
        public T next() {
            if (current >= count) {
                throw new NoSuchElementException();
            }
            T temp = array[current];
            current++;
            return temp;
        }
    }
}
