package com.profdenis.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularList<T> implements List<T> {
    private int count;
    private final Node dummy;

    public CircularList() {
        count = 0;
        dummy = new Node(null);
        dummy.next = dummy;
        dummy.previous = dummy;
    }

    @SafeVarargs
    public CircularList(T... elements) {
        this();
        for (T element : elements) {
            add(element);
        }
    }

    // this is private, so it assumes that index will be valid
    private Node nodeAt(int index) {
        Node current = dummy.next;

        for (int i = 0; i < index; i++) {
            current = current.next;
        }

        return current;
    }

    @Override
    public void add(T element) {
        add(count, element);
    }

    @Override
    public void add(int index, T element) {
        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException();
        }
        Node current = nodeAt(index);
        Node temp = new Node(element, current.previous, current);
        current.previous = temp;
        temp.previous.next = temp;
        count++;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        Node temp = nodeAt(index);
        return temp.element;
    }

    @Override
    public int indexOf(T element) {
        Node current = dummy.next;

        int i = 0;
        while (current != dummy && !current.element.equals(element)) {
            i++;
            current = current.next;
        }
        // if not found
        if (current == dummy) {
            return -1;
        }
        return i;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean remove(T element) {
        int index = indexOf(element);
        if (index == -1) {
            return false;
        }
        remove(index);
        return true;
    }

    @Override
    public T remove(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        Node current = nodeAt(index);
        current.previous.next = current.next;
        current.next.previous = current.previous;
        current.previous = null;
        current.next = null;
        count--;

        return current.element;
    }

    @Override
    public T set(int index, T element) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        Node temp = nodeAt(index);
        T old = temp.element;
        temp.element = element;

        return old;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public Iterator<T> iterator() {
        return new CircularListIterator();
    }

    @Override
    public String toString() {
        StringBuilder temp = new StringBuilder("-> ");

        Node current = dummy.next;
        while (current.next != dummy) {
            temp.append(current.element);
            temp.append(" <--> ");
            current = current.next;
        }
        temp.append(current.element);
        temp.append(" <-");

        return temp.toString();
    }

    private class Node {
        public T element;
        public Node previous;
        public Node next;

        public Node(T element) {
            this.element = element;
            this.previous = null;
            this.next = null;
        }

        public Node(T element, Node previous, Node next) {
            this.element = element;
            this.previous = previous;
            this.next = next;
        }
    }

    private class CircularListIterator implements Iterator<T> {
        private Node current = dummy.next;

        @Override
        public boolean hasNext() {
            return current != dummy;
        }

        @Override
        public T next() {
            if (current == dummy) {
                throw new NoSuchElementException();
            }
            T temp = current.element;
            current = current.next;
            return temp;
        }
    }

    public static void main(String[] args) {
        CircularList<Integer> list = new CircularList<>();
        list.add(5);
        list.add(8);
        list.add(4);
        System.out.println(list);
        System.out.println(list.indexOf(5));
        System.out.println(list.indexOf(8));
        System.out.println(list.indexOf(4));
        System.out.println(list.indexOf(7));
    }
}
