package com.profdenis.list;

import java.util.Iterator;

public interface List<T> {
    void add(T element);

    void add(int index, T element);

    T get(int index);

    int indexOf(T element);

    boolean isEmpty();

    boolean remove(T element);

    T remove(int index);

    T set(int index, T element);

    int size();

    Iterator<T> iterator();
}
