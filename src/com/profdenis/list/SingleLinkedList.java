package com.profdenis.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SingleLinkedList<T> implements List<T> {
    private int count;
    private Node first;
    private Node last;

    public SingleLinkedList() {
        count = 0;
        first = null;
        last = null;
    }

    @SafeVarargs
    public SingleLinkedList(T... elements) {
        this();
        for (T element : elements) {
            add(element);
        }
    }

    // this is private, so it assumes that index will be valid
    private Node nodeAt(int index) {
        Node current = first;

//        int i = 0;
//        while (i < index) {
//            i++;
//            current = current.next;
//        }
        for (int i = 0; i < index; i++) {
            current = current.next;
        }

        return current;
    }

    @Override
    public void add(T element) {
        if (isEmpty()) {
            first = new Node(element);
            last = first;
        } else {
            Node temp = new Node(element);
            last.next = temp;
            last = temp;
//            last.next = new Node(element);
//            last = last.next;
        }
        count++;
    }

    @Override
    public void add(int index, T element) {
        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException();
        }

        if (index == size()) {
            // add at the end
            // empty list with index 0 goes here
            add(element);
            return;
        } else if (index == 0) {
            // add at the beginning
            first = new Node(element, first);
        } else {
            Node previous = nodeAt(index - 1); // step 1
            previous.next = new Node(element, previous.next); // step 2 and 3
        }
        count++;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        Node temp = nodeAt(index);
        return temp.element;
    }

    @Override
    public int indexOf(T element) {
        Node current = first;

        int i = 0;
        while (current != null && !current.element.equals(element)) {
            i++;
            current = current.next;
        }
        // if not found
        if (current == null) {
            return -1;
        }
        return i;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean remove(T element) {
        int index = indexOf(element);
        if (index == -1) {
            return false;
        }
        remove(index);
        return true;
    }

    @Override
    public T remove(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        T element;

        // if only 1 element in the list
        if (first == last) {
            element = first.element;
            first = null;
            last = null;
        } else if (index == 0) { // if remove first element
            Node temp = first;
            first = first.next;
            temp.next = null;
            element = temp.element;
        } else if (index == size() - 1) { // if remove last element
            element = last.element;
            last = nodeAt(size() - 1);
            last.next = null;
        } else { // all other cases
            Node previous = nodeAt(index - 1);
            Node temp = previous.next;
            previous.next = temp.next;
            temp.next = null;
            element = temp.element;
        }

        count--;
        return element;
    }

    @Override
    public T set(int index, T element) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        Node temp = nodeAt(index);
        T old = temp.element;
        temp.element = element;

        return old;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public Iterator<T> iterator() {
        return new SingleLinkedListIterator();
    }

    @Override
    public String toString() {
        StringBuilder temp = new StringBuilder();

        Node current = first;
//        while(current != null) {
//            temp.append(current.element);
//            temp.append(" --> ");
//            current = current.next;
//        }
//        temp.deleteCharAt(temp.length()-1);
//        temp.deleteCharAt(temp.length()-1);
//        temp.append("|");
        while (current != last) {
//        while(current.next != null) {
            temp.append(current.element);
            temp.append(" --> ");
            current = current.next;
        }
        temp.append(current.element);
        temp.append(" --|");

        return temp.toString();
    }

    private class Node {
        public T element;
        public Node next;

        public Node(T element) {
            this(element, null);
        }

        public Node(T element, Node next) {
            this.element = element;
            this.next = next;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "element=" + element +
                    ", next=" + next +
                    '}';
        }
    }

    private class SingleLinkedListIterator implements Iterator<T> {
        private Node current = first;
        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            if (current == null) {
                throw new NoSuchElementException();
            }
            T temp = current.element;
            current = current.next;
            return temp;
        }
    }

    public static void main(String[] args) {
        SingleLinkedList<Integer> list = new SingleLinkedList<>();
        list.add(5);
        list.add(8);
        list.add(4);
        System.out.println(list);
        System.out.println(list.indexOf(5));
        System.out.println(list.indexOf(8));
        System.out.println(list.indexOf(4));
        System.out.println(list.indexOf(7));
    }
}
