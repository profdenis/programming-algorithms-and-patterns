package com.profdenis.list;

import java.util.Iterator;

public class TestArrayList {
    public static void main(String[] args) {
//        ArrayList<String> list = new ArrayList<>();
//        String[] names = new String[3];
//        names[0] = "Denis";
//        names[1] = "Alice";
//        names[2] = "Bob";
//        ArrayList<String> list = new ArrayList<>(names);
        ArrayList<String> list = new ArrayList<>(1, "Denis", "Alice", "Bob");
        System.out.println("Size = " + list.size());
        System.out.println("isEmpty? " + list.isEmpty());

        list.add("Denis");
        list.add("Alice");

        System.out.println("Size = " + list.size());
        System.out.println("isEmpty? " + list.isEmpty());

        Iterator<String> iter = list.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

        list.add(1, "Bob");
        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println(list.getCapacity());

        list.add(0, "John");
        list.add(0, "Jane");
        System.out.println(list.getCapacity());
        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        list.remove("Bob");

        System.out.println(list);
//        for (String name: list) {
//            System.out.println(name);
//        }
    }
}
