package com.profdenis.recursive;

public class Fibonacci {
    public static int fiboRec(int n) {
        if (n == 0 || n == 1) {
            return n;
        }
        // the temp variables are not necessary, and would not normally be used
        // but debugging (and understanding) recursive functions without these temp variables is more difficult
        int temp1 = fiboRec(n-1);
        int temp2 = fiboRec(n-2);
        return temp1 + temp2;
//        return fiboRec(n-1) + fiboRec(n-2);
    }

    public static int fiboIter(int n) {
        int f_i2 = 0;
        int f_i1 = 1;
        int f_i = 1;
        if (n == 0 || n == 1) {
            return n;
        }
        for (int i = 2; i <= n; i++) {
            f_i = f_i1 + f_i2;
            f_i2 = f_i1;
            f_i1 = f_i;
        }
        return f_i;
    }

    public static void main(String[] args) {
        System.out.printf("fiboRec(5) --> %s\n", fiboRec(5));
        System.out.printf("fiboRec(25) --> %s\n", fiboRec(25));
        System.out.printf("fiboIter(5) --> %s\n", fiboIter(5));
        System.out.printf("fiboIter(25) --> %s\n", fiboIter(25));
        System.out.printf("fiboIter(50) --> %s\n", fiboIter(50));
    }

}
