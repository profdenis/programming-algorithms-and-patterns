package com.profdenis.recursive;

public class Sum {
    public static int sumIter(int n) {
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum += i;
        }
        return sum;
    }

    public static int sumRec(int n) {
        if (n < 1) {
            return 0;
        }
        return n + sumRec(n-1);
    }

    public static void main(String[] args) {
        System.out.printf("sumIter(5) --> %s\n", sumIter(5));
        System.out.printf("sumIter(25) --> %s\n", sumIter(25));
        System.out.printf("sumRec(5) --> %s\n", sumRec(5));
        System.out.printf("sumRec(25) --> %s\n", sumRec(25));
    }
}
