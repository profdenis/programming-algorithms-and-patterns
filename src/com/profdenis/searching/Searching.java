package com.profdenis.searching;

import java.util.Arrays;

public class Searching {
    public static int sequentialSearch(int[] arr, int target) {
        for (int i = 0; i < arr.length; i++) {
            if (target == arr[i]) {
                return i;
            }
        }
        return -1;
    }

    public static <T> int sequentialSearch(T[] arr, T target) {
        for (int i = 0; i < arr.length; i++) {
            if (target.equals(arr[i])) {
                return i;
            }
        }
        return -1;
    }

    public static int binarySearch(int[] arr, int target) {
        int start = 0;
        int middle = arr.length / 2;
        int end = arr.length - 1;
        while (start <= end) {
            if (target == arr[middle]) {
                return middle;
            }
            if (target < arr[middle]) {
                end = middle - 1;
            } else {
                start = middle + 1;
            }
            middle = (start + end) / 2;
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println("sequentialSearch");
        int[] arr1 = {5, 2, 8, 7, 3, 1, 4, 6, 9};
        System.out.println("target 7 --> index " + sequentialSearch(arr1, 7));

        System.out.println("binarySearch");
        int[] arr2 = {1, 2, 4, 5, 7, 8, 9, 12};
        System.out.println("target 7 --> index " + binarySearch(arr2, 7));

        Arrays.sort(arr1);
        for (int x : arr1) {
            System.out.println(x);
        }
        System.out.println("target 7 --> index " + binarySearch(arr1, 7));
    }
}
