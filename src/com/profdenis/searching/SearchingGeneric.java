package com.profdenis.searching;

import java.util.Arrays;
import java.util.Comparator;

public class SearchingGeneric {
    public static <T> int sequentialSearch(T[] arr, T target) {
        return sequentialSearch(arr, target, 0);
    }

    public static <T> int sequentialSearch(T[] arr, T target, int start) {
        for (int i = start; i < arr.length; i++) {
            if (target.equals(arr[i])) {
                return i;
            }
        }
        return -1;
    }

    public static <T> int sequentialSearch(T[] arr, T target, Comparator<T> comparator) {
        return sequentialSearch(arr, target, 0, comparator);
    }

    public static <T> int sequentialSearch(T[] arr, T target, int start, Comparator<T> comparator) {
        for (int i = start; i < arr.length; i++) {
            if (comparator.compare(target, arr[i]) == 0) {
                return i;
            }
        }
        return -1;
    }

    public static <T extends Comparable<T>> int binarySearch(T[] arr, T target) {
        return binarySearch(arr, target, 0);
    }

    public static <T extends Comparable<T>> int binarySearch(T[] arr, T target, int start) {
        return binarySearch(arr, target, start, Comparator.naturalOrder());
    }

    public static <T> int binarySearch(T[] arr, T target, Comparator<T> comparator) {
        return binarySearch(arr, target, 0, comparator);
    }

    public static <T> int binarySearch(T[] arr, T target, int start, Comparator<T> comparator) {
        int middle = arr.length / 2;
        int end = arr.length - 1;
        while (start <= end) {
            int compare = comparator.compare(target, arr[middle]);
            if (compare <= 0) {
                end = middle - 1;
            } else {
                start = middle + 1;
            }
            middle = (start + end) / 2;
        }
        // if we ended up before the first element (with end = -1), the calculation of middle is wrong because
        // (start + end) / 2 = (0 + -1) / 2 = -1 / 2 = 0 with integer arithmetic
        // we would need middle = -1 in this case
        // we need middle to be 1 before the potential match
        if (end == -1) {
            middle = -1;
        }
        if (middle < arr.length && comparator.compare(target, arr[middle + 1]) == 0) {
            return middle + 1;
        }
        return -1;

    }

    public static void main(String[] args) {
        System.out.println("sequentialSearch");
        Integer[] arr1 = {5, 2, 8, 7, 3, 1, 4, 6, 9};
        System.out.println("target 7 --> index " + sequentialSearch(arr1, 7));

        System.out.println("binarySearch");
        Integer[] arr2 = {1, 2, 4, 5, 7, 8, 9, 12};
        System.out.println("target 7 --> index " + binarySearch(arr2, 7));

        Arrays.sort(arr1);
        System.out.println(Arrays.toString(arr1));
        System.out.println("target 7 --> index " + binarySearch(arr1, 7));

        Integer[] arr3 = {1, 2, 4, 4, 4, 4, 5, 8, 9, 12};
        System.out.println(Arrays.toString(arr3));
        int target = 4;
        int index = sequentialSearch(arr3, target);
        while (index != -1) {
            System.out.println("target 4 --> index " + index);
//            index = binarySearch(arr3, target, index + 1);
            // in this case, it's better to use a sequential search instead of a binary search again. Why?
            index = sequentialSearch(arr3, target, index + 1);
        }

    }
}
