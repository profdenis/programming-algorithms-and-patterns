package com.profdenis.searching;

import com.profdenis.comparators.Person;

import java.util.*;

public class SearchingStdLib {
    public static void main(String[] args) {
        Integer[] arr = {12, 65, 43, 5, 76, 25, 22, 43, 78};
        System.out.println(Arrays.toString(arr));

        // indexOf only on lists, and lists cannot contain int, so need objects such as Integer
        System.out.printf("Index of 25: %d\n", Arrays.asList(arr).indexOf(25));
        System.out.printf("Index of 2: %d\n", Arrays.asList(arr).indexOf(2));
        System.out.printf("Index of 43: %d\n", Arrays.asList(arr).indexOf(43));

        // sort and binarySearch work on both arrays of int and arrays of Integer
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
        System.out.printf("Binary search for 25: %d\n", Arrays.binarySearch(arr, 25));
        System.out.printf("Binary search for 2: %d\n", Arrays.binarySearch(arr, 2));
        System.out.printf("Binary search for 43: %d\n", Arrays.binarySearch(arr, 43));

        // Person array
        Person p1 = new Person("Denis", "denis@example.com");
        Person p2 = new Person("Alice", "alice@example.com");
        Person p3 = new Person("Alice", "alice3@example.com");
        Person p4 = new Person("Alice4", "alice@example.com");
        Person p5 = new Person("Bob", "bob@example.com");
        Person[] people = {p1, p2, p3, p4, p5};

        System.out.println(Arrays.toString(people));
        Arrays.sort(people, new Person.ComparatorName());
        System.out.println("Sorted by name: " + Arrays.toString(people));
        Arrays.sort(people);
        System.out.println("Sorted by id (default order): " + Arrays.toString(people));
        Arrays.sort(people, new Person.ComparatorEmail());
        System.out.println("Sorted by email: " + Arrays.toString(people));

        // use a Map to find people by some key easily
        // map with ids as keys, Person objects as values
        // Map interface: not guarantee the keys will be kept in order (but they could be)
        Map<Integer, Person> byId = new HashMap<Integer, Person>();
        for (Person p : people) {
            byId.put(p.getId(), p);
        }
        System.out.println("\nGet person by id = 3: " + byId.get(3));
        System.out.println("Get person by id = 6: " + byId.get(6));
        for (Integer id : byId.keySet()) {
            System.out.printf("id %d --> %s\n", id, byId.get(id));
        }

        // map with emails as keys, Person objects as values
        // problem: 2 people with same email
        Map<String, Person> byEmail = new HashMap<>();
        for (Person p : people) {
            byEmail.put(p.getEmail(), p);
        }
        System.out.println("\nGet person by email=denis@example.com: " + byEmail.get("denis@example.com"));
        System.out.println("Get person by email=alice@example.com: " + byEmail.get("alice@example.com"));
        for (String email : byEmail.keySet()) {
            System.out.printf("email %s --> %s\n", email, byEmail.get(email));
        }

        // SortedMap: keys are kept in order
        // but getting and putting in the map are slower
        SortedMap<Integer, Person> byIdSorted = new TreeMap<>();
        for (Person p : people) {
            byIdSorted.put(p.getId(), p);
        }
        System.out.println("\nGet person by id = 3: " + byIdSorted.get(3));
        System.out.println("Get person by id = 6: " + byIdSorted.get(6));
        for (int id : byIdSorted.keySet()) {
            System.out.printf("id %d --> %s\n", id, byIdSorted.get(id));
        }

        // map with emails as keys, Person objects as values
        // problem: 2 people with same email
        SortedMap<String, Person> byEmailSorted = new TreeMap<>();
        for (Person p : people) {
            byEmailSorted.put(p.getEmail(), p);
        }
        System.out.println("\nGet person by email=denis@example.com: " + byEmailSorted.get("denis@example.com"));
        System.out.println("Get person by email=alice@example.com: " + byEmailSorted.get("alice@example.com"));
        for (String email : byEmailSorted.keySet()) {
            System.out.printf("email %s --> %s\n", email, byEmailSorted.get(email));
        }

        // to allow many people for the same key in a map, use values of different types
        // for example, we can use values of type Set<Person>
        Map<String, Set<Person>> byName = new HashMap<>();
        for (Person p : people) {
            // if name is not already a key in the map
            if (!byName.containsKey(p.getName())) {
                // add the key associated to an empty set
                byName.put(p.getName(), new HashSet<>());
            }
            byName.get(p.getName()).add(p);

        }
        System.out.println("\nGet person by name=Denis: " + byName.get("Denis"));
        System.out.println("Get person by name=Alice: " + byName.get("Alice"));
        for (String name : byName.keySet()) {
            System.out.printf("name %s --> %s\n", name, byName.get(name));
        }
    }
}
