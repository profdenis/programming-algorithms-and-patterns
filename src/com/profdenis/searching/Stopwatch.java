package com.profdenis.searching;

public class Stopwatch {

    private long elapsed;
    private long start;

    public Stopwatch () {
        elapsed = 0;
    }

    public void start() {
        start = System.nanoTime();
    }

    public void end() {
//        long end = System.nanoTime();
//        elapsed += end - start;
        elapsed += System.nanoTime() - start;
    }

    public long getTime() {
        return elapsed;
    }

    public void reset() {
        elapsed = 0;
    }
}
