package com.profdenis.searching;

import java.util.Arrays;
import java.util.Random;

public class TimingBinarySearch {

    public static int[] genRandomArray(int length, int max) {
        int[] temp = new int[length];
        Random rand = new Random();

        for (int i = 0; i < temp.length; i++) {
            temp[i] = rand.nextInt(max);
        }
        return temp;
    }

    public static void main(String[] args) {
        int n_searches = 10;

        Stopwatch sw = new Stopwatch();
        for (int length = 10; length <= 1_000_000_000; length *= 10) {
            int[] arr = genRandomArray(length, length / 2);
            Arrays.sort(arr);
            int[] targets = genRandomArray(n_searches, length / 2);

            // the first call is often very slow because of factors out of the control of our program;
            // the memory management, the compiler, the inclusion of code from another class or package, ... can slow
            // down the first call, so we skip it in the timings
            int skipped = Searching.binarySearch(arr, 0);

//            long total = 0;
            sw.reset();
            for (int target : targets) {
//                long start = System.nanoTime();
                sw.start();
                int index = Searching.binarySearch(arr, target);
//                long end = System.nanoTime();
//                total += end - start;
                sw.end();
            }
            System.out.printf("%d,%d\n", length, sw.getTime() / n_searches);
        }

    }
}
