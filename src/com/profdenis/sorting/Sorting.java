package com.profdenis.sorting;

import com.profdenis.utils.Utils;

import java.util.Arrays;
import java.util.Random;

public class Sorting {

    private static final Random rand = new Random();

    public static boolean isSorted(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                return false;
            }
        }
        return true;
    }

    public static void selectionSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int min = arr[i];
            int minPos = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[minPos]) {
                    min = arr[j];
                    minPos = j;
                }
            }
            arr[minPos] = arr[i];
            arr[i] = min;
        }
    }

    public static void insertionSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int x = arr[i];
            int j = i;
            while (j > 0 && arr[j - 1] > x) {
                arr[j] = arr[j - 1];
                j--;
            }
            arr[j] = x;
        }
    }

    public static void mergeSort(int[] arr) {
        mergeSort(arr, 0, arr.length - 1);
    }

    private static void mergeSort(int[] arr, int start, int end) {
        int n = end - start + 1;
        if (n <= 1) {
            return;
        }
        int middle = (start + end + 1) / 2;
        mergeSort(arr, start, middle - 1);
        mergeSort(arr, middle, end);
        merge(arr, start, middle, end);

    }

    private static void merge(int[] arr, int start, int middle, int end) {
        int n = end - start + 1;
        int[] temp = new int[n];
        int i = start;
        int j = middle;
        int k = 0;
        while (k < n) {
            while (i < middle && (j > end || arr[i] <= arr[j])) {
                temp[k] = arr[i];
                i++;
                k++;
            }
            while (j <= end && (i >= middle || arr[j] <= arr[i])) {
                temp[k] = arr[j];
                j++;
                k++;
            }
        }

        for (k = 0; k < n; k++) {
            arr[start + k] = temp[k];
        }
    }

    public static void quickSort(int[] arr) {
        quickSort(arr, 0, arr.length - 1);
    }

    private static void quickSort(int[] arr, int start, int end) {
        int n = end - start + 1;
        if (n <= 1) {
            return;
        }
        int p = partitionLomuto(arr, start, end);
        quickSort(arr, start, p - 1);
        quickSort(arr, p + 1, end);
    }

    public static void quickSortRandomPivot(int[] arr) {
        quickSortRandomPivot(arr, 0, arr.length - 1);
    }

    private static void quickSortRandomPivot(int[] arr, int start, int end) {
        int n = end - start + 1;
        if (n <= 1) {
            return;
        }
        int a = rand.nextInt(start, end + 1);
        int temp = arr[a];
        arr[a] = arr[end];
        arr[end] = temp;
        int p = partitionLomuto(arr, start, end);
        quickSortRandomPivot(arr, start, p - 1);
        quickSortRandomPivot(arr, p + 1, end);
    }

    private static int partitionLomuto(int[] arr, int start, int end) {
        int pivot = arr[end];
        int i = start;
        for (int j = start; j < end; j++) {
            if (arr[j] < pivot) {
                int temp = arr[j];
                arr[j] = arr[i];
                arr[i] = temp;
                i++;
            }
        }
        int temp = arr[i];
        arr[i] = arr[end];
        arr[end] = temp;
        return i;
    }


    public static void main(String[] args) {
        int[] arr1 = Utils.genRandomIntArray(100, 1);
//        int[] arr1 = {5, 4, 6, 2, 9, 3, 7};
//        int[] arr1 = {5, 5, 5, 5, 5, 5, 5};
//        int[] arr1 = {26, 17, 4, 70, 96, 81, 57, 6, 99, 71};
        int[] arr2 = arr1.clone();
        int[] arr3 = arr1.clone();

        System.out.printf("arr1 isSorted? %b --- %s\n", isSorted(arr1), Arrays.toString(arr1));
        System.out.println("sorting arr1 with merge sort");
        mergeSort(arr1);
        System.out.printf("arr1 isSorted? %b --- %s\n", isSorted(arr1), Arrays.toString(arr1));

        System.out.printf("\narr2 isSorted? %b --- %s\n", isSorted(arr2), Arrays.toString(arr2));
        System.out.println("sorting arr2 with quick sort Lomuto sort");
        quickSort(arr2);
        System.out.printf("arr2 isSorted? %b --- %s\n", isSorted(arr2), Arrays.toString(arr2));

        System.out.printf("\narr3 isSorted? %b --- %s\n", isSorted(arr3), Arrays.toString(arr3));
        System.out.println("sorting arr3 with quick sort random pivot");
        quickSortRandomPivot(arr3);
        System.out.printf("arr3 isSorted? %b --- %s\n", isSorted(arr3), Arrays.toString(arr3));
    }
}
