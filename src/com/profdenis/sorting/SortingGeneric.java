package com.profdenis.sorting;

import com.profdenis.utils.Utils;
import jdk.jshell.spi.ExecutionControl;

import java.util.Arrays;
import java.util.Comparator;

public class SortingGeneric {
    public static <T extends Comparable<T>> boolean isSorted(T[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i].compareTo(arr[i + 1]) > 0) {
                return false;
            }
        }
        return true;
    }

    public static <T extends Comparable<T>> void selectionSort(T[] arr) {
//        selectionSort(arr, Comparator.naturalOrder());
        for (int i = 0; i < arr.length - 1; i++) {
            T min = arr[i];
            int minPos = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j].compareTo(arr[minPos]) < 0) {
                    min = arr[j];
                    minPos = j;
                }
            }
            arr[minPos] = arr[i];
            arr[i] = min;
        }
    }

    public static <T> void selectionSort(T[] arr, Comparator<T> comparator) {
        for (int i = 0; i < arr.length - 1; i++) {
            T min = arr[i];
            int minPos = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (comparator.compare(arr[j], arr[minPos]) < 0) {
                    min = arr[j];
                    minPos = j;
                }
            }
            arr[minPos] = arr[i];
            arr[i] = min;
        }
    }

    public static <T extends Comparable<T>> void insertionSort(T[] arr) {
        insertionSort(arr, Comparator.naturalOrder());
    }

    public static <T> void insertionSort(T[] arr, Comparator<T> comparator) {
        for (int i = 0; i < arr.length; i++) {
            T x = arr[i];
            int j = i;
            while (j > 0 && comparator.compare(arr[j - 1], x) > 0) {
                arr[j] = arr[j - 1];
                j--;
            }
            arr[j] = x;
        }
    }

    public static void main(String[] args) {
        Integer[] arr1 = Utils.genRandomIntegerArray(10, 100);
        Integer[] arr2 = arr1.clone();
        System.out.printf("arr1 isSorted? %b --- %s\n", isSorted(arr1), Arrays.toString(arr1));
        System.out.println("sorting arr1 with selection sort");
        selectionSort(arr1);
        System.out.printf("arr1 isSorted? %b --- %s\n", isSorted(arr1), Arrays.toString(arr1));

        System.out.printf("\narr2 isSorted? %b --- %s\n", isSorted(arr2), Arrays.toString(arr2));
        System.out.println("sorting arr2 with insertion sort");
        insertionSort(arr2);
        System.out.printf("arr2 isSorted? %b --- %s\n", isSorted(arr2), Arrays.toString(arr2));
    }
}
