package com.profdenis.stackqueue;

import com.profdenis.list.ArrayList;

public class ArrayQueue<T> implements Queue<T> {

    private final ArrayList<T> list;

    public ArrayQueue() {
        list = new ArrayList<>();
    }

    public ArrayQueue(int capacity) {
        list = new ArrayList<>(capacity);
    }

    @Override
    public void enqueue(T element) {
        list.add(element);
    }

    @Override
    public T dequeue() {
        return list.remove(0);
    }

    @Override
    public T peek() {
        return list.get(0);
    }

    @Override
    public int size() {
        return list.size();
    }
}
