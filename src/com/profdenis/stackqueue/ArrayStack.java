package com.profdenis.stackqueue;

import com.profdenis.list.ArrayList;

public class ArrayStack<T> implements Stack<T> {

    private final ArrayList<T> list;

    public ArrayStack() {
        list = new ArrayList<>();
    }

    public ArrayStack(int capacity) {
        list = new ArrayList<>(capacity);
    }

    @Override
    public void push(T element) {
        list.add(element);
    }

    @Override
    public T pop() {
        return list.remove(list.size()-1);
    }

    @Override
    public T peek() {
        return list.get(list.size()-1);
    }

    @Override
    public int size() {
        return list.size();
    }
}
