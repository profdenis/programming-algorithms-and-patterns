package com.profdenis.stackqueue;

import com.profdenis.list.CircularList;


public class LinkedQueue<T> implements Queue<T> {

    private final CircularList<T> list;

    public LinkedQueue() {
        list = new CircularList<>();
    }

    @Override
    public void enqueue(T element) {
        list.add(element);
    }

    @Override
    public T dequeue() {
        return list.remove(0);
    }

    @Override
    public T peek() {
        return list.get(0);
    }

    @Override
    public int size() {
        return list.size();
    }
}
