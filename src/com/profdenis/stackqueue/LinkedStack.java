package com.profdenis.stackqueue;

import com.profdenis.list.SingleLinkedList;

import java.util.EmptyStackException;

public class LinkedStack<T> implements Stack<T> {

    private final SingleLinkedList<T> list = new SingleLinkedList<>();

    public LinkedStack() {
//        list = new SingleLinkedList<>();
    }

    @SafeVarargs
    public LinkedStack(T... elements) {
//        list = new SingleLinkedList<>();
        for (T element : elements) {
            list.add(0, element);
        }
    }

    @Override
    public void push(T element) {
        list.add(0, element);
    }

    @Override
    public T pop() {
        if (list.isEmpty()) {
            throw new EmptyStackException();
        }
        return list.remove(0);
    }

    @Override
    public T peek() {
//        if (size() == 0) {
        if (list.isEmpty()) {
            throw new EmptyStackException();
        }
        return list.get(0);
    }

//    @Override
//    public T peek() {
//        try {
//            return list.get(0);
//        } catch (IndexOutOfBoundsException e) {
//            throw new EmptyStackException();
//        }
//    }

    @Override
    public int size() {
        return list.size();
    }
}
