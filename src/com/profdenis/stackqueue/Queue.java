package com.profdenis.stackqueue;

public interface Queue<T> {
    void enqueue(T element);
    T dequeue();
    T peek();
    int size();
}
