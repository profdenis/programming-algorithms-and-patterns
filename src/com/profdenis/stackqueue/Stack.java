package com.profdenis.stackqueue;

public interface Stack<T> {
    void push(T element);
    T pop();
    T peek();
    int size();
}
