package com.profdenis.utils;

import java.util.Random;

public class Utils {
    public static int[] genRandomIntArray(int length, int max) {
        int[] temp = new int[length];
        Random rand = new Random();

        for (int i = 0; i < temp.length; i++) {
            temp[i] = rand.nextInt(max);
        }
        return temp;
    }

    public static Integer[] genRandomIntegerArray(int length, int max) {
        Integer[] temp = new Integer[length];
        Random rand = new Random();

        for (int i = 0; i < temp.length; i++) {
            temp[i] = rand.nextInt(max);
        }
        return temp;
    }
}
