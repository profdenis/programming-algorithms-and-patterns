package com.profdenis.comparators;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonV1Test {

    PersonV1 p1 = new PersonV1(1, "Denis", "denis@example.com");
    PersonV1 p2 = new PersonV1(2, "Alice", "alice@example.com");
    PersonV1 p3 = new PersonV1(3, "Alice", "alice3@example.com");
    PersonV1 p4 = new PersonV1(4, "Alice4", "alice@example.com");
    PersonV1 p5 = new PersonV1(1, "Bob", "bob@example.com");

    @Test
    void testEquals() {
        assertEquals(p1, p1);
        assertNotEquals(p1, p2);
        assertNotEquals(p2, p3);
        assertNotEquals(p2, p4);
        assertNotEquals(p3, p4);
        assertNotEquals(p1, p5);
    }

    @Test
    void testHashCode() {
        assertEquals(p1.hashCode(), p1.hashCode());
        assertNotEquals(p1.hashCode(), p2.hashCode());
        assertNotEquals(p2.hashCode(), p3.hashCode());
        assertNotEquals(p2.hashCode(), p4.hashCode());
        assertNotEquals(p3.hashCode(), p4.hashCode());
        assertNotEquals(p1.hashCode(), p5.hashCode());
    }

    @Test
    void compareTo() {
        //noinspection EqualsWithItself
        assertEquals(0, p1.compareTo(p1));
        assertTrue(p1.compareTo(p2) < 0);
        assertTrue(p2.compareTo(p3) < 0);
        assertTrue(p4.compareTo(p2) > 0);
        assertTrue(p4.compareTo(p3) > 0 );
        assertNotEquals(0, p1.compareTo(p5));

    }
}