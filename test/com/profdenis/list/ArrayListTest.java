package com.profdenis.list;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class ArrayListTest {

    ArrayList<Integer> emptyListCap4;
    ArrayList<Integer> listCap4Size3;

    @BeforeEach
    void setUp() {
        emptyListCap4 = new ArrayList<>(4);
        listCap4Size3 = new ArrayList<>(4, 1, 2, 3);
    }

    @Test
    void isEmpty() {
        assertTrue(emptyListCap4.isEmpty());

        assertFalse(listCap4Size3.isEmpty());
    }

    @Test
    void size() {
        assertEquals(0, emptyListCap4.size());

        assertEquals(3, listCap4Size3.size());
    }

    @Test
    void getCapacity() {
        assertEquals(4, emptyListCap4.getCapacity());

        assertEquals(4, listCap4Size3.getCapacity());
    }

    @Test
    void indexOf() {
        assertEquals(-1, emptyListCap4.indexOf(2));

        assertEquals(0, listCap4Size3.indexOf(1));
        assertEquals(1, listCap4Size3.indexOf(2));
        assertEquals(2, listCap4Size3.indexOf(3));
        assertEquals(-1, listCap4Size3.indexOf(4));
    }

    @Test
    void get() {
        assertThrows(IndexOutOfBoundsException.class, () -> emptyListCap4.get(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> emptyListCap4.get(0));

        assertThrows(IndexOutOfBoundsException.class, () -> listCap4Size3.get(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> listCap4Size3.get(3));
        assertEquals(1, listCap4Size3.get(0));
        assertEquals(2, listCap4Size3.get(1));
        assertEquals(3, listCap4Size3.get(2));
    }

    @Test
    void set() {
        assertThrows(IndexOutOfBoundsException.class, () -> emptyListCap4.set(-1, 5));
        assertThrows(IndexOutOfBoundsException.class, () -> emptyListCap4.set(0, 5));

        assertThrows(IndexOutOfBoundsException.class, () -> listCap4Size3.set(-1, 5));
        assertThrows(IndexOutOfBoundsException.class, () -> listCap4Size3.set(3, 5));
        assertEquals(1, listCap4Size3.set(0, 5));
        assertEquals(5, listCap4Size3.get(0));
    }

    @Test
    void addElement() {
        emptyListCap4.add(5);
        assertFalse(emptyListCap4.isEmpty());
        assertEquals(1, emptyListCap4.size());
        assertEquals(4, emptyListCap4.getCapacity());
        assertEquals(5, emptyListCap4.get(0));

        listCap4Size3.add(5);
        assertFalse(listCap4Size3.isEmpty());
        assertEquals(4, listCap4Size3.size());
        assertEquals(4, listCap4Size3.getCapacity());
        assertEquals(5, listCap4Size3.get(3));

        listCap4Size3.add(6);
        assertFalse(listCap4Size3.isEmpty());
        assertEquals(5, listCap4Size3.size());
        assertTrue(4 < listCap4Size3.getCapacity());
        assertEquals(6, listCap4Size3.get(4));
    }

    @Test
    void addIndexElement() {
        assertThrows(IndexOutOfBoundsException.class, () -> emptyListCap4.add(-1, 5));
        assertThrows(IndexOutOfBoundsException.class, () -> emptyListCap4.add(1, 5));
        emptyListCap4.add(0, 5);
        assertFalse(emptyListCap4.isEmpty());
        assertEquals(1, emptyListCap4.size());
        assertEquals(4, emptyListCap4.getCapacity());
        assertEquals(5, emptyListCap4.get(0));

        assertThrows(IndexOutOfBoundsException.class, () -> listCap4Size3.set(-1, 5));
        assertThrows(IndexOutOfBoundsException.class, () -> listCap4Size3.set(4, 5));
        listCap4Size3.add(2, 5);
        assertFalse(listCap4Size3.isEmpty());
        assertEquals(4, listCap4Size3.size());
        assertEquals(4, listCap4Size3.getCapacity());
        assertEquals(5, listCap4Size3.get(2));
        assertEquals(3, listCap4Size3.get(3));

        listCap4Size3.add(0, 6);
        assertFalse(listCap4Size3.isEmpty());
        assertEquals(5, listCap4Size3.size());
        assertTrue(4 < listCap4Size3.getCapacity());
        assertEquals(6, listCap4Size3.get(0));
        assertEquals(1, listCap4Size3.get(1));
        assertEquals(2, listCap4Size3.get(2));
        assertEquals(5, listCap4Size3.get(3));
        assertEquals(3, listCap4Size3.get(4));
    }

    @Test
    void removeElement() {
        assertFalse(emptyListCap4.remove(Integer.valueOf(5)));
        assertTrue(emptyListCap4.isEmpty());
        assertEquals(0, emptyListCap4.size());
        assertEquals(4, emptyListCap4.getCapacity());

        assertFalse(listCap4Size3.remove(Integer.valueOf(0)));
        assertTrue(listCap4Size3.remove(Integer.valueOf(1)));
        assertEquals(2, listCap4Size3.get(0));
        assertEquals(3, listCap4Size3.get(1));
        assertTrue(listCap4Size3.remove(Integer.valueOf(3)));
        assertEquals(2, listCap4Size3.get(0));
        assertTrue(listCap4Size3.remove(Integer.valueOf(2)));
        assertTrue(listCap4Size3.isEmpty());
        assertEquals(0, listCap4Size3.size());
        assertEquals(4, listCap4Size3.getCapacity());
    }

    @Test
    void removeIndex() {
        assertThrows(IndexOutOfBoundsException.class, () -> emptyListCap4.remove(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> emptyListCap4.remove(0));
        assertTrue(emptyListCap4.isEmpty());
        assertEquals(0, emptyListCap4.size());
        assertEquals(4, emptyListCap4.getCapacity());

        assertThrows(IndexOutOfBoundsException.class, () -> listCap4Size3.remove(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> listCap4Size3.remove(3));
        assertEquals(1, listCap4Size3.remove(0));
        assertEquals(2, listCap4Size3.get(0));
        assertEquals(3, listCap4Size3.get(1));
        assertEquals(3, listCap4Size3.remove(1));
        assertEquals(2, listCap4Size3.get(0));
        assertEquals(2, listCap4Size3.remove(0));
        assertTrue(listCap4Size3.isEmpty());
        assertEquals(0, listCap4Size3.size());
        assertEquals(4, listCap4Size3.getCapacity());


    }

    @Test
    void iterator() {
        Iterator<Integer> iter = emptyListCap4.iterator();
        assertFalse(iter.hasNext());
        assertThrows(NoSuchElementException.class, iter::next);

        iter = listCap4Size3.iterator();
        assertTrue(iter.hasNext());
        assertEquals(1, iter.next());
        assertTrue(iter.hasNext());
        assertEquals(2, iter.next());
        assertTrue(iter.hasNext());
        assertEquals(3, iter.next());
        assertFalse(iter.hasNext());
        assertThrows(NoSuchElementException.class, iter::next);
    }

}