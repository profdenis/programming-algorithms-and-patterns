package com.profdenis.list;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class SingleLinkedListTest {

    SingleLinkedList<Integer> emptyList;
    SingleLinkedList<Integer> listSize3;

    @BeforeEach
    void setUp() {
        emptyList = new SingleLinkedList<>();
        listSize3 = new SingleLinkedList<>(1, 2, 3);
    }

    @Test
    void isEmpty() {
        assertTrue(emptyList.isEmpty());

        assertFalse(listSize3.isEmpty());
    }

    @Test
    void size() {
        assertEquals(0, emptyList.size());

        assertEquals(3, listSize3.size());
    }

    @Test
    void indexOf() {
        assertEquals(-1, emptyList.indexOf(2));

        assertEquals(0, listSize3.indexOf(1));
        assertEquals(1, listSize3.indexOf(2));
        assertEquals(2, listSize3.indexOf(3));
        assertEquals(-1, listSize3.indexOf(4));
    }

    @Test
    void get() {
        assertThrows(IndexOutOfBoundsException.class, () -> emptyList.get(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> emptyList.get(0));

        assertThrows(IndexOutOfBoundsException.class, () -> listSize3.get(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> listSize3.get(3));
        assertEquals(1, listSize3.get(0));
        assertEquals(2, listSize3.get(1));
        assertEquals(3, listSize3.get(2));
    }

    @Test
    void set() {
        assertThrows(IndexOutOfBoundsException.class, () -> emptyList.set(-1, 5));
        assertThrows(IndexOutOfBoundsException.class, () -> emptyList.set(0, 5));

        assertThrows(IndexOutOfBoundsException.class, () -> listSize3.set(-1, 5));
        assertThrows(IndexOutOfBoundsException.class, () -> listSize3.set(3, 5));
        assertEquals(1, listSize3.set(0, 5));
        assertEquals(5, listSize3.get(0));
    }

    @Test
    void addElement() {
        emptyList.add(5);
        assertFalse(emptyList.isEmpty());
        assertEquals(1, emptyList.size());
        assertEquals(5, emptyList.get(0));

        listSize3.add(5);
        assertFalse(listSize3.isEmpty());
        assertEquals(4, listSize3.size());
        assertEquals(5, listSize3.get(3));

        listSize3.add(6);
        assertFalse(listSize3.isEmpty());
        assertEquals(5, listSize3.size());
        assertEquals(6, listSize3.get(4));
    }

    @Test
    void addIndexElement() {
        assertThrows(IndexOutOfBoundsException.class, () -> emptyList.add(-1, 5));
        assertThrows(IndexOutOfBoundsException.class, () -> emptyList.add(1, 5));
        emptyList.add(0, 5);
        assertFalse(emptyList.isEmpty());
        assertEquals(1, emptyList.size());
        assertEquals(5, emptyList.get(0));

        assertThrows(IndexOutOfBoundsException.class, () -> listSize3.set(-1, 5));
        assertThrows(IndexOutOfBoundsException.class, () -> listSize3.set(4, 5));
        listSize3.add(2, 5);
        assertFalse(listSize3.isEmpty());
        assertEquals(4, listSize3.size());
        assertEquals(5, listSize3.get(2));
        assertEquals(3, listSize3.get(3));

        listSize3.add(0, 6);
        assertFalse(listSize3.isEmpty());
        assertEquals(5, listSize3.size());
        assertEquals(6, listSize3.get(0));
        assertEquals(1, listSize3.get(1));
        assertEquals(2, listSize3.get(2));
        assertEquals(5, listSize3.get(3));
        assertEquals(3, listSize3.get(4));
    }

    @Test
    void removeElement() {
        assertFalse(emptyList.remove(Integer.valueOf(5)));
        assertTrue(emptyList.isEmpty());
        assertEquals(0, emptyList.size());

        assertFalse(listSize3.remove(Integer.valueOf(0)));
        assertTrue(listSize3.remove(Integer.valueOf(1)));
        assertEquals(2, listSize3.get(0));
        assertEquals(3, listSize3.get(1));
        assertTrue(listSize3.remove(Integer.valueOf(3)));
        assertEquals(2, listSize3.get(0));
        assertTrue(listSize3.remove(Integer.valueOf(2)));
        assertTrue(listSize3.isEmpty());
        assertEquals(0, listSize3.size());
    }

    @Test
    void removeIndex() {
        assertThrows(IndexOutOfBoundsException.class, () -> emptyList.remove(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> emptyList.remove(0));
        assertTrue(emptyList.isEmpty());
        assertEquals(0, emptyList.size());

        assertThrows(IndexOutOfBoundsException.class, () -> listSize3.remove(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> listSize3.remove(3));
        assertEquals(1, listSize3.remove(0));
        assertEquals(2, listSize3.get(0));
        assertEquals(3, listSize3.get(1));
        assertEquals(3, listSize3.remove(1));
        assertEquals(2, listSize3.get(0));
        assertEquals(2, listSize3.remove(0));
        assertTrue(listSize3.isEmpty());
        assertEquals(0, listSize3.size());
    }

    @Test
    void iterator() {
        Iterator<Integer> iter = emptyList.iterator();
        assertFalse(iter.hasNext());
        assertThrows(NoSuchElementException.class, iter::next);

        iter = listSize3.iterator();
        assertTrue(iter.hasNext());
        assertEquals(1, iter.next());
        assertTrue(iter.hasNext());
        assertEquals(2, iter.next());
        assertTrue(iter.hasNext());
        assertEquals(3, iter.next());
        assertFalse(iter.hasNext());
        assertThrows(NoSuchElementException.class, iter::next);
    }

}