package com.profdenis.recursive;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Sum2Test {

    int[] arguments = {-1, 0, 1, 5, 25};
    int[] expected = {0, 0, 1, 15, 325};

    @Test
    void sumIter() {
        for (int i = 0; i < arguments.length; i++) {
            assertEquals(expected[i], Sum.sumIter(arguments[i]));
        }
    }

    @Test
    void sumRec() {
        for (int i = 0; i < arguments.length; i++) {
            assertEquals(expected[i], Sum.sumRec(arguments[i]));
        }
    }
}