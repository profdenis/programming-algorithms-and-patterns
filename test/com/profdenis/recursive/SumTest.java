package com.profdenis.recursive;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumTest {

    @Test
    void sumIter() {
        assertEquals(0, Sum.sumIter(-1));
        assertEquals(0, Sum.sumIter(0));
        assertEquals(1, Sum.sumIter(1));
        assertEquals(15, Sum.sumIter(5));
        assertEquals(325, Sum.sumIter(25));
    }

    @Test
    void sumRec() {
        assertEquals(0, Sum.sumRec(-1));
        assertEquals(0, Sum.sumRec(0));
        assertEquals(1, Sum.sumRec(1));
        assertEquals(15, Sum.sumRec(5));
        assertEquals(325, Sum.sumRec(25));
    }
}