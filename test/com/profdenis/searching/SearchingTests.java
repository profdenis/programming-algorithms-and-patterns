package com.profdenis.searching;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SearchingTests {

    int[] arr1 = {5, 2, 8, 7, 3, 1, 4, 6, 9};
    int[] targets1 = {5, 7, 9, 0};
    int[] expected1 = {0, 3, 8, -1};

    int[] arr2 = {1, 2, 4, 5, 7, 8, 9, 12};
    int[] targets2 = {0, 1, 6, 7, 9, 10, 12, 13};
    int[] expected2 = {-1, 0, -1, 4, 6, -1, 7, -1};

    Integer[] arr3 = {5, 2, 8, 7, 3, 1, 4, 6, 9};
    Integer[] targets3 = {5, 7, 9, 0};
    Integer[] expected3 = {0, 3, 8, -1};

    Integer[] arr4 = {1, 2, 4, 5, 7, 8, 9, 12};
    Integer[] targets4 = {0, 1, 6, 7, 9, 10, 12, 13};
    Integer[] expected4 = {-1, 0, -1, 4, 6, -1, 7, -1};

    @Test
    void testSequentialSearch() {
        for (int i = 0; i < targets1.length; i++) {
            assertEquals(expected1[i], Searching.sequentialSearch(arr1, targets1[i]));
        }

        for (int i = 0; i < targets2.length; i++) {
            assertEquals(expected2[i], Searching.sequentialSearch(arr2, targets2[i]));
        }

        for (int i = 0; i < targets3.length; i++) {
            assertEquals(expected3[i], SearchingGeneric.sequentialSearch(arr3, targets3[i]));
        }

        for (int i = 0; i < targets4.length; i++) {
            assertEquals(expected4[i], SearchingGeneric.sequentialSearch(arr4, targets4[i]));
        }
    }

    @Test
    void testBinarySearch() {
        for (int i = 0; i < targets2.length; i++) {
            assertEquals(expected2[i], Searching.binarySearch(arr2, targets2[i]));
        }

        for (int i = 0; i < targets4.length; i++) {
            assertEquals(expected4[i], SearchingGeneric.binarySearch(arr4, targets4[i]));
        }
    }
}
