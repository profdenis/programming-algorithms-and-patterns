package com.profdenis.sorting;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

class SortingGeneric2Test {

    Integer[][] unsorted;
    Integer[][] expected = {{0, 1}, {0, 1, 3, 4, 5}, {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}};
    Integer[][] reversed = {{1, 0}, {5, 4, 3, 1, 0}, {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}};

    @BeforeEach
    void setUp() {
        unsorted = new Integer[][]{{1, 0}, {1, 5, 3, 4, 0}, {1, 7, 5, 3, 8, 6, 4, 9, 0, 2}};
    }

    @Test
    void isSorted() {
        for (Integer[] integers : unsorted) {
            assertFalse(SortingGeneric.isSorted(integers));
        }
        assertTrue(SortingGeneric.isSorted(new Integer[]{}));
        assertTrue(SortingGeneric.isSorted(new Integer[]{1}));
        for (Integer[] integers : expected) {
            assertTrue(SortingGeneric.isSorted(integers));
        }
    }

    @Test
    void selectionSort() {
        for (int i = 0; i < unsorted.length; i++) {
            SortingGeneric.selectionSort(unsorted[i]);
            assertArrayEquals(expected[i], unsorted[i]);
        }
    }

    @Test
    void selectionSortComparator() {
        for (int i = 0; i < unsorted.length; i++) {
            SortingGeneric.selectionSort(unsorted[i], Comparator.naturalOrder());
            assertArrayEquals(expected[i], unsorted[i]);
        }

        for (int i = 0; i < unsorted.length; i++) {
            SortingGeneric.selectionSort(unsorted[i], new ReverseComparator());
            assertArrayEquals(reversed[i], unsorted[i]);
        }
    }

    @Test
    void insertionSort() {
        for (int i = 0; i < unsorted.length; i++) {
            SortingGeneric.insertionSort(unsorted[i]);
            assertArrayEquals(expected[i], unsorted[i]);
        }
    }

    @Test
    void insertionSortComparator() {
        for (int i = 0; i < unsorted.length; i++) {
            SortingGeneric.insertionSort(unsorted[i], Comparator.naturalOrder());
            assertArrayEquals(expected[i], unsorted[i]);
        }

        for (int i = 0; i < unsorted.length; i++) {
            SortingGeneric.insertionSort(unsorted[i], new ReverseComparator());
            assertArrayEquals(reversed[i], unsorted[i]);
        }
    }

    static class ReverseComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            return -Integer.compare(o1, o2);
        }
    }
}