package com.profdenis.sorting;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

class SortingGenericTest {

    Integer[] unsorted1;// = {1, 0};
    Integer[] unsorted2;// = {1, 5, 3, 4, 0};
    Integer[] unsorted3;// = {1, 7, 5, 3, 8, 6, 4, 9, 0, 2};
    Integer[] expected1 = {0, 1};
    Integer[] expected2 = {0, 1, 3, 4, 5};
    Integer[] expected3 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    Integer[] reversed1 = {1, 0};

    @BeforeEach
    void setUp() {
        unsorted1 = new Integer[]{1, 0};
        unsorted2 = new Integer[]{1, 5, 3, 4, 0};
        unsorted3 = new Integer[]{1, 7, 5, 3, 8, 6, 4, 9, 0, 2};
    }

    @Test
    void isSorted() {
        assertFalse(SortingGeneric.isSorted(unsorted1));
        assertFalse(SortingGeneric.isSorted(unsorted2));
        assertFalse(SortingGeneric.isSorted(unsorted3));
        assertTrue(SortingGeneric.isSorted(new Integer[]{}));
        assertTrue(SortingGeneric.isSorted(new Integer[]{1}));
        assertTrue(SortingGeneric.isSorted(expected1));
        assertTrue(SortingGeneric.isSorted(expected2));
        assertTrue(SortingGeneric.isSorted(expected3));
    }

    @Test
    void selectionSort() {
        Integer[] unsorted = {1, 5, 3, 4, 0};
        Integer[] expected = {0, 1, 3, 4, 5};
        SortingGeneric.selectionSort(unsorted);
        assertArrayEquals(expected, unsorted);
    }

    @Test
    void selectionSortComparator() {
    }

    @Test
    void insertionSort() {
        SortingGeneric.insertionSort(unsorted1);
        assertArrayEquals(expected1, unsorted1);
        SortingGeneric.insertionSort(unsorted2);
        assertArrayEquals(expected2, unsorted2);
        SortingGeneric.insertionSort(unsorted3);
        assertArrayEquals(expected3, unsorted3);
    }

    @Test
    void insertionSortComparator() {
        SortingGeneric.insertionSort(unsorted1, Comparator.naturalOrder());
        assertArrayEquals(expected1, unsorted1);
        SortingGeneric.insertionSort(unsorted2, Comparator.naturalOrder());
        assertArrayEquals(expected2, unsorted2);
        SortingGeneric.insertionSort(unsorted3, Comparator.naturalOrder());
        assertArrayEquals(expected3, unsorted3);

        SortingGeneric.insertionSort(unsorted1, new ReverseComparator());
        assertArrayEquals(reversed1, unsorted1);
    }

    static class ReverseComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            return - Integer.compare(o1, o2);
        }
    }
}