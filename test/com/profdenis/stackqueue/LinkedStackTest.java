package com.profdenis.stackqueue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.*;

class LinkedStackTest {

    LinkedStack<Integer> emptyStack;
    LinkedStack<Integer> stackSize3;

    @BeforeEach
    void setUp() {
        emptyStack = new LinkedStack<>();
        stackSize3 = new LinkedStack<>(1, 2, 3);
    }

    @Test
    void size() {
        assertEquals(0, emptyStack.size());

        assertEquals(3, stackSize3.size());
    }

    @Test
    void peek() {
        assertThrows(EmptyStackException.class, () -> emptyStack.peek());
        assertEquals(0, emptyStack.size());

        assertEquals(3, stackSize3.peek());
        assertEquals(3, stackSize3.size());
    }

    @Test
    void push() {
        emptyStack.push(5);
        assertEquals(1, emptyStack.size());
        assertEquals(5, emptyStack.peek());

        stackSize3.push(5);
        assertEquals(4, stackSize3.size());
        assertEquals(5, stackSize3.peek());

        stackSize3.push(6);
        assertEquals(5, stackSize3.size());
        assertEquals(6, stackSize3.peek());
    }

    @Test
    void pop() {
        assertThrows(EmptyStackException.class, () -> emptyStack.pop());

        assertEquals(3, stackSize3.pop());
        assertEquals(2, stackSize3.size());
        assertEquals(2, stackSize3.pop());
        assertEquals(1, stackSize3.size());
        assertEquals(1, stackSize3.pop());
        assertEquals(0, stackSize3.size());

        stackSize3.push(5);
        assertEquals(1, stackSize3.size());
        assertEquals(5, stackSize3.pop());
        assertEquals(0, stackSize3.size());
    }


}